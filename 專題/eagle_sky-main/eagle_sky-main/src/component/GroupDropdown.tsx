import React, { useState } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { color } from '../style';
//import { EditGroup, AddMember, RemoveMember } from '../../src/page/person/Person1function';

function MemberDropdown(props) {
  const { members, user, groupId, navigation } = props;
    let accountId=0;
  return (
    <View style={styles.memberDropdown}>
      {props.members.map((member, index) => (
      console.log("Member Object:", member),

        <View key={index} style={styles.row}>
          <View style={styles.memberInfo}>
            <Text style={styles.memberOptionText}>{member.name}</Text>
          </View>
          <View key={index} style={styles.removeButtonContainer}>
            {props.user !== member.name && (
                  accountId = member.id,
                  console.log("accountId1",accountId),
              <TouchableOpacity
                style={styles.removeButton}
                onPress={() => handleRemoveMember(groupId, member.id, navigation)} // RemoveMember
              >
                <Text style={styles.word}>[ 移除 ]</Text>
              </TouchableOpacity>
            )}
            {props.user === member.name && (
                  accountId = member.id,
                  console.log("accountId2",accountId),
              <TouchableOpacity
                style={styles.removeButton}
                onPress={() => handleRemoveMember(groupId, member.id, navigation)} // RemoveMember
              >
                <Text style={styles.word}>[ 退出 ]</Text>
              </TouchableOpacity>
            )}
          </View>
        </View>
      ))}
    </View>
  );
    function handleRemoveMember(groupId, accountId, navigation) {

      console.log("Remove Member - Group ID:", groupId);
      console.log("Remove Member - Account ID:", accountId);
      navigation.navigate("Person1function", { kind: 'RemoveMember', groupId, accountId });
    }
}

function GroupDropdown(props) {
  const { group, user, groupId, groupName, navigation } = props;
  const { accounts } = group;
  const accountId=[];
  const [showOptions, setShowOptions] = useState(false);

  const handleGroupSelect = () => {
    setShowOptions(!showOptions);
  };

  return (
    <View style={styles.container}>
      <Text style={{ height: 20 }} />
      <View style={styles.groupContainer}>
        <Text style={styles.groupText}>{props.groupName}</Text>
        <TouchableOpacity style={styles.iconButton} onPress={handleGroupSelect}>
          <View style={showOptions ? styles.triangle_up : styles.triangle_down} />
        </TouchableOpacity>
      </View>
      {showOptions && (
        <View>
          <MemberDropdown navigation={navigation} members={accounts} user={user} groupId={groupId}/>
          <Text style={{ height: 20 }} />
          <View style={styles.row}>
            <TouchableOpacity
              style={styles.editButton}
              onPress={() => {navigation.navigate("Person1function", {kind : 'EditGroup',groupId:groupId})}} // EditGroup
            >
              <Text style={styles.word}>[ 編輯名稱 ]</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.addButton}
              onPress={() => {navigation.navigate("Person1function", {kind : 'AddMember',groupId:groupId})}} // AddMember
            >
              <Text style={styles.word}>[ 新增成員 ]</Text>
            </TouchableOpacity>
          </View>
          <Text style={{ height: 20 }} />
        </View>
      )}
    </View>
  );
}

export default GroupDropdown;

const styles = StyleSheet.create({
  container: {
    alignSelf: 'stretch',
    paddingHorizontal: 20,
    flex:1,
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 30,
    paddingVertical: 0,
  },
  memberInfo: {
    flex: 1,
  },
  groupContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-round',
    paddingVertical: 10,
    paddingHorizontal: 20,
  },
  groupText: {
    fontSize: 20,
    fontWeight: '500',
    color: color.darkBlue,
    flex: 1,
  },
  memberDropdown: {
    borderColor: color.darkBlue,
    //borderRadius: 5,
    flex:1,
    marginTop: 10,
  },
  memberOptionText: {
    fontSize: 20,
    fontWeight: '400',
    color: "black",
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  removeButtonContainer: {
    marginLeft: 5,
  },
  removeButton: {
    color: color.darkBlue,
    right: 10,
    alignItems: 'flex-end',
    //flexDirection: 'row',
  },
  triangle_up: {
    width: 0,
    height: 0,
    borderStyle: 'solid',
    borderWidth: 5,
    borderBottomWidth: 10,
    borderLeftColor: 'transparent',
    borderTopColor: 'transparent',
    borderBottomColor: color.darkBlue,
    borderRightColor: 'transparent',
  },
  triangle_down: {
    width: 0,
    height: 0,
    borderStyle: 'solid',
    borderWidth: 5,
    borderTopWidth: 10,
    borderLeftColor: 'transparent',
    borderTopColor: color.darkBlue,
    borderBottomColor: 'transparent',
    borderRightColor: 'transparent',
    top: 7,
  },
  editButton: {
    marginTop: 5,
    alignItems: 'center',
    //left:20,
  },
  addButton: {
    marginTop: 5,
    alignItems: 'center',
    //left:30,
  },
  removeGroupButton: {
    marginTop: 5,
    alignItems: 'center',
    //left:40,
  },
  word: {
    color: color.darkBlue,
  }
});

export default GroupDropdown;

