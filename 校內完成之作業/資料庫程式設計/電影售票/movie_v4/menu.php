<!DOCTYPE html>

<head>
    <meta charset="UTF-5">
    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" href="menu.css">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
</head>

<body>
    <header>
        <h2>影城系統</h2>
        <input type="button" onclick="location.href='login.php'" value="登入">
        <input type="button" value="客服機器人" onclick="location.href='chatbot.html'">
    </header>

    <br>
    <br>
    <main>
        <div class="siderPadding"></div>
        <div class="sider">
            <p>類別：</p>
            <br>
            <form method="post">
                <input type="submit" value="全部" name="all"><br>
                <input type="submit" value="動作" name="active"><br>
                <input type="submit" value="愛情" name="love"><br>
                <input type="submit" value="懸疑" name="suspense"><br>
                <input type="submit" value="恐怖" name="horror"><br>
                <input type="submit" value="動畫" name="animation"><br>
                <input type="submit" value="其他" name="other"><br>
            </form>
            <br>
        </div>
    
        <div class="filmBlock">
                <?php
                    include_once('filmPosters.php');
                ?>
        </div>
    </main>
    
</body>