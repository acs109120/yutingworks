/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */
import { color } from '../style'
import { navigationType } from './navigationType'
import React, {useState, useEffect ,Component } from 'react';
import type {PropsWithChildren} from 'react';
import ConcertCard from '../component/ConcertCard'
import ActivityPhoto from '../assets/eagleSky2.png'  //照片路徑改這
import {
  Alert,
  FlatList,
  SafeAreaView,
  TextInput,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Button,
  TouchableOpacity,
} from 'react-native';
import {
  Colors,
} from 'react-native/Libraries/NewAppScreen';


type SectionProps = PropsWithChildren<{
  title: string;
}>;
class Ticket{
  title: string="";
  seat: string="";
  date: string="";
  price: number=0 ;
}

class User{
  name: string="";
  phone: string="";
}
export default function Home({ navigation }: navigationType) {
  const isDarkMode = useColorScheme() === 'dark';
  const backgroundStyle = {
    backgroundColor: color.backgroundColor,
  };

  const AddButton= ({ title, onPress = () => { } }) => {
    return (
      <TouchableOpacity style={styles.BtnContainer2}
        activeOpacity={0.8}
        onPress={onPress}>
          <Text style={styles.Title2}>{title}</Text>
      </TouchableOpacity>
    )
  }
  
  const EstablishButton = ({ title, onPress = () => { } }) => {
    return (
        <TouchableOpacity
            activeOpacity={0.8}
            onPress={onPress}
        >
            <View style={styles.BtnContainer}>
                <Text style={styles.Title}>{title}</Text>
            </View>
        </TouchableOpacity>
    )
  }
  const GroupDropdown = () => {
  
    const groupOptions = ['愛人錯過', '失去猶如和', '你我他她它牠'];
    const [selectedGroup, setSelectedGroup] = useState('');
    const [isDropdownVisible, setIsDropdownVisible] = useState(false);
  
    const handleGroupSelect = (option) => {
        setSelectedGroup(option);
        setIsDropdownVisible(false);
      };
  
    const toggleDropdown = () => {
        setIsDropdownVisible(!isDropdownVisible);
    };
  
    return (
      <View style={styless.container}>
              <TouchableOpacity style={styless.dropdownButton} onPress={toggleDropdown}>
                  <View style={styless.block}>
                <Text style={styless.selectedGroupText}> {selectedGroup || '預綁定之演唱會 : 請選擇 '}</Text>
                <View style={isDropdownVisible ? styless.triangle_up : styless.triangle_down}  />
                </View>
              </TouchableOpacity>
  
              <Text style={{ height: 50 }} />
        {isDropdownVisible && (
               <View style={[styless.optionList, styless.dropdownPosition]}>
                 <FlatList
                   data={groupOptions}
                   renderItem={({ item }) => (
                     <TouchableOpacity style={styless.optionButton} onPress={() => handleGroupSelect(item)}>
                       <Text style={styless.optionText}>{item}</Text>
                     </TouchableOpacity>
                   )}
                   keyExtractor={(item) => item}
                   nestedScrollEnabled
                 />
               </View>
             )}
      </View>
    );
  };
  const [groupInfo, setGroupInfo] = useState({
     groupName: '',  //string 
     members: [],   //array 下面再接id name ...
   });               //這裡改了初始設null 發現剛開頁面初始框會變2個 先不改

  const [establishButtonCount, setEstablishButtonCount] = useState(0);
  const [submittedData, setSubmittedData] = useState(null); // 用于显示提交后的数据

  const handleAddMember = () => {   // 點'新增成員'時會被調用 ，newmerber會接在原members後面
    
    if(establishButtonCount < 2){
        setEstablishButtonCount(prevCount => prevCount + 1);

        const newMember = { id: '', name: '' };
        setGroupInfo((prev) => ({ ...prev, members: [...prev.members, newMember] }));      
    }else{
        Alert.alert('已達人數上限4人');
    }   
  };
  
  const handleMemberInputChange = (text:string, index:number, field:string) => { //處理文字輸入框中的文字變化
    const newMembers = [...groupInfo.members]; //創建member訊息的副本
    newMembers[index][field] = text;  //更新副本中特定成員的特定字段
    setGroupInfo((prev) => ({ ...prev, members: newMembers })); //使用副本更新groupInfo的成員信息
  };

  const handleCreateGroup = () => {
    
    setSubmittedData(groupInfo);
    console.log(groupInfo); // 输出旧值
    setTimeout(() => {
      console.log(groupInfo); // 输出最新值
    }, 0);
    setGroupInfo({
      groupName: '',
      members: [],
    });
  };
  useEffect(() => {
    console.log("Updated groupInfo:", groupInfo);
  }, [groupInfo]);

  const determinestablish = () =>{
        handleCreateGroup();
        handleFirst();
        showTip();

  }
//////////////////////
const [firstID, setFirstID] = useState('');
  const [firstName, setFirstName] = useState('');
  const [displayText, setDisplayText] = useState(''); //測試顯示用

  const handleFirstID = (text:string) => {
    setFirstID(text);
  };

  const handleFirstName = (text:string) => {
    setFirstName(text);
  };

  const handleFirst = () => {
    const combinedText = `${firstID} ${firstName}`;
    setDisplayText(combinedText);
    setFirstID(''); 
    setFirstName('');
  };
  ///////////////////////////
  const showTip = () => {
    Alert.alert('建立群組',groupInfo.groupName)
} 
const ticket = new Ticket();
        //設置ticket屬性
        ticket.title = "演唱會名稱";
        ticket.seat = "A區";
        ticket.date = "2023/12/25 18:00~22:00";
        ticket.price = 2600;
    const user = new User();
        //設置user屬性
        user.name = "蕭湘竹";
        user.phone = "0987654321";
  return (
    <SafeAreaView style={backgroundStyle}>
      <GroupDropdown />
      <FlatList
  contentContainerStyle={styles.container}
  data={groupInfo.members}
  ListHeaderComponent={<GroupDropdown />} // 在列表頂部放置 GroupDropdown
  renderItem={({ item, index }) => (
    <View style={styles.divmem1}>
      {/* 其他項目內容 */}
      <View style={styles.inputContainer}>
        <Text style={styles.word}>I D：</Text>
        <TextInput
          style={styles.input}
          placeholder=""
          value={item.id}
          onChangeText={(text) => handleMemberInputChange(text, index, 'id')}
        />
      </View>
      <Text style={{ height: 5 }} />
      <View style={styles.inputContainer}>
        <Text style={styles.word}>姓名：</Text>
        <TextInput
          style={styles.input}
          placeholder=""
          value={item.name}
          onChangeText={(text) => handleMemberInputChange(text, index, 'name')}
        />
      </View>
    </View>
  )}
  keyExtractor={(item, index) => index.toString()} // 使用索引作為 key
  ItemSeparatorComponent={() => {return <View style={{height: 50}} />}}
  ListFooterComponent={(
    <View>
      {/* 增加成員按鈕 */}
      <AddButton title="+" onPress={handleAddMember} />
    </View>
  )}
/>
<View style={styles.divin}>
  <EstablishButton title="建立" onPress={determinestablish} />
  <Text>{'\n'}</Text>
</View>


    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  line1:{   //字下方的線
    height: 30,
    padding:2,
    width: '100%',
    borderBottomWidth: 3,
    borderColor: color.blue,
    marginBottom: 20,
    fontSize: 17,
    flexDirection: 'row',
    letterSpacing:1,
  },
  divin:{  //最外層框
    //marginHorizontal: 2,
        borderRadius: 15,
        
        padding: 5,
        marginTop:10,
        marginLeft:30,
        marginRight:30,
        marginButtom:0,
        //backgroundColor: color.backgroundColor ,
        paddingHorizontal: 20,
  },
  
  divmem1:{  //下方新增成員之框
      borderRadius: 10,
        marginBottom: 10,
        //padding:5,
        backgroundColor:color.lightBlock,
        //alignItems: 'center',
        padding: 15,
        //paddingBottom:0,
        marginTop:5,
        //margin:5,
        marginLeft:50,
        marginRight:39,
        flex:1,
        //backgroundColor: color.backgroundColor ,
        paddingHorizontal: 10,
  },

smallTitle1: {
  fontSize: 14,
  fontWeight: '600',
  color: '#000000',
},
smallTitle2: {
  fontSize: 16,
  marginTop: 25,
  fontWeight: '400',
  color: color.darkBlue,
  flexDirection: 'row',
},
  Title: { color:'white', fontWeight: 'bold', fontSize: 14 }, //button本身設定
  Title2: { color:color.darkBlue, fontWeight: '300',fontSize: 35, marginTop: -6 }, //+button大小設定
BtnContainer: {  //'建立' 按鈕
    backgroundColor: color.blue,
    //height: 40,
    width:60,
    borderRadius: 10,
    justifyContent: "center",
    alignItems: "center",
    alignSelf: 'flex-end',
    paddingTop:3,
    paddingBottom:7,
},
BtnContainer2 : {  //'+'按鈕
  //backgroundColor: color.blue,
  //height: 40,
  width:40,
  height:40,
  borderWidth:1,
  // textAlign: 'center', // 文字水平居中
  // textAlignVertical: 'center', // 文字垂直居中（僅Android支持）
  margin:15,
   //justifyContent: "center",
   alignItems: "center",
   alignSelf: 'center',
  //padding:5,
  borderRadius: 50,
  borderStyle:'dashed',
  borderColor: color.darkBlue
}, 
  input: {
    height: 25,
    width: '75%',
    borderBottomWidth: 3,
    borderColor: color.blue,
    marginBottom: 7,
    fontSize: 15,
    flexDirection: 'row',
    
    padding:2,
},
inputContainer: {
    flexDirection: 'row', // 水平排列
    flex:1,
    //marginTop:10,
    marginBottom: 1,
    textAlign: 'left',
},
word: {
  marginTop: 3,
  fontSize: 16,
  fontWeight: '500',
  color: color.darkBlue,
  flexDirection: 'row',

},
});
const styless = StyleSheet.create({
  container: {
    alignSelf: 'stretch',
    paddingHorizontal: 15,
  },
  dropdownButton: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'space-between',
    backgroundColor: color.blue,
    borderBottomWidth: 3,
    borderBottomColor: color.darkBlue,
    padding: 10,
    width: '100%', // 使用百分比設定寬度
    //aspectRatio: 8 / 1, // 設定寬高比例為 4:1
    alignSelf: 'center', // 水平置中
    borderRadius: 5,
    position: "absolute",
    zIndex: 2,
  },
  block: {
    //marginLeft: 20,
    //bottom: 10,
    backgroundColor: color.blue,
    width: '100%', // 使用百分比設定寬度
    //aspectRatio: 5 / 1, // 設定寬高比例為 4:1
    alignSelf: 'center', // 水平置中
    borderRadius: 5,
    flexDirection: 'row',
  },
  selectedGroupText: {
    fontSize: 15,
    fontWeight: '600',
    color: '#FFFFFF',
    flex:1,
    letterSpacing:1.5,
  },
  optionList: {
    padding: 5,
    borderWidth: 1,
    backgroundColor: color.blue,
    borderColor: '#FFFFFF',
    borderRadius: 0,
    marginTop: 5,
  },
  optionButton: {
    padding: 10,
    borderBottomWidth: 1,
    borderBottomColor: color.white,
  },
  optionText: {
    fontSize: 15,
    fontWeight: '600',
    color: color.white,
  },
  triangle_up:{
    width:0,
    height:0,
    borderStyle:'solid',
    borderWidth:5,
    borderBottomWidth:10,
    borderLeftColor: 'transparent',
    borderTopColor: 'transparent',
    borderBottomColor:color.white,
    borderRightColor: 'transparent',

  },
  triangle_down:{
      width:0,
      height:0,
      borderStyle:'solid',
      borderWidth:5,
      borderTopWidth:10,
      borderLeftColor: 'transparent',
      borderTopColor:color.white,
      borderBottomColor: 'transparent',
      borderRightColor: 'transparent',
      top:7,
  },
  dropdownPosition: {
      position: 'absolute',
      zIndex: 2,
      marginTop: 40,
      alignSelf: 'center', // 水平置中
      width:'100%',
  },
});
