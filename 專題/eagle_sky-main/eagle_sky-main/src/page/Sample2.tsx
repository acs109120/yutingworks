/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */
import React from 'react';
import { StackNavigationProp } from '@react-navigation/stack';
import { navigationType } from './navigationType'
import QRCode from 'react-native-qrcode-svg';
import eagleSky from '../assets/eagleSky.png'
import {
  Image,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  AppRegistry,
  Text,
  useColorScheme,
  View,
  Button,
} from 'react-native';
export default function Home({ navigation }: navigationType) {
  return (
    // <Pressable onPress={() => navigation.navigate('ColorPalette')}>
      <View>
        {/* 生成QRcode */}
        <QRCode
          value="Just some string value"
          logo={eagleSky}
          logoSize={50}
          size={200}
          logoBackgroundColor='white'
        />
        <Text>Hello World, This is Home Screen</Text>
        <Button
          title="Go to Sample1"
          onPress={() => navigation.navigate('Sample1')}
        />
      </View>
    // </Pressable>
  );
}