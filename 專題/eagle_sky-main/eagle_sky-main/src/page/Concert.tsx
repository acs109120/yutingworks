import { color } from '../style';
import { navigationType } from './navigationType';
import React, { useEffect,Component } from 'react';
import type {PropsWithChildren} from 'react';
import ConcertCard from '../component/ConcertCard';
import ActivityPhoto from '../assets/eagleSky2.png' ; //照片路徑改這
import {loadConcert,ticketConcert,getTicketConcert,concert} from './concert/load'
import {
  Image,
  FlatList,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  AppRegistry,
  Text,
  useColorScheme,
  View,
  Button,
  TouchableOpacity,
  TouchableWithoutFeedback,
} from 'react-native';
import {
  Colors,
} from 'react-native/Libraries/NewAppScreen';
import { useRoute } from '@react-navigation/native';
type SectionProps = PropsWithChildren<{
  title: string;
}>;
//訂購票卷 轉售票卷 時間倒數更改 顯示 還未修正 後端時間格式



function Section({children, title}: SectionProps): JSX.Element {
    const isDarkMode = useColorScheme() === 'dark';
  
    return (
      <View style={styles.sectionContainer}>
        
        <Text
          style={
            styles.sectionTitle
          }>
          {title}
        </Text>
        
        <Text
          style={
            styles.sectionDescription
           
          }>
          {children}
        </Text>
      </View>
    );
  }
  const BookingButton = ({ title, onPress = () => { } }) => {
    return (
        <TouchableOpacity
            activeOpacity={0.8}
            onPress={onPress}
        >
            <View style={styles.BtnContainerRight}>
                <Text style={styles.Title}>{title}</Text>
            </View>
        </TouchableOpacity>
    )
  }
  const TicketgButton = ({ title, onPress = () => { } }) => {
    return (
        <TouchableOpacity
            activeOpacity={0.8}
            onPress={onPress}
        >
           <View style={styles.BtnContainerRight}>
                  <Text style={styles.Title}>{title}</Text>
              </View>
              
        </TouchableOpacity>
    )
  }
  const TicketgButtonDisable = ({ title, onPress = () => { } }) => {
    return (
        <TouchableWithoutFeedback>
           <View style={styles.BtnContainerRightLimit}>
                  <Text style={styles.Title}>{title}</Text>
              </View>      
        </TouchableWithoutFeedback>
    )
  }


export default function Home({ navigation }: navigationType): JSX.Element {
 
    const isDarkMode = useColorScheme() === 'dark';
    const [time, setTime] = React.useState(new Date());
    const backgroundStyle = {
      backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
    };
    let sellWhat:boolean = true;
    const route = useRoute();
    let concertId = route.params && route.params.concertId;
    //console.log("concertId  : ",concertId);
    

    const [activityData, setActivityData] = React.useState({
      title: "Loading...",
      date: "Loading...",
      time_start: "Loading...",
      time_end: "Loading...",
      date_sell:"2100-12-31",
      img: "http://ip205048.ntcu.edu.tw:8030/uploads/thumbnail_img01copy1_5836a23dd0.jpg",
      imgseat:"http://ip205048.ntcu.edu.tw:8030/uploads/thumbnail_img01copy1_5836a23dd0.jpg",
      introduction: "Loading...",
      notice: "Loading...",
    });
    
    useEffect(() => {
      // 异步加载数据库数据
      loadConcert(concertId).then((loadedConcert) => {
        if(loadedConcert.time_start)
              loadedConcert.time_start=loadedConcert.time_start.substring(0,5)
        if(concert.time_end)
              loadedConcert.time_end=loadedConcert.time_end.substring(0,5)
        // 使用loadedConcert来更新组件的状态
        setActivityData({
          title: loadedConcert.title,
          date: loadedConcert.date,
          time_start: loadedConcert.time_start,
          time_end: loadedConcert.time_end,
          date_sell:loadedConcert.date_sell,
          img: loadedConcert.img,
          imgseat:loadedConcert.imgseat,
          introduction: loadedConcert.introduction,
          notice: loadedConcert.notice,
        });
      });
    }, [concertId]);

var msDiff = new Date(activityData.date).getTime()-new Date().getTime(); //month day year
var diff = msDiff / (1000 * 60 * 60 * 24); //演唱會日期-現在差幾天
  //console.log("msDiff: ",diff)

var sellDiff = new Date(activityData.date_sell).getTime()-new Date().getTime(); //month day year
var sdiff = sellDiff / (1000 * 60 * 60 * 24); //sell日期-現在日期差幾天 
//console.log("sdiff: ",sdiff)
 //console.log("Concer.tsx 的datasell: ",activityData)

    return (
      <SafeAreaView style={backgroundStyle}>
        <StatusBar
          
          backgroundColor={backgroundStyle.backgroundColor}
        />
        <ScrollView
          contentInsetAdjustmentBehavior="automatic"
          style={backgroundStyle}>
            
          <View style={styles.detailsActivity}>
                {<ConcertCard
                  title={activityData.title}
                  date={activityData.date}
                  time_start={activityData.time_start}
                  time_end={activityData.time_end}
                  img={activityData.img}
                />
                }
                
              { diff>=7 && sdiff<=0 ?(

                  <BookingButton title="訂購票卷"
                      onPress={() => navigation.navigate("Booking", {concertId : concertId, sellWhat: true})}
                  />
                ) :(
                  <TicketgButtonDisable title="訂購票卷"               
                      onPress={() => navigation.navigate("Booking")}
                  />
              )}
                
             
              {diff >=2 && diff<=7 && sdiff<=0 ?(

                  <TicketgButton title="購買轉售票"               
                      onPress={() => navigation.navigate("Booking",{concertId : concertId, sellWhat: false})}
                  />
                ):(
                  <TicketgButtonDisable title="購買轉售票"               
                      onPress={() => navigation.navigate("Booking")}
                  />
              )}
                 
          </View>
          
          <View style={styles.detailsContainer}>
            <Section title="注意事項"> 
            <Text>{activityData.notice}</Text>
            </Section>
          
            <Section title="活動介紹"> 
              <Text style={styles.highlight}></Text> 
             <Text>{activityData.introduction}</Text>
            </Section>

            <Section title="場地及票種規畫圖"> 
             <Text style={styles.highlight}></Text>
              
              

            </Section>
            <Image  
                    //resizeMode={'cover'} 
                    style={{ width: 300, height: 200,alignSelf: 'center', }}
                    source={{ uri: activityData.imgseat }}
              /> 

          </View>
          <Text>{'\n'}</Text>
          
            <Text>{'\n'}</Text>
            <Text>{'\n'}</Text>
           
        </ScrollView>
      </SafeAreaView>
    );
  }
  
const styles = StyleSheet.create({
    sectionContainer: {
      marginTop: 32,
      paddingHorizontal: 24,
    },
    sectionTitle: {
      fontSize: 24,
      fontWeight: '600',
      color:'black',
    },
    sectionDescription: {
      marginTop: 8,
      fontSize: 18,
      fontWeight: '400',
      color:'black',
    },
    highlight: {
      fontWeight: '700',
    },
    Title: { color:'white', fontWeight: 'bold', fontSize: 14 }, //button本身設定
    BtnContainer: {  //靠中的黃按鈕
      backgroundColor: color.darkyellow,
      //height: 40,
      width:'50%',
      borderRadius: 10,
      justifyContent: "center",
      alignItems: "center",
      alignSelf: 'center',
      paddingTop:3,
      paddingBottom:7,
    },
    BtnContainerRight: {  //靠右且可按的按鈕
      backgroundColor: color.darkyellow,
      height: 30,
      width:100,
      borderRadius: 10,
      justifyContent: "center",
      textAlign:'center',
      alignItems: "center",
      alignSelf: 'flex-end',
      margin:5,
    },
    BtnContainerRightLimit: {  //靠右但不可按的按鈕
        backgroundColor: color.purplegray,
        height: 30,
        width:100,
        borderRadius: 10,
        justifyContent: "center",
        textAlign:'center',
        alignItems: "center",
        alignSelf: 'flex-end',
        margin:5,
        // paddingTop:4,
        // paddingBottom:4,
        // padding:4,
       
    },
    NoteHowDay:{  //還剩幾天才開放轉售區
      fontWeight: 'bold', 
      fontSize: 13,
      alignSelf: 'flex-end',
      paddingRight:15 ,
      color:color.purplegray,
    },
    qrcenter: { //qrcode照片用
      //width: '70%',
      maxWidth: 182,
      //maxWidth:170,
      //maxWidth: '60%',
      //paddingTop: 5,
      //paddingBottom:5,
      //maxHeight: 182,
      height: 182, 
      alignSelf: 'center',
      //backgroundColor: 'steelblue',
      //box-shadow: 0 0 8px 0 rgba(0, 0, 0, 0.08),
      //position: 'relative',
  },
  detailsActivity: { //上面活動區的眶框
    //flexDirection: "row",
          marginHorizontal: 2,
          borderRadius: 15,
          marginBottom: 0,
          marginTop: 20,
          padding: 5,
          //margin:8,
          marginLeft:8,
          marginRight:8,
          //backgroundColor: color.backgroundColor ,
          paddingHorizontal: 20,
  },
  detailsContainer: { //下面說明欄的眶框
    //flexDirection: "row",
          marginHorizontal: 2,
          borderRadius: 15,
          marginBottom: 20,
          marginTop: -30,
          padding: 5,
          //margin:8,
          marginLeft:5,
          marginRight:5,
          //backgroundColor: color.backgroundColor ,
          paddingHorizontal: 20,
  },
  center: {
        justifyContent: 'center',
        alignSelf: 'center',
        alignItems: 'center',
        width: 80,
        height: 80,
    },
    
  });