import { http, sqlURL } from '../../../http'
import eagleSky from '../../assets/eagleSky.png'
import qs from 'qs'
import { userId } from '../../user';

class Concert {
    id: number = 2;
    title: string = "";
    date: string = "";
    time_start: string = "";
    time_end: string = "";
    img: string = "";
    imgseat:string="";
    introduction: string= "";
    notice:string= "";
}

let concert : Concert

async function loadConcert(id :number){
    await http.get('/api/concert-alls/'+ id.toString() +'?populate=seat_map&populate=photo').then(res => {
        concert = {
            id: res.data.data.id,
            title: res.data.data.attributes.title,
            date: res.data.data.attributes.date,
            time_start: res.data.data.attributes.time_start,
            time_end: res.data.data.attributes.time_end,
            introduction: res.data.data.attributes.introduction,
            notice: res.data.data.attributes.notice,
            img: (!res.data.data.attributes.photo.data)?
                eagleSky:
                sqlURL + res.data.data.attributes.photo.data.attributes.formats.thumbnail.url,
            imgseat:(!res.data.data.attributes.seat_map.data)?
                eagleSky:
                sqlURL + res.data.data.attributes.seat_map.data.attributes.formats.thumbnail.url

        }
        concert = concert
    })
    return concert;
}
function setQuery(id: number = 2){
    let query = qs.stringify({
        filters: {
            account: {
                id: {
                    '$eq': userId
                }
            },
            area:{
                concert_all:{
                    id: {
                        '$eq': id
                    }
                }
            }
        }
    })
    return query
}

let seat: string = "";
let area: string = "";
async function getSeat(concertId: number = 2){
    await http.get('/api/tickets?populate=account,area.concert_all&' + setQuery(concertId)).then(res => {
        seat = res.data.data[0]
            ? res.data.data[0].attributes.seat
            : "",
        area = res.data.data[0]
        ? res.data.data[0].attributes.area.data.attributes.area
        : ""
       // console.log("area :   ", area)
       // console.log("seat :   ",seat)
    })
}

export { loadConcert, concert, getSeat, seat,area}
