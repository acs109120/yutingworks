<!DOCTYPE html>

<?php
    session_start();
    $_SESSION["name"]=null;
?>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" href="login.css">
</head>

<body>
    <script type="text/javascript" src="logout.php"></script>
    <header>
        <h2>影城系統</h2>
        <input type="button" onclick="location.href='menu.php'" value="回主頁">
        <input type="button" value="客服機器人" onclick="location.href='chatbot.html'">
    </header>
    <br>
    <br>
    <div class="loginBlock">
        <h3>影城</h3>
        <form class="login" method="post" action="loginRun.php">
            <input id="account" name="account" type="text" placeholder="帳號">
            <input id="password" name="password" type="password" placeholder="密碼">
            <input id="wordsCheck" name="wordsCheck" type="text" placeholder="請輸入：我不是機器人">
            <input type="submit" value="登入">
        </form>
        <input type="button" class="register" onclick="location.href='register.html'" value="註冊帳號">
    </div>
</body>