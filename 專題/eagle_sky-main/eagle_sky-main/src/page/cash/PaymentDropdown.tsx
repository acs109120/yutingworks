import React, { useState } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, FlatList, TextInput } from 'react-native';
import { color } from '../../style';

const PaymentDropdown = () => {
  const paymentOptions = ['VISA', 'Linepay', '銀行轉帳'];
  const [showOptions, setShowOptions] = useState(false);
  const [selectedPayment, setSelectedPayment] = useState('');
  const [showPaymentForm, setShowPaymentForm] = useState(false);
  const [inputValue, setInputValue] = useState('');

  const handlePaymentSelection = (option) => {
    setSelectedPayment(option);
    setShowOptions(false);

    switch (option) {
      case 'VISA':
        setShowPaymentForm(true);
        break;
      case 'Linepay':
      case '銀行轉帳':
      default:
        setShowPaymentForm(false);
    }
  };

  return (
    <View style={styles.container}>
      <Text style={styles.title}>付款方式：{selectedPayment}</Text>
      {/* options列表 */}
      {showOptions && (
        <View style={styles.optionList}>
          <FlatList
            data={paymentOptions}
            renderItem={({ item }) => (
              <TouchableOpacity style={styles.optionButton} onPress={() => handlePaymentSelection(item)}>
                <Text style={styles.optionText}>{item}</Text>
              </TouchableOpacity>
            )}
            keyExtractor={(item) => item}
            nestedScrollEnabled // 添加此属性
          />
        </View>
      )}
      {/* 選填資料顯示 */}
      {showPaymentForm && (
        <View style={styles.container}>
        <Text style={{height: 20}}/>
          <View style={styles.inputContainer}>
            <Text style={styles.word}>信用卡號：</Text>
            <TextInput style={styles.input} placeholder="請輸入卡號" onChangeText={(text) => setInputValue(text)} secureTextEntry={false} />
          </View>
        <Text style={{height: 10}}/>
          <View style={styles.inputContainer}>
            <Text style={styles.word}>有效期限：</Text>
            <TextInput style={styles.input} placeholder="請輸入有效期限" />
          </View>
        <Text style={{height: 10}}/>
          <View style={styles.inputContainer}>
            <Text style={styles.word}>　安全碼：</Text>
            <TextInput style={styles.input} placeholder="請輸入安全碼" />
          </View>
        </View>
      )}
      {/* 倒三角形button */}
            <TouchableOpacity style={styles.iconButton} onPress={() => setShowOptions(!showOptions)}>
              <View style={showOptions ? styles.triangle_up : styles.triangle_down}  />
            </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    position: 'relative',
    alignSelf: 'stretch',
    paddingHorizontal: 20,
    flex: 1
  },
  iconButton: {
    position: 'absolute',
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'flex-end',
    right: 10, // 右側距離,
    padding: 10,
    zIndex: 1,
    top: 2
  },
  optionButton: {
    padding: 10,
    borderBottomWidth: 1,
    borderBottomColor: color.darkBlue,
    borderRadius: 5,
    paddingVertical: 10,
    paddingHorizontal: 15,
    marginBottom: 5,
  },
  optionText: {
    marginTop: 3,
    fontSize: 15,
    fontWeight: '600',
    color: color.darkBlue,
    flexDirection: 'row',
  },
  triangle_up:{
    width:0,
    height:0,
    borderStyle:'solid',
    borderWidth:5,
    borderBottomWidth:10,
    borderLeftColor: 'transparent',
    borderTopColor: 'transparent',
    borderBottomColor: color.darkBlue,
    borderRightColor: 'transparent',
    top: -7
  },
  triangle_down:{
      width:0,
      height:0,
      borderStyle:'solid',
      borderWidth:5,
      borderTopWidth:10,
      borderLeftColor: 'transparent',
      borderTopColor: color.darkBlue,
      borderBottomColor: 'transparent',
      borderRightColor: 'transparent',
  },
  optionList:{
      padding: 5,
      //marginVertical: 5,
      //borderWidth: 1,
      borderColor: color.darkBlue,
      borderRadius: 0,
  },
    title: {
        fontSize: 18,
        fontWeight: '700',
        color: color.darkBlue,
    },
    word: {
        marginTop: 3,
        fontSize: 15,
        fontWeight: '600',
        color: color.darkBlue,
        flexDirection: 'row',
    },
    line: {
        height: 3,
        width: '100%',
        backgroundColor: color.lightBlue,
        flex: 1,
        padding: 3,
    },
    input: {
        height: 12,
        width: '80%',
        borderBottomWidth: 2,
        borderColor: color.darkBlue,
        marginBottom: 1,
        fontSize: 12,
        flexDirection: 'row',
    },
    inputContainer: {
        flexDirection: 'row', // 水平排列
        flex:1,
        marginBottom: 1,
        textAlign: 'left',
    },
});

export default PaymentDropdown;
