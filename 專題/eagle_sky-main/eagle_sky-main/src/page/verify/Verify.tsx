import React, { useState,useEffect } from 'react';
import { Button, View, StyleSheet, Text, TextInput, Image, TouchableOpacity, Alert, FlatList } from 'react-native';
import { navigationType } from '../navigationType'
import { color } from '../../style'
import { useRoute } from '@react-navigation/native';

import { checkVerification } from "../../../api/verify";
import OTPInputView from "@twotalltotems/react-native-otp-input";

export default function Verify({ navigation }: navigationType){
    const [inputVerification, setInputVerification] = React.useState('');
    const route = useRoute();
      let verificationCode = route.params && route.params.verificationCode;
    //const [password, setPassword] = React.useState('')
    const [invalidCode, setInvalidCode] = useState(false);
    const determin = () =>{
        
        if(verificationCode == inputVerification){
            navigation.navigate("Home");
        }else{
            Alert.alert("驗證碼錯誤")
        }
    }
    
    return (
        <View>
            <Text style={styles.loginText}>電話驗證</Text>
            <View style={styles.inputBlock}>
                <View style={styles.inputLine}>
                    <Text style={styles.inputText}>電話：</Text>
                    <TextInput
                        style={styles.input}
                        placeholder=" "
                        value={inputVerification}
                        onChangeText={text => setInputVerification(text)}
                    />
                </View>
                
            </View>
            
            <TouchableOpacity
                style={styles.loginButton}
                activeOpacity={0.7}
                onPress={() => {determin}}   //登入的按鈕在這
            >
                <Text style={styles.loginButtonText}>驗證</Text>
            </TouchableOpacity>

            <OTPInputView
                style={{ width: "80%", height: 200 }}
                pinCount={6}
                autoFocusOnLoad
                codeInputFieldStyle={styles.underlineStyleBase}
                codeInputHighlightStyle={styles.underlineStyleHighLighted}
                onCodeFilled={(code) => {
                    checkVerification(verificationCode, code).then((success) => {
                    if (!success) setInvalidCode(true);
                    success && navigation.replace("Home");
                    });
                }}
              />
     {invalidCode && <Text style={styles.error}>Incorrect code.</Text>}
        </View>
    )
}

const styles = StyleSheet.create({
    loginText: {
        fontSize: 32,
        color: color.darkBlue,
        marginTop: 80,
        textAlign: 'center',
        fontWeight: '700'
    },
    inputBlock: {
        alignItems: 'center',
        margin: 50,
        fontSize: 24,
        color: color.darkBlue
    },
    inputLine: {   
        flexDirection: 'row',
        alignItems: 'center',
        margin: 10
    },
    inputText: {
        fontSize: 16,
        color: color.darkBlue,
    },
    input: {
        borderBottomColor: color.darkBlue,
        borderBottomWidth: 3,
        width: 250,
        padding: 3
    },
    loginButton: {
        backgroundColor: color.blue,
        padding: 8,
        borderRadius: 5,
        width: 70,
        alignSelf: 'flex-end',
        marginTop: 50,
        marginRight: 50
    },
    loginButtonText: {
        color: color.white,
        textAlign: 'center',
        fontSize: 16
    },
    registerButton: {
        backgroundColor: 'white',
        padding: 8,
        borderRadius: 10,
        width: 150,
        alignSelf: 'flex-end',
        margin: 20,
        marginRight: 50
    },
    registerButtonText: {
        color: color.textyellow,
        fontSize: 14,
        borderBottomColor: color.textyellow,
        borderBottomWidth: 2,
        margin: 20,
        marginRight: 40,
        alignSelf: 'flex-end',
        width: 130
    },
    wrapper: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
      },
     
      borderStyleBase: {
        width: 30,
        height: 45,
      },
     
      borderStyleHighLighted: {
        borderColor: "#03DAC6",
      },
     
      underlineStyleBase: {
        width: 30,
        height: 45,
        borderWidth: 0,
        borderBottomWidth: 1,
        color: "black",
        fontSize: 20,
      },
     
      underlineStyleHighLighted: {
        borderColor: "#03DAC6",
      },
     
      prompt: {
        fontSize: 24,
        paddingHorizontal: 30,
        paddingBottom: 20,
      },
     
      message: {
        fontSize: 16,
        paddingHorizontal: 30,
      },
     
      error: {
        color: "red",
      }
})