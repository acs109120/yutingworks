<?php
    $conn = new mysqli("localhost", "root", "", "filmdatabase") or die("Database Error");

    // $check_data = "SELECT id, spead, title, duration, kind FROM movie";
    // $run_query=mysqli_query($conn,$check_data) or die("Error");
    
    // $_POST['all']="1";

    if (!empty($_POST['all'])){
        $check_data = "SELECT id, spead, title, time, duration, kind FROM movie ORDER BY time";
        $run_query=mysqli_query($conn,$check_data) or die("Error");
        show($run_query);
    }
    else if (!empty($_POST['active'])){
        $check_data = "SELECT id, spead, title, time, duration, kind FROM movie WHERE kind='動作' ORDER BY time";
        $run_query=mysqli_query($conn,$check_data) or die("Error");
        show($run_query);
    }
    else if (!empty($_POST['love'])){
        $check_data = "SELECT id, spead, title, time, duration, kind FROM movie WHERE kind='愛情' ORDER BY time";
        $run_query=mysqli_query($conn,$check_data) or die("Error");
        show($run_query);
    }
    else if (!empty($_POST['suspense'])){
        $check_data = "SELECT id, spead, title, time, duration, kind FROM movie WHERE kind='懸疑' ORDER BY time";
        $run_query=mysqli_query($conn,$check_data) or die("Error");
        show($run_query);
    }
    else if (!empty($_POST['horror'])){
        $check_data = "SELECT id, spead, title, time, duration, kind FROM movie WHERE kind='恐怖' ORDER BY time";
        $run_query=mysqli_query($conn,$check_data) or die("Error");
        show($run_query);
    }
    else if (!empty($_POST['animation'])){
        $check_data = "SELECT id, spead, title, time, duration, kind FROM movie WHERE kind='動畫' ORDER BY time";
        $run_query=mysqli_query($conn,$check_data) or die("Error");
        show($run_query);
    }
    else if (!empty($_POST['other'])){
        $check_data = "SELECT id, spead, title, time, duration, kind FROM movie WHERE kind NOT IN ('動作', '愛情', '懸疑', '恐怖', '動畫') ORDER BY time";
        $run_query=mysqli_query($conn,$check_data) or die("Error");
        show($run_query);
    }

    else{
        $check_data = "SELECT id, spead, title, time, duration, kind FROM movie ORDER BY time";
        $run_query=mysqli_query($conn,$check_data) or die("Error");
        show($run_query);
    }

    function show($run_query){
        if($run_query){
            if(mysqli_num_rows($run_query)>0){
                while($fetch_data = mysqli_fetch_assoc($run_query)){
                    echo "<div class='film'>";
                    echo "<img src=".$fetch_data['id'].".png>";
                    echo "<p>".$fetch_data['title']."</p>";
                    echo "<span class='kind'>".$fetch_data['kind']."</span><span>　片長：".$fetch_data['duration']."</span></p>";
                    echo "<p class='time'>放映時間：</p><p class='time'>".$fetch_data['time']."</p>";
                    echo "<input type='button' onclick=\"location.href='booking.php?id=". $fetch_data['id'] ."'\" value='訂票'>";
                    echo "<p>　　票價：".$fetch_data["spead"]."</p>";
                    echo "</div>";
                }
            }
        }
    }
    
    
?>