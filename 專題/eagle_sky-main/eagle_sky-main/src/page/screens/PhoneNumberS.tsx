import React, { useState, useRef } from "react";
import {
 SafeAreaView,
 StyleSheet,
 View,
 TouchableOpacity,
 Text,
 Modal,
 TextInput,
 Alert,
 Image
} from "react-native";
import { navigationType } from '../navigationType'
import { Colors } from "react-native/Libraries/NewAppScreen";
import PhoneInput from "react-native-phone-number-input";
import { sendSmsVerification } from "../../../api/verify";
import { color } from '../../style';
import OpenEye from '../../assets/OpenEye3.png' ;
import CloseEye from '../../assets/CloseEye3.png' ;
function generateRandom6DigitNumber(): string {
  const min = 100000; // 最小的 6 位数字
  const max = 999999; // 最大的 6 位数字

  const randomNumber = Math.floor(Math.random() * (max - min + 1)) + min;  
  return randomNumber.toString();
}
let verificationNumbers = "123456";


export default function PhoneNumberS({ navigation }: navigationType) {
 
 const [value, setValue] = useState("");
 const [formattedValue, setFormattedValue] = useState("");
 const [pswinput, setpswinput] = useState("");
 const [showPassword, setShowPassword] = useState(false);
 const phoneInput = useRef<PhoneInput>(null);
 verificationNumbers = generateRandom6DigitNumber();
 console.log("in verifyrandom",verificationNumbers);

const handleSomeEvent = () => {
  if (phoneInput.current) {
    // 使用 phoneInput.current.getValue() 來獲取輸入的值
    //const phoneNumber = phoneInput.current.getValue();
    console.log('formattedValue:', formattedValue);
    //console.log('value:', value);
  }
};
let secret="123465";

const HandleVerify = (phone:string  ,psw : string)=>{
  let phonenum: number = +phone;

  if(phone.trim()==''){
    Alert.alert("電話號碼不可空白")
  }else if(psw.trim()==''){
    Alert.alert("密碼不可空白")
  }else{
   //找手機號碼phonenum之密碼=secret
      if(psw != secret){
        Alert.alert("密碼錯誤 或 查無此帳號")
        console.log("psw wrong")
      }else{
        navigation.navigate("OTP", { phoneNumber: formattedValue});
        console.log("Go to verify phone ! ");
      }
  }
  
};

 return (
   
     <View style={styles.container}>
       <SafeAreaView style={styles.wrapper}>
       <View style={styles.welcome}>
        </View>

         <PhoneInput
           ref={phoneInput}
           defaultValue={value}
           defaultCode="TW"
           layout="first"
           onChangeText={(text) => {
             setValue(text);
           }}
           onChangeFormattedText={(text) => {
             setFormattedValue(text);
           }}
           countryPickerProps={{ withAlphaFilter: true }}
           withShadow
           autoFocus
         />
         <View style={styles.inputContainer}>
                <Text style={styles.word}>密碼   ：</Text>
                <TextInput
                      style={styles.input}
                      placeholder=""
                      value={pswinput} 
                      onChangeText={ (text) =>setpswinput(text)}
                      secureTextEntry={!showPassword}
                 />
                  <TouchableOpacity onPress={() => setShowPassword(!showPassword)}>
                  {showPassword ? (
                      <Image
                        style={{ width: 30, height: 30 }}
                        source={CloseEye}
                      />
                    ) : (
                      <Image  
                          //resizeMode={'cover'} 
                          style={{ width: 30, height: 30 }}
                          source={OpenEye}
                        /> 
                    )}
                  </TouchableOpacity>
         </View>
       
        

         <TouchableOpacity
            style={styles.loginButton}
            activeOpacity={0.7}
            onPress={() => {
              HandleVerify(value,pswinput)
              // sendSmsVerification(formattedValue,verificationNumbers).then((sent) => {
              //   navigation.navigate("OTP", { phoneNumber: formattedValue ,verificationNumbers:verificationNumbers});
              //   console.log("Sent!");
              // });
              // sendSmsVerification(formattedValue).then((sent) => {
              //   navigation.navigate("OTP", { phoneNumber: formattedValue});
              //   console.log("Sent!");
              // });
              
            }}
          >
           <Text style={styles.loginButtonText}>登入</Text>
         </TouchableOpacity>
         <TouchableOpacity
                // style={styles.registerButton}
                activeOpacity={0.7}
                onPress={() => {navigation.navigate("Register")}}
            >
                <Text style={styles.registerButtonText}>第一次使用－－註冊</Text>
            </TouchableOpacity>
       </SafeAreaView>
     </View>
   
 );
};

const styles = StyleSheet.create({
  loginText: {
    fontSize: 32,
    color: color.darkBlue,
    marginTop: 80,
    textAlign: 'center',
    fontWeight: '700'
},
inputBlock: {
    alignItems: 'center',
    margin: 50,
    fontSize: 24,
    color: color.darkBlue
},
inputLine: {   
    flexDirection: 'row',
    alignItems: 'center',
    margin: 10
},
inputText: {
    fontSize: 16,
    color: color.darkBlue,
},
input: {
    borderBottomColor: color.darkBlue,
    borderBottomWidth: 3,
    width: 230,
    padding: 3
},
loginButton: {
    backgroundColor: color.blue,
    padding: 8,
    borderRadius: 5,
    width: 70,
    alignSelf: 'flex-end',
    marginTop: 50,
    marginRight: 50
},
loginButtonText: {
    color: color.white,
    textAlign: 'center',
    fontSize: 16
},
registerButton: {
    backgroundColor: 'white',
    padding: 8,
    borderRadius: 10,
    width: 150,
    alignSelf: 'flex-end',
    margin: 20,
    marginRight: 50
},
registerButtonText: {
    color: color.textyellow,
    fontSize: 14,
    borderBottomColor: color.textyellow,
    borderBottomWidth: 2,
    margin: 20,
    marginRight: 40,
    alignSelf: 'flex-end',
    width: 130
},
 container: {
   flex: 1,
   backgroundColor: Colors.lighter,
 },

 wrapper: {
   flex: 1,
   justifyContent: "center",
   alignItems: "center",
 },

 button: {
   marginTop: 20,
   height: 50,
   width: 300,
   justifyContent: "center",
   alignItems: "center",
   backgroundColor: "#7CDB8A",
   shadowColor: "rgba(0,0,0,0.4)",
   shadowOffset: {
     width: 1,
     height: 5,
   },
   shadowOpacity: 0.34,
   shadowRadius: 6.27,
   elevation: 10,
 },

 buttonText: {
   color: "white",
   fontSize: 14,
 },

 welcome: {
   padding: 20,
 },

 status: {
   padding: 20,
   marginBottom: 20,
   justifyContent: "center",
   alignItems: "flex-start",
   color: "gray",
 },
 inputContainer: {
  flexDirection: 'row', // 水平排列
 marginTop:30,
  //marginTop:10,
  marginBottom: 1,
  textAlign: 'left',
},
word: {
  marginTop: 3,
  fontSize: 18,
  fontWeight: '700',
  color: color.darkBlue,
  flexDirection: 'row',

},
});


