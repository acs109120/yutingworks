import React, { useEffect } from 'react';
import { Component } from 'react';
import { color } from '../../style'
import { Image, StyleSheet, Text, TouchableOpacity, View, TextInput, Alert } from 'react-native';
import { navigationType } from '../navigationType'
import { changeName, changeEmail, changePassword } from './loadAndUpload'

let userId = 2;
let userPassword = "123";

export default function Home({route, navigation }: navigationType) {
    const kind = route.params ? route.params.kind : {kind:'ooo'}

    return (
        <View style={styles.body}>
            <View style={styles.block}>
                { kind=="changeName" && <ChangeName navigation = {navigation} />}
                { kind=="changePassword" && <ChangePassword navigation = {navigation} />}
                { kind=="changeMail" && <ChangeMail navigation = {navigation} />}
            </View>
        </View>
    )
}


function ChangeName({ navigation }: navigationType){
    const [name, typeName] = React.useState('');
    const [password, typePassword] = React.useState('');
    return(
        <View>
            <View style={styles.line}>
                <Text style={styles.text}>更改姓名：</Text>
                <TextInput
                    style={styles.textInput}
                    onChangeText={text => typeName(text)}
                    value={name}
                />
            </View>
            <View style={styles.line}>
                <Text style={styles.text}>　　密碼：</Text>
                <TextInput
                    style={styles.textInput}
                    onChangeText={text => typePassword(text)}
                    value={password}
                />
            </View>
            <View style={styles.buttonBlock}>
                <TouchableOpacity
                    style={styles.cancelButton}
                    activeOpacity={0.7}
                    onPress={() => {navigation.goBack()}}
                >
                    <Text style={styles.buttonText}>取消</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style={styles.sureButton}
                    activeOpacity={0.7}
                    onPress={()=>{
                        if(userPassword==password)
                            changeName(userId, name)
                        else if(!name)
                            Alert.alert("姓名不可為空")
                        else
                            Alert.alert("密碼錯誤")
                    }}
                >
                    <Text style={styles.buttonText}>確定更改</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

function ChangePassword({ navigation }: navigationType){
    const [oldPassword, typeOldPassword] = React.useState('');
    const [password, typePassword] = React.useState('');
    const [passwordSure, typePasswordSure] = React.useState('');
    return(
        <View>
            <View style={styles.line}>
                <Text style={styles.text}>舊密碼：</Text>
                <TextInput
                    style={styles.textInput}
                    onChangeText={text => typeOldPassword(text)}
                    value={oldPassword}
                />
            </View>
            <View style={styles.line}>
                <Text style={styles.text}>新密碼：</Text>
                <TextInput
                    style={styles.textInput}
                    onChangeText={text => typePassword(text)}
                    value={password}
                />
            </View>
            <View style={styles.line}>
                <Text style={styles.text}>確認新密碼：</Text>
                <TextInput
                    style={styles.textInput}
                    onChangeText={text => typePasswordSure(text)}
                    value={passwordSure}
                />
            </View>
            <View style={styles.buttonBlock}>
                <TouchableOpacity
                    style={styles.cancelButton}
                    activeOpacity={0.7}
                    onPress={() => {navigation.goBack()}}
                >
                    <Text style={styles.buttonText}>取消</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style={styles.sureButton}
                    activeOpacity={0.7}
                    onPress={()=>{
                        if(password==passwordSure && userPassword==password)
                            changePassword(userId, password)
                        else if(userPassword!=oldPassword)
                            Alert.alert("密碼錯誤")
                        else if(password!=passwordSure)
                            Alert.alert("新密碼不一致")
                        else if(!password)
                            Alert.alert("密碼不可為空")
                    }}
                >
                    <Text style={styles.buttonText}>確定更改</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

function ChangeMail({ navigation }: navigationType){
    const [mail, typeMail] = React.useState('');
    const [password, typePassword] = React.useState('');
    return(
        <View>
            <View style={styles.line}>
                <Text style={styles.text}>更改信箱：</Text>
                <TextInput
                    style={styles.textInput}
                    onChangeText={text => typeMail(text)}
                    value={mail}
                />
            </View>
            <View style={styles.line}>
                <Text style={styles.text}>　　密碼：</Text>
                <TextInput
                    style={styles.textInput}
                    onChangeText={text => typePassword(text)}
                    value={password}
                />
            </View>
            <View style={styles.buttonBlock}>
                <TouchableOpacity
                    style={styles.cancelButton}
                    activeOpacity={0.7}
                    onPress={() => {navigation.goBack()}}
                >
                    <Text style={styles.buttonText}>取消</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style={styles.sureButton}
                    activeOpacity={0.7}
                    onPress={()=>{
                        if(userPassword==password)
                            changeEmail(userId, mail)
                        else if(!mail)
                            Alert.alert("信箱不可為空")
                        else
                            Alert.alert("密碼錯誤")
                    }}
                >
                    <Text style={styles.buttonText}>確定更改</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    body: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    block: {
        backgroundColor: color.backgroundColor,
        width: '80%',
        borderRadius: 5,
        paddingHorizontal: 25,
        paddingVertical: 10
    },
    line: {
        flexDirection: 'row',
        marginVertical: 5,
        alignItems: 'center'
    },
    text: {
        fontSize: 16,
        color: color.darkBlue,
    },
    textInput: {
        borderBottomWidth: 3,
        borderColor: color.darkBlue,
        padding: 2,
        flex: 1
    },
    buttonBlock: {
        flexDirection: 'row',
        gap: 10,
        justifyContent: 'flex-end'
    },
    cancelButton: {
        backgroundColor: color.blue,
        width: 50,
        height: 30,
        borderRadius: 5,
        alignSelf: 'flex-end',
        marginTop: 50,
        marginBottom: 20
    },
    sureButton: {
        backgroundColor: color.red,
        width: 80,
        height: 30,
        borderRadius: 8,
        alignSelf: 'flex-end',
        marginTop: 50,
        marginBottom: 20
    },
    buttonText:{
        color: "white",
        textAlign: 'center',
        alignSelf: 'center',
        lineHeight: 30
    }
})

