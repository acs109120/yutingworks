<!DOCTYPE html>

<head>
    <meta charset="UTF-5">
    <link rel="stylesheet" href="bookingSuccess.css">
    <meta name="viewport" content="width=device-width">
</head>

<body>
    <header>
        <h2>影城系統</h2>
            <input type="button" onclick="location.href='login.php'" value="登出">
            <input type="button" onclick="location.href='prf.php'" value="訂票資料">
            <input type="button" value="客服機器人" onclick="location.href='chatbot.html'">
    </header>
    <br>
    <br>
    <main>
        <h1>訂票成功！</h1>
        <br>
        <?php
            session_start();
            $name=$_SESSION['name'];
            echo "<p>" . $name . "，感謝訂票</p>";
        ?>
        <p>請在上映前十分鐘到場領票</p>
        <br>
        <br>
        <br>
        <br>
        <div>
            <input type="button" onclick="location.href='menuLogin.php'" value="回首頁">
            <input type="button" onclick="location.href=location.href='booking.php?id=<?php echo $_GET['id']?>'" value="返回訂票">
        </div>
    </main>
</body>       