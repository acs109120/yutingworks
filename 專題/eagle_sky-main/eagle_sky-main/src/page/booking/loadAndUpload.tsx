import { http, sqlURL } from '../../../http'
import qs from 'qs'

class Area {
    id: number = 0;
    area: string = "Z";
    seat_total: number = 0;
    price: number = 0
}

function setQuery(concertId: number = 2){
    let query = qs.stringify({
        filters: {
            concert_all: {
                id:{
                    '$eq': concertId
                }
            }
        }
    })
    return query
}

let areas : Area[] = []

async function loadAreas(concertId: number = 2){
    await http.get('/api/areas?populate=concert_all&'+setQuery(concertId)).then(res => {
        areas = res.data.data.map((item: any) => {
            return ({
                id: item.id,
                area: item.attributes.area,
                seat_total: item.attributes.seat_total,
                price: item.attributes.price
            })
        })
        areas = [...areas]
    })
    return areas;
}

async function buyTicket(areaId: number){
    let seat = 0;
    await http.get('/api/areas/' + areaId.toString()).then((res) => {
        seat = res.data.data.attributes.seat_total
    })
    await http.put('api/areas/' + areaId.toString(), {
        data:{
            seat_total: (seat-1)
        }
    })
}


let resellAreas : Area[] = [];
async function loadResellAreas(concertId: number = 2){
    await http.get('/api/areas?populate=concert_all&'+setQuery(concertId)).then((res) => {
        resellAreas = res.data.data.map((item: any) => {
            return {
                id: item.id,
                area: item.attributes.area,
                seat_total: item.attributes.seat_resell_total,
                price: item.attributes.price
            }
        })
        resellAreas = [...resellAreas]
    })
    return resellAreas;
}

async function buyResellTicket(areaId: number){
    let seat = 0;
    await http.get('/api/areas/' + areaId.toString()).then((res) => {
        seat = res.data.data.attributes.seat_resell_total
    })
    await http.put('api/areas/' + areaId.toString(), {
        data:{
            seat_resell_total: (seat-1)
        }
    })
}

export {loadAreas, areas, buyTicket,
        loadResellAreas, resellAreas, buyResellTicket}