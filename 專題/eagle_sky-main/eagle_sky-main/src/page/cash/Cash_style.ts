import { StyleSheet } from 'react-native';
import { color } from '../../style'

export const styles = StyleSheet.create({
    background: {
        paddingTop: 10,
        paddingHorizontal: 25,
        flex: 1,
        height: "100%",
        backgroundColor: color.backgroundColor,
    },
    container:{
        marginTop: 15,
        paddingHorizontal: 0,
    },
    header: {
        marginTop: 5,
        fontSize: 24,
        fontWeight: '700',
        color: color.darkBlue,
        textAlign: 'center', // 文字水平居中
        textAlignVertical: 'center', // 文字垂直居中（僅Android支持）
    },
    title: {
        fontSize: 18,
        fontWeight: '700',
        color: color.darkBlue,
    },
    text: {
        marginTop: 25,
        fontSize: 15,
        fontWeight: '600',
        color: color.darkBlue,
        textAlign:'right',
    },
    word: {
        marginTop: 3,
        fontSize: 15,
        fontWeight: '600',
        color: color.darkBlue,
        flexDirection: 'row',
    },
    button: {
        backgroundColor: color.red,
        width:'20%',
        borderRadius: 10,
        justifyContent: "flex-end",
        alignItems: "center",
        alignSelf: 'flex-end',
        paddingTop:3,
        paddingBottom:7,
    },
    button_text: {
        fontSize: 18,
        fontWeight: '700',
        color: "white",
    },
    line: {
        height: 3,
        width: '100%',
        backgroundColor: color.lightBlue,
        flex: 1,
        padding: 3,
      },
    input: {
        height: 12,
        width: '80%',
        borderBottomWidth: 2,
        borderColor: color.darkBlue,
        marginBottom: 1,
        fontSize: 12,
        flexDirection: 'row',
    },
    inputContainer: {
        flexDirection: 'row', // 水平排列
        flex:1,
        marginBottom: 1,
        textAlign: 'left',
    },
    notificationContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: color.backgroundColor,
    },
    notification: {
        backgroundColor: color.purplegray,
        padding: 20,
        borderRadius: 10,
        fontSize: 20,
    },
});
