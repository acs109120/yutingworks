import React, { useState,useEffect,useRef } from 'react';
import { Alert,Button, View, StyleSheet, Text, TextInput, Image, TouchableOpacity, FlatList } from 'react-native';
import { navigationType } from '../navigationType'
import { color } from '../../style'
import {register} from './update';
import { useRoute } from '@react-navigation/native';
import OpenEye from '../../assets/OpenEye3.png' ;
import CloseEye from '../../assets/CloseEye3.png' ;
import PhoneInput from "react-native-phone-number-input";
export default function RegisterOTP({ navigation }: navigationType){
    const route = useRoute();
    const [value, setValue] = useState("");
    const [formattedValue, setFormattedValue] = useState("");
    const [phoneNumber, setPhoneNumber] = React.useState('');
    const [phoneNumberverify, setPhoneNumberverify] = React.useState('');
   
    const [email, setEmail] = React.useState('')
    const [emailverify, setEmailverify] = React.useState('')
    const [showPhone, setShowPhone] =  React.useState(false);
    const [showEmail, setShowEmail] =  React.useState(false);
    const phoneInput = useRef<PhoneInput>(null);
    const [isOkPhone,setisOkPhone] = useState(false);
    const [isOkEmail,setisOkEmail] = useState(false);
    let name = route.params && route.params.name;
    let password = route.params && route.params.password;

    let defaultphoneverify = "123456";
    let defaultmailverify = "123456";

    const handleverify=(phoneverify:string,mailverify:string)=>{
        if(phoneverify.trim() == defaultphoneverify){
            setisOkPhone(true);
            console.log("isOkPhone ",isOkPhone)
        }
        if(mailverify.trim() == defaultmailverify){
            setisOkEmail(true);
            console.log("isOkEmail ",isOkEmail)
        }

        if(!isOkPhone ){   
            Alert.alert("電話尚未完成驗證")  
        }else if(!isOkEmail ){
            Alert.alert("電子郵件尚未驗證完畢")
        } else{
            Alert.alert("註冊成功，請1分鐘後再登入，資料寫入資料庫中")

            register(formattedValue,name.toString(),email,password.toString())

            navigation.navigate("PhoneNumberS");
            console.log("email",email)
            console.log("emailverify",emailverify)
            console.log("formattedValue",formattedValue)
            console.log("phoneNumberverify",phoneNumberverify)
        }
    }

    const handlecreate = (name:string , password:string ,phone:string, email:string,phoneverify:string,mailverify:string) => {
        if( phone.trim() ==='' || email.trim() === ''){
            Alert.alert("資料勿空白")
        }else{
            handleverify(phoneverify,mailverify);  
        }
    }

    const handlephone = (phonenum:string) => {
        if(phonenum.trim() === ''){
            Alert.alert("請輸入欲綁定之電話號碼")
        }else{
            Alert.alert("已發送驗證碼")
        }

    }
    const handleemail = (mail:string) => {
        if(mail.trim() === ''){
            Alert.alert("請輸入欲綁定之電子郵件")
        }else{
            Alert.alert("已發送驗證碼")
        }
    }

    return (
        <View>
            <Text style={styles.loginText}>註冊</Text>
            <View style={styles.inputBlock}>
               <View style={styles.inputLine}>
                    <PhoneInput
                        ref={phoneInput}
                        defaultValue={value}
                        defaultCode="TW"
                        layout="first"
                        onChangeText={(text) => {
                            setValue(text);
                        }}
                        onChangeFormattedText={(text) => {
                            setFormattedValue(text);
                            setisOkPhone(false);
                        }}
                        countryPickerProps={{ withAlphaFilter: true }}
                        withShadow
                        autoFocus
                    />
                
                    <TouchableOpacity
                        style={styles.verifyButton}
                        activeOpacity={0.7}
                        onPress={() => {
                            
                            handlephone(value);
                        }}   
                    >
                        <Text style={styles.verifyButtonText}>驗證</Text>
                    </TouchableOpacity>
                </View> 
                

                <View style={styles.inputLine2}>
                        <Text style={styles.inputText}>驗證碼  ：</Text>
                        <TextInput
                            style={styles.input2}
                            placeholder="   6位數驗證碼"
                            value={phoneNumberverify} 
                            onChangeText={ (text) =>{
                                setPhoneNumberverify(text);
                               
                            }}
                            secureTextEntry={!showPhone}
                        />
                        <TouchableOpacity onPress={() => setShowPhone(!showPhone)}>
                        {showPhone ? (
                            <Image
                                style={{ width: 30, height: 30 }}
                                source={CloseEye}
                            />
                            ) : (
                            <Image  
                                //resizeMode={'cover'} 
                                style={{ width: 30, height: 30 }}
                                source={OpenEye}
                                /> 
                            )}
                        </TouchableOpacity>
                </View>
                
                <View style={styles.inputLine}>
                    <Text style={styles.word}>Email  ：</Text>
                    <TextInput
                        style={styles.input}
                        placeholder=" "
                        value={email}
                        onChangeText={text => {
                            setisOkEmail(false);
                            setEmail(text)}}
                    />
                    <TouchableOpacity
                        style={styles.verifyButton}
                        activeOpacity={0.7}
                        onPress={() => {
                            handleemail(email)
                           
                        }}   
                    >
                        <Text style={styles.verifyButtonText}>驗證</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.inputLine2}>
                    <Text style={styles.inputText}>驗證碼  ：</Text>
                    <TextInput
                        style={styles.input2}
                        placeholder="  6位數驗證碼"
                        value={emailverify}
                        onChangeText={text => setEmailverify(text)}
                        secureTextEntry={!showEmail}
                    />
                    <TouchableOpacity onPress={() => setShowEmail(!showEmail)}>
                        {showEmail ? (
                            <Image
                                style={{ width: 30, height: 30 }}
                                source={CloseEye}
                            />
                            ) : (
                            <Image  
                                //resizeMode={'cover'} 
                                style={{ width: 30, height: 30 }}
                                source={OpenEye}
                                /> 
                            )}
                        </TouchableOpacity>
                </View>
            </View>
            <TouchableOpacity
                style={styles.registerButton}
                activeOpacity={0.7}
                onPress={() => {
                    handlecreate(name,password,formattedValue,email,phoneNumberverify,emailverify)
                    
                }}   //註冊的按鈕在這
            >
                <Text style={styles.registerButtonText}>註冊</Text>
            </TouchableOpacity>
            
        </View>
    )
}

const styles = StyleSheet.create({
    loginText: {
        fontSize: 32,
        color: color.darkBlue,
        marginTop: 80,
        textAlign: 'center',
        fontWeight: '700'
    },
    inputBlock: {
        alignItems: 'center',
        margin: 35,
        fontSize: 24,
        color: color.darkBlue
    },
    inputLine: {   
        flexDirection: 'row',
        alignItems: 'center',
        marginTop:25,
        margin: 10,
    },
    inputLine2: {   
        flexDirection: 'row',
        alignItems: 'center',
        margin: 8
    },
    inputText: {
        fontSize: 16,
        color: color.darkBlue,
    },
    input: {
        borderBottomColor: color.darkBlue,
        borderBottomWidth: 3,
        width: 250,
        padding: 3
    },
    input2: {
        borderBottomColor: color.darkBlue,
        borderBottomWidth: 3,
        width: 205,
        padding: 3
    },
    verifyButton: {
        backgroundColor: color.blue,
        padding: 1,
        borderRadius: 5,
        width: 20,
        alignSelf: 'flex-end',
        //marginTop: 40,
        marginLeft: 10
    },
    verifyButtonText: {
        color: color.white,
        textAlign: 'center',
        fontSize: 12
    },
    registerButton: {
        backgroundColor: color.blue,
        padding: 8,
        borderRadius: 5,
        width: 70,
        alignSelf: 'flex-end',
        marginTop: 40,
        marginRight: 50
    },
    registerButtonText: {
        color: color.white,
        textAlign: 'center',
        fontSize: 16
    },
    loginButton: {
        backgroundColor: 'white',
        padding: 8,
        borderRadius: 10,
        width: 150,
        alignSelf: 'flex-end',
        margin: 20,
        marginRight: 50
    },
    loginButtonText: {
        color: color.textyellow,
        fontSize: 14,
        borderBottomColor: color.textyellow,
        borderBottomWidth: 2,
        margin: 20,
        marginRight: 40,
        alignSelf: 'flex-end',
        width: 130
    },
    inputContainer: {
        flexDirection: 'row', // 水平排列
       marginTop:30,
        //marginTop:10,
        marginBottom: 1,
        textAlign: 'left',
      },
      word: {
        marginTop: 3,
        fontSize: 18,
        fontWeight: '700',
        color: color.darkBlue,
        flexDirection: 'row',
      
      },
})