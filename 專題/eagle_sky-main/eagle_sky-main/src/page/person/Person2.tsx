/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */
import { color } from '../../style'
import { navigationType } from '../navigationType'
import React, {useState, useEffect ,Component } from 'react';
import {
  Alert,
  FlatList,
  SafeAreaView,
  TextInput,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Button,
  TouchableOpacity,
} from 'react-native';
import { useNavigation } from '@react-navigation/native';
import {createGroup, getAccountName, accountName,addGroupMember, 
  getTicketConcertAreas,getGroups,groups} from './loadAndUpload';
import { CheckNameOther} from './Person2Check';
import { userId } from '../../user';

export default function Home({ navigation }: navigationType): JSX.Element {
  const isDarkMode = useColorScheme() === 'dark';
  const backgroundStyle = {
    backgroundColor: color.backgroundColor,
  };

  const AddButton= ({ title, onPress = () => { } }) => {
    return (
      <TouchableOpacity style={styles.BtnContainer2}
        activeOpacity={0.8}
        onPress={onPress}>
          <Text style={styles.Title2}>{title}</Text>
      </TouchableOpacity>
    )
  }
  const EstablishButton = ({ title, onPress = () => { } }) => {
    return (
        <TouchableOpacity
            activeOpacity={0.8}
            onPress={onPress}
        >
            <View style={styles.BtnContainer}>
                <Text style={styles.Title}>{title}</Text>
            </View>
        </TouchableOpacity>
    )
  }
  class Concert {
    id: number = 0;
    concertName: string = "";
    areaName: string = "";
  }
  class Group {
    id: number = 0;
    name: string = "";
    concert: string = "";
    areaId: number = 0;
    accounts: {
        id: number;
        name: string;
    }[] = [];
}
  //拿account內所有資料
  let [showAccoutGroups, updateAccoutGroups] = React.useState([]  as Group[]);
      useEffect(() => {
          Promise.all([getGroups()]).then(() =>
              updateAccoutGroups(groups)
          )
      }, [])
  //console.log("getGroups:",showAccoutGroups[0]);

    //拿account有的票之演唱會
  let [showConcerts, updateConcerts] = React.useState([]  as Concert[]);
      useEffect(() => {
        getTicketConcertAreas().then((concerts) => {
          const simplifiedConcerts = concerts.map((concert) => ({
            id: concert.id,
            concertName: concert.concertName,
            areaName:concert.areaName,
          }));
          updateConcerts(simplifiedConcerts);
        });
      }, []);

      
  const checkGroup=() => {
      let canestablish:boolean=true;
      if( selectedConcertId == null){
          Alert.alert("請選擇欲創建之演唱會群組");
      }else{
          for(let x=0; x<showAccoutGroups.length;x++){
            // console.log(showAccoutGroups[x].areaId)
            //console.log("chosse",selectedConcertId)
            if(showAccoutGroups[x].areaId == selectedConcertId ){
                Alert.alert("已有此演唱會群組 無法建立");
                canestablish =false;
            }
          }
          if(canestablish){
            handleCreateGroup();
          }
            
      }  
  }
  
  let selectedConcertId:number
  const GroupDropdown = () => {

      //console.log("下拉選單: ",showConcerts);

      const [selectedGroup, setSelectedGroup] = useState('');
      const [isDropdownVisible, setIsDropdownVisible] = useState(false);

      const handleGroupSelect = (option) => {
        setSelectedGroup(option);
        setIsDropdownVisible(false);

        selectedConcertId = option.id;
        
      };


      const toggleDropdown = () => {
          setIsDropdownVisible(!isDropdownVisible);
      };

      return (
        <View style={styless.container}>
                <TouchableOpacity style={styless.dropdownButton} onPress={toggleDropdown}>
                    <View style={styless.block}>
                    <Text style={styless.selectedGroupText}> {selectedGroup.concertName || '預綁定之演唱會 : 請選擇 '}</Text>

                  <View style={isDropdownVisible ? styless.triangle_up : styless.triangle_down}  />
                  </View>
                </TouchableOpacity>

                <Text style={{ height: 50 }} />
          {isDropdownVisible && (
                <View style={[styless.optionList, styless.dropdownPosition]}>
                  <FlatList
                    data={showConcerts}
                    renderItem={({ item }) => (
                      <TouchableOpacity style={styless.optionButton} onPress={() => handleGroupSelect(item)}>
                        <Text style={styless.optionText}>{item.concertName}</Text>
                      </TouchableOpacity>
                    )}
                    keyExtractor={(item) =>item.id.toString()}
                    nestedScrollEnabled
                  />
                </View>
              )}
        </View>
      );
  };

  const [groupInfo, setGroupInfo] = useState({
     groupName: '',  //string
     members: [],   //array 下面再接id name ...
   });               //這裡改了初始設null 發現剛開頁面初始框會變2個 先不改
   const [finalGroup, setfinalGroup] = useState({
    groupName: '',  //string
    members: [],   //array 下面再接id name ...
  });
  const [establishButtonCount, setEstablishButtonCount] = useState(0);
  const [submittedData, setSubmittedData] =  useState({
    groupName: '',  //string
    members: [],   //array 下面再接id name ...
  });               //這裡改了初始設null 發現剛開頁面初始框會變2個 先不改

  const navigations = useNavigation();
  const reloadPage = () => {
    navigations.navigate('Person');
    console.log("reload");
  };
  const handleAddMember = () => {   // 點'新增成員'時會被調用 ，newmerber會接在原members後面

    if(establishButtonCount < 2){
        setEstablishButtonCount(prevCount => prevCount + 1);

        const newMember = { id: '', name: '' };
        setGroupInfo((prev) => ({ ...prev, members: [...prev.members, newMember] }));
    }else{
        Alert.alert('已達人數上限4人');
    }
  };



  const handleMemberInputChange = (text:string, index:number, field:string) => { //處理文字輸入框中的文字變化
    const newMembers = [...groupInfo.members]; //創建member訊息的副本
    // console.log("newMembers before update:", newMembers); 
    // 检查 index 是否有效
    if (index >= 0 && index < newMembers.length) {
      newMembers[index][field] = text;
      //console.log("index: ",index)
      setGroupInfo((prev) => ({ ...prev, members: newMembers }));
    } else {
      //console.log(" newMembers.length", newMembers.length);
      console.error(`Invalid index ${index}`);
    }
  };


  const handleCreateGroup = async () => {
      // 定義初始成員
      const newMember = { id: String(firstID || ''), name: firstName || '' };
    
      // 更新成員列表
      const updatedMembers = [...groupInfo.members, newMember];
    
      // 更新 finalGroup
      const newFinalGroup = { ...finalGroup };
      newFinalGroup.members = [...newFinalGroup.members, ...updatedMembers];
      setfinalGroup(newFinalGroup);
      console.log("groupInfo.groupname",groupInfo.groupName);
      console.log("finalGroup",newFinalGroup);
      // 複製 groupInfo 以提交數據
      const submittedData = { ...groupInfo };
      setSubmittedData(submittedData);
    
      // 初始化變數
      let result = "";
      let repeatname = true;
      let okname = true;
      let nullname = true;
    
      // 創建一個函數來處理每個成員的異步操作
      const processedIds = new Set();
      const processMember = async (member) => {
      const memberIdAsNumber = parseInt(member.id, 10);
    
        try {
          if (member.id === null || member.name === null || member.name.trim() === '') {
            nullname=false;
            //console.log("資料不可空白")
          }
        
          if (processedIds.has(memberIdAsNumber)) {
            repeatname = false;
          } else {
            processedIds.add(memberIdAsNumber);
          }

          if(!nullname ){
            Alert.alert('資料請勿空白');
          }else if( !repeatname){
            Alert.alert('不可重複新增');
          }else{
            // 使用 Promise.all 等待所有異步操作完成
            const [other, accountName] = await Promise.all([
              CheckNameOther(memberIdAsNumber),
              getAccountName(memberIdAsNumber),
            ]);
      
            //console.log("other ", other);
            //console.log("id:", memberIdAsNumber);
            //console.log("name:", accountName);
          }

          
          if (accountName != member.name) {
            result +=
              " ID:" +
              member.id +
              " 對應之名字" +
              member.name +
              " 與" +
              accountName +
              "不相符 ";
            okname = false;
          }

          // 處理完異步操作後
        } catch (error) {
          console.error("處理異步操作時出錯：", error);
        }
      };
    
      try {
        // 使用 Promise.all 等待所有成員的異步操作完成
        await Promise.all(newFinalGroup.members.map(processMember));
    
        
        if (!nullname ) {
          //Alert.alert('資料請勿空白');
        } else if (!repeatname ) {
          //Alert.alert('不可重複新增');
        } else if (!okname ) {
          Alert.alert(result);
        } else {
      
          const newGroupId = await createGroup(groupInfo.groupName, userId, selectedConcertId);

          for (const member of newFinalGroup.members) {
            const receiverId = member.id;
            await addGroupMember(newGroupId, receiverId, userId);
          }
    
          Alert.alert('建立成功 已發送邀請');
          deleteThing();
        }
    
        // 清空 finalGroup
        setfinalGroup({
          groupName: '',
          members: [],
        });

      } catch (error) {
        console.error("等待所有異步操作完成時出錯：", error);
      }
  
    
  };
  

  const deleteThing =  () => {

    setGroupInfo({
      groupName: '',
      members: [],
    });

    setFirstID('');
    setFirstName('');
    setEstablishButtonCount(0); //+次數歸零
    reloadPage();
  }

  const determinestablish = () =>{ //按建立
        //handleCreateGroup();
        for(let x=0; x<showAccoutGroups.length;x++){
          if(showAccoutGroups[x].areaId == selectedConcertId ){
              Alert.alert("已有群組 無法建立");
          }else{
            handleCreateGroup();
          }
    }
        //showTip();
  }
  const showTip = () => {
    Alert.alert('建立群組',groupInfo.groupName)
    reloadPage();
}
//////////////////////
const [firstID, setFirstID] = useState('');
const [firstName, setFirstName] = useState('');

  const handleFirstID = (text:string) => {
    setFirstID(text);
  };

  const handleFirstName = (text:string) => {
    setFirstName(text);
  };

  ///////////////////////////



  return (
    <SafeAreaView style={backgroundStyle}>
      <StatusBar
        barStyle={isDarkMode ? 'light-content' : 'dark-content'}
        backgroundColor={backgroundStyle.backgroundColor}
      />
      <Text style={{ height: 25 }} />
       <GroupDropdown />
       <Text style={{ height: 10 }} />
      <ScrollView
        contentInsetAdjustmentBehavior="automatic">
        <View style={styles.divin}>

            <Text style={{ height: 20 }} />
            <Text style={styles.smallTitle1}>群組名稱 : </Text>
            <TextInput
                style={styles.line1}
                placeholder=" "
                value={groupInfo.groupName}
                onChangeText={(text) => setGroupInfo((prev) => ({ ...prev, groupName: text }))}
            />

        <Text style={styles.smallTitle1}>新增成員 : </Text>
        </View>
        <View style={styles.divmem1}>

          <View style={styles.inputContainer}>
            <Text style={styles.word}>I D    ：</Text>
            <TextInput
                  style={styles.input}
                  placeholder=""
                  value={firstID}
                  onChangeText={ handleFirstID}

             />
          </View>
          <Text style={{height: 5}}/>
          <View style={styles.inputContainer}>
            <Text style={styles.word}>姓名 ：</Text>
            <TextInput
                  style={styles.input}
                  placeholder=""
                  value={firstName}
                  onChangeText={ handleFirstName}
            />
          </View>

        </View>

        {groupInfo.members.map((member, index) => (  //index 從0開始 ，每增加一個框+1
          <View key={index} style={styles.divmem1}>

              <View style={styles.inputContainer}>
                <Text style={styles.word}>I D    ：</Text>
                <TextInput
                      style={styles.input}
                      placeholder=""
                      value={member.id}  //若上先設好初始有id name 一開始頁面會有兩個框框
                      onChangeText={ (text) =>handleMemberInputChange(text, index, 'id')}

                 />
              </View>
              <Text style={{height: 5}}/>

              <View style={styles.inputContainer}>
                <Text style={styles.word}>姓名 ：</Text>
                <TextInput
                      style={styles.input}
                      placeholder=""
                      value={member.name}   //若上先設好初始有id name 一開始頁面會有兩個框框
                      onChangeText={ (text) =>handleMemberInputChange(text, index, 'name')}
                />

              </View>

          </View>
        ))}
        <AddButton title="+"
                onPress={handleAddMember}
        />


        <View style={styles.divin}>
          <EstablishButton title="建立"
                onPress={checkGroup}
                  

          />
          <Text>{'\n'}</Text>
          <Text style={{ height: 100 }} />



        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  line1:{   //字下方的線
    height: 30,
    padding:2,
    width: '100%',
    borderBottomWidth: 3,
    borderColor: color.blue,
    marginBottom: 20,
    fontSize: 17,
    flexDirection: 'row',
    letterSpacing:1,
  },
  divin:{  //最外層框
    //marginHorizontal: 2,
        borderRadius: 15,

        padding: 5,
        marginTop:10,
        marginLeft:30,
        marginRight:30,
        marginButtom:0,
        //backgroundColor: color.backgroundColor ,
        paddingHorizontal: 20,
  },

  divmem1:{  //下方新增成員之框
      borderRadius: 10,
        marginBottom: 10,
        //padding:5,
        backgroundColor:color.lightBlock,
        //alignItems: 'center',
        padding: 15,
        //paddingBottom:0,
        marginTop:5,
        //margin:5,
        marginLeft:50,
        marginRight:39,
        flex:1,
        //backgroundColor: color.backgroundColor ,
        paddingHorizontal: 10,
  },

smallTitle1: {
  fontSize: 14,
  fontWeight: '600',
  color: '#000000',
},
smallTitle2: {
  fontSize: 16,
  marginTop: 25,
  fontWeight: '400',
  color: color.darkBlue,
  flexDirection: 'row',
},
  Title: { color:'white', fontWeight: 'bold', fontSize: 14 }, //button本身設定
  Title2: { color:color.darkBlue, fontWeight: '300',fontSize: 35, marginTop: -6 }, //+button大小設定
BtnContainer: {  //'建立' 按鈕
    backgroundColor: color.blue,
    //height: 40,
    width:60,
    borderRadius: 10,
    justifyContent: "center",
    alignItems: "center",
    alignSelf: 'flex-end',
    paddingTop:3,
    paddingBottom:7,
},
BtnContainer2 : {  //'+'按鈕
  //backgroundColor: color.blue,
  //height: 40,
  width:40,
  height:40,
  borderWidth:1,
  // textAlign: 'center', // 文字水平居中
  // textAlignVertical: 'center', // 文字垂直居中（僅Android支持）
  margin:15,
   //justifyContent: "center",
   alignItems: "center",
   alignSelf: 'center',
  //padding:5,
  borderRadius: 50,
  borderStyle:'dashed',
  borderColor: color.darkBlue
},
  input: {
    height: 25,
    width: '75%',
    borderBottomWidth: 3,
    borderColor: color.blue,
    marginBottom: 7,
    fontSize: 15,
    flexDirection: 'row',

    padding:2,
},
inputContainer: {
    flexDirection: 'row', // 水平排列
    flex:1,
    //marginTop:10,
    marginBottom: 1,
    textAlign: 'left',
},
word: {
  marginTop: 3,
  fontSize: 16,
  fontWeight: '500',
  color: color.darkBlue,
  flexDirection: 'row',

},
});
const styless = StyleSheet.create({
  container: {
    alignSelf: 'stretch',
    paddingHorizontal: 15,
  },
  dropdownButton: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'space-between',
    backgroundColor: color.blue,
    borderBottomWidth: 3,
    borderBottomColor: color.white,
    padding: 10,
    width: '90%', // 使用百分比設定寬度
    //aspectRatio: 8 / 1, // 設定寬高比例為 4:1
    alignSelf: 'center', // 水平置中
    borderRadius: 5,
    position: "absolute",
    zIndex: 2,
  },
  block: {
    //marginLeft: 20,
    //bottom: 10,
    backgroundColor: color.blue,
    width: '100%', // 使用百分比設定寬度
    //aspectRatio: 5 / 1, // 設定寬高比例為 4:1
    alignSelf: 'center', // 水平置中
    borderRadius: 5,
    flexDirection: 'row',
  },
  selectedGroupText: {
    fontSize: 15,
    fontWeight: '600',
    color: '#FFFFFF',
    flex:1,
    letterSpacing:1.5,
  },
  optionList: {
    padding: 5,
    borderWidth: 1,
    backgroundColor: color.blue,
    borderColor: '#FFFFFF',
    borderRadius: 0,
    marginTop:5,
  },
  optionButton: {
    padding: 10,
    borderBottomWidth: 1,
    borderBottomColor: color.white,
  },
  optionText: {
    fontSize: 15,
    fontWeight: '600',
    color: color.white,
  },
  triangle_up:{
    width:0,
    height:0,
    borderStyle:'solid',
    borderWidth:5,
    borderBottomWidth:10,
    borderLeftColor: 'transparent',
    borderTopColor: 'transparent',
    borderBottomColor:color.white,
    borderRightColor: 'transparent',

  },
  triangle_down:{
      width:0,
      height:0,
      borderStyle:'solid',
      borderWidth:5,
      borderTopWidth:10,
      borderLeftColor: 'transparent',
      borderTopColor:color.white,
      borderBottomColor: 'transparent',
      borderRightColor: 'transparent',
      top:7,
  },
  dropdownPosition: {
      position: 'absolute',
      zIndex: 2,
      marginTop: 40,
      alignSelf: 'center', // 水平置中
      width:'90%',
  },
});