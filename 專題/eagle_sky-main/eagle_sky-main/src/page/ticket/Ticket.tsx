/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */
import React, {useEffect} from 'react';
import { StackNavigationProp } from '@react-navigation/stack';
import { navigationType } from '../navigationType'
import { color } from '../../style'
import TicketCard from './TicketCard';
import { loadTickets, tickets } from './load';

import { StyleSheet, Text, View, FlatList } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';

class Ticket {
    id: number = 0;
    seat: any = "";
    pay_finished : boolean = false;
    sell_waiting : boolean = false;
    area: string = "Z";
    areaId: number = 0;
    concertId : number = 0;
    concert: string = "演唱會名稱"
    date: string = "";
    time_start: string = "";
    time_end: string = "";
    img: string = "";
}

export default function Home({ navigation }: navigationType) {
    let [showTickets, updateTickets] = React.useState([]  as Ticket[]);
    useEffect(() => {
        Promise.all([loadTickets()]).then(() =>
            updateTickets(tickets)
        )
    }, [])
    return (
        <View style = {styles.body}>
            <FlatList
                ListHeaderComponent = {() => {return <Text style = {styles.title}>票套</Text>}}
                data={showTickets}
                renderItem={({item}) => {return  (
                    <TouchableOpacity
                        activeOpacity={0.8}
                        onPress={() => {navigation.navigate("Enter", {concertId : item.concertId})}}
                    >
                        {TicketCard (item)}
                    </TouchableOpacity>
                )}}
                initialNumToRender={3}
                style = {styles.scroll}
                showsVerticalScrollIndicator = {false}
                ItemSeparatorComponent={() => {return <View style={{height: 35}} />}}
                ListFooterComponent={() => {   
                    return <View style={{height: 20}} />}}
                ListEmptyComponent={<Text style={styles.listEmpty}>尚無票券</Text>}
            />
        </View>
    );
}

const styles = StyleSheet.create({
    body: {
        backgroundColor: color.backgroundColor,
        alignItems: 'center',
    },
    title: {
        color: color.darkBlue,
        fontSize: 24,
        fontWeight: '700',
        marginBottom: 55,
        marginTop: 35,
        textAlign: 'center',
        // marginLeft: 30,
    },
    scroll: {
        // width: "100%",
        // marginLeft: -30
    },
    listEmpty: {
        fontSize: 24,
        fontWeight: "600",
        color: color.blue,
        textAlign: "center",
        marginTop: 100
    }
})