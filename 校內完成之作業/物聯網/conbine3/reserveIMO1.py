import cv2
from pyzbar.pyzbar import decode
import pymysql
from datetime import datetime
import datetime
import time
   
def checkDB(id):
    def checkTime():
        time_now = datetime.datetime.now()
        localtime = time_now.strftime("20%y-%m-%d %H:%M:%S")
        db = pymysql.connect(host='localhost', port=3306, user='root', passwd='', db='playground', charset='utf8')
        cursor = db.cursor()
        sqlSearch = "SELECT estimate From playground Where id = '" + id +"'"
        try:
          #搜尋資料庫
          cursor.execute(sqlSearch)
          estimate_time=list(cursor.fetchone())
          db.close()
          
          if estimate_time == None:
              return False
          elif str(localtime) <= str(estimate_time[0]) :
              return True
        except:
          #發生錯誤時停止執行SQL
          db.rollback()
          print('Error Try Again')
          db.close()
        return False
    localtime = time.localtime()
    localtime = time.strftime("%Y-%m-%d %H:%M:%S", localtime)
    db = pymysql.connect(host='localhost', port=3306, user='root', passwd='', db='playground', charset='utf8')
    cursor = db.cursor()
    if checkTime():
        sqlSearch = "SELECT rides1,rides2,rides3 From playground Where id = '" + id +"'"
        try:
          #搜尋資料庫
          cursor.execute(sqlSearch)
          t = cursor.fetchone()
          if t == None:
              print('You haven\'t reserved me, IMO')
              return False
          else:
              index=0
              if t[0] == "IMO1":
                  index = 0
              elif t[1] == "IMO1":
                  index = 1
              elif t[2] == "IMO1":
                  index = 2
              else:
                  print('You haven\'t reserved me, IMO')
                  return False
              
              sqlWrite = "UPDATE playground SET rides" + str(index+1) + "='' WHERE id='" + id +"'"
              #更新playground
              #print(sqlWrite)
              cursor.execute(sqlWrite)
              db.commit()
              t = cursor.fetchone()
              #更新count
              sqlSearch = "SELECT count From playground Where id = '" + id +"'"
              cursor.execute(sqlSearch)
              t = cursor.fetchone()
              count = str(int(t[0])-1)
              sqlWrite = "UPDATE playground SET count=" + count + " WHERE id='" + id +"'"
              cursor.execute(sqlWrite)
              db.commit()
              
              #寫rides
              cursor2 = db.cursor()
              time_now = datetime.datetime.now()
              localtime = time_now.strftime("%y/%m/%d %H:%M:%S")
              sqlWrite = "INSERT INTO rides(name, reserve_time, id) VALUES ('IMO1', '" + localtime + "', '" + id + "')"
              #print(sqlWrite)
              cursor2.execute(sqlWrite)
              db.commit()
              db.close()
              
              print('Welcome, have a good time~~')
              return True
        except:
          #發生錯誤時停止執行SQL
          db.rollback()
          print('Error, Try Again')
          db.close()
          return False
    else:
        print('Unavaliable IMO')  
        return False
    return False
      
# 捕获摄像头输入
cap = cv2.VideoCapture(0)
repeat_qrcodes = ""

while True:
    # 读取摄像头帧
    ret, frame = cap.read()

    # 将帧转换为灰度图像
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    # 检测 QR 码
    qr_codes = decode(gray)
    
    #print(qr_codes)
    
    
        # 处理解码结果
    for qr_code in qr_codes:
        if repeat_qrcodes != qr_code.data.decode('utf-8'):
            repeat_qrcodes = qr_code.data.decode('utf-8')
            checkDB(str(int(repeat_qrcodes)))
        
    # 显示摄像头输入
    #cv2.imshow('frame', frame)

    # 按 'q' 键退出循环
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
    

# 释放摄像头
cap.release()

# 关闭窗口
cv2.destroyAllWindows()
