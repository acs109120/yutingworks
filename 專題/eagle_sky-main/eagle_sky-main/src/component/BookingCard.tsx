import React from 'react';
import { StackNavigationProp } from '@react-navigation/stack';
import { navigationType } from './navigationType'
import { View, StyleSheet, Text, Image, TouchableOpacity } from 'react-native';
import { color } from '../style'
import { buyTicket, buyResellTicket } from '../page/booking/loadAndUpload';
import { useRoute } from '@react-navigation/native';

export let area : Area = {seat:"A區", price:2800, remaining:6}

class Area {
    seat: string="";
    price: number=0;
    remaining: number=0;
}

export function BookingCard({seat, price, remaining, navigation, sellWhat, concertId, areaId}: Area ): JSX.Element{
    const BookingButton = ({onPress = () => { }}) =>{
          const handlePress = () => {
                if (sellWhat) {
                  // sellWhat=true
                  buyTicket(areaId);
                } else {
                  // sellWhat=false
                  buyResellTicket(areaId);
                }
                // 傳遞到Cash
                navigation.navigate("Cash", {
                  concertId: concertId,
                  areaId: areaId,
                  sellWhat: sellWhat,
                  areaArea: seat,
                  areaPrice: price,
                });


            onPress();
          };

        return(
            <TouchableOpacity activeOpacity={0.9} onPress={handlePress}>
                <View style={styles.button}>
                    <Text style={styles.button_text}>訂票</Text>
                </View>
            </TouchableOpacity>

        )
    }

    return (
        <View style={styles.container}>
            <View style={styles.left}>
                <View style={styles.Block}>
                    <View style={styles.wordContainer}>
                        <Text style={styles.title}> {seat} </Text>
                        <View style={styles.bottom}>
                            <Text style={styles.word}> NT.{price} </Text>
                            <Text style={styles.word}> [剩餘{remaining}位] </Text>
                        </View>
                             <BookingButton />
                    </View>

                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        marginBottom: 1,
        textAlign: 'left',
    },
    wordContainer: {
        justifyContent: 'flex-end',
        flexDirection: 'row',
        flex: 1,
        alignItems: 'center',
        bottom:8,
    },
    buttonContainer: {
        alignItems: 'flex-end',
    },
    left: {
        justifyContent: 'center',
    },
    title: {
        color: color.brown,
        fontSize: 28,
        fontWeight: '900',
        marginTop: 5,
        textAlign: 'center',
        //textAlignVertical:'bottom',
        top:5,
        left:14,
    },
    word: {
        marginTop:5,
        fontSize: 13,
        fontWeight: '400',
        color: color.brown,
        flexDirection: 'row',
        textAlign: 'center',
        textAlignVertical:'bottom',
        left:11,
        top:4,
    },
    Block: {
        marginLeft: 20,
        //bottom: 10,
        backgroundColor: color.yellow,
        width: '90%', // 使用百分比設定寬度
        aspectRatio: 4 / 1, // 設定寬高比例為 4:1
        alignSelf: 'center', // 水平置中
        borderRadius: 20,
    },
    bottom: {
        //justifyContent: 'space-between',
        //alignItems: 'flex-end', // 使字靠近block底部
        top:10,
        flexDirection: 'row',
        //alignContent:'baseline',
    },
    button: {
        backgroundColor: color.darkyellow,
        width:'55%',
        //length:'10%',
        borderRadius:8,
        justifyContent: "center",
        alignItems: "center",
        alignSelf: 'center',
        paddingTop:3,
        paddingBottom:5,
        right:'5%',
        top:'10%',
        flexDirection: 'row',
        bottom:10,
    },
    button_text: {
        fontSize: 18,
        fontWeight: '700',
        color: "white",
        flexDirection: 'row',
        textAlign: 'center',
    },
})

export default BookingCard;