import React from 'react';
import { View, StyleSheet, Text, Image, TouchableOpacity } from 'react-native';
import { StackNavigationProp } from '@react-navigation/stack';
import { color } from '../style'
import eagleSky from '../assets/eagleSky2.png'

export let concert : Concert = {title:"演唱會名稱", date:"2023/12/25", time_start:"18:00", time_end:"22:00", img:eagleSky}

class Concert {
    title: string = "";
    date: string = "";
    time_start: string = "00:00";
    time_end: string = "00:00";
    img: string = eagleSky;
}

export default function ConcertCard(concert: Concert){
    if(concert.time_start)
        concert.time_start=concert.time_start.substring(0,5)
    if(concert.time_end)
        concert.time_end=concert.time_end.substring(0,5)
    return(
        <View style={styles.body}>
            {/* <Text>{img}</Text>
            <Text>{eagleSky}</Text> */}
            <Image source={concert.img ?
                {uri: concert.img} :
                eagleSky}
                style={styles.imgCss}
                resizeMode={concert.img ? "cover" : "contain"} 
            />
            <View style={styles.line} />
            <Text style={styles.title}>
                {concert.title}
            </Text>
            <Text style={styles.date}>
                {concert.date}　{concert.time_start}～{concert.time_end}
            </Text>
        </View>
    )
}

const styles = StyleSheet.create({
    body: {
        minWidth: "100%",
        justifyContent: "center"
    },
    imgCss: {
        maxWidth: "100%",
        minWidth: "100%",
        height: 120,
        backgroundColor: '#D9D9D9'
    },
    line: {
        // width: '100%',
        height: 5,
        marginTop: 8,
        backgroundColor: color.lightBlue
    },
    title: {
        color: color.darkBlue,
        fontSize: 20,
        fontWeight: '700'
    },
    date: {
        color: color.darkBlue,
        fontSize: 14,
        fontWeight: '600'
    }
})
