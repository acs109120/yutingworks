import React, { useEffect,useState, useRef  } from 'react';
import { Alert,Button, View, StyleSheet, Text, TextInput, Image, TouchableOpacity, FlatList } from 'react-native';
import { navigationType } from '../navigationType';
import { useRoute } from '@react-navigation/native';
import { color } from '../../style';
import {verifyphone} from '../login/SendPhone'
import PhoneInput from "react-native-phone-number-input";
import { sendSmsVerification } from '../../../api/verify'

const userPhoneNumber = '+886972145362'; // 更換為有效的接收簡訊的手機號碼 //databaseM/4

export default function Login({ navigation ,route}: navigationType){
    const [phoneNumber, setPhoneNumber] = React.useState('');
    const [password, setPassword] = React.useState('')
    const phoneInput = useRef<PhoneInput>(null);
    const [value, setValue] = useState("");
    const [formattedValue, setFormattedValue] = useState("");
    const VerifyButton = ({ title, onPress = () => { } }) => {
        return (
            <TouchableOpacity
                activeOpacity={0.7}
                style={styles.loginButton}
                onPress={() => {navigation.navigate("PhoneNumber")}}
            >     
               <Text style={styles.loginButtonText}>登入</Text>
                
            </TouchableOpacity>
        )
    }
    const handleSomeEvent = () => {
        if (phoneInput.current) {
          // 使用 phoneInput.current.getValue() 來獲取輸入的值
          //const phoneNumber = phoneInput.current.getValue();
          console.log('formattedValue:', formattedValue);
          //console.log('value:', value);
        }
      };

    let verificationCode=152451;
    const checkaccount=()=>{
        if(userPhoneNumber == phoneNumber){
            //verifyphone();
            navigation.navigate("Home",{verificationCode : verificationCode})
        }else{
            Alert.alert("密碼或電話錯誤")
        }
    }

   

    return (
        <View>
            <Text style={styles.loginText}>登入</Text>
            <View style={styles.inputBlock}>
                {/* <View style={styles.inputLine}>
                    <Text style={styles.inputText}>電話：</Text>
                    <TextInput
                        style={styles.input}
                        placeholder=" "
                        value={phoneNumber}
                        onChangeText={text => setPhoneNumber(text)}
                    />
                </View> */}

                <PhoneInput
                    ref={phoneInput}
                    defaultValue={value}
                    defaultCode="US"
                    layout="first"
                    onChangeText={(text) => {
                        setValue(text);
                    }}
                    onChangeFormattedText={(text) => {
                        setFormattedValue(text);
                    }}
                    countryPickerProps={{ withAlphaFilter: true }}
                    withShadow
                    autoFocus
                 />

                <View style={styles.inputLine}>
                    <Text style={styles.inputText}>密碼：</Text>
                    <TextInput
                        style={styles.input}
                        placeholder=" "
                        value={password}
                        onChangeText={text => setPassword(text)}
                    />
                </View>
                
       
            </View>
            <TouchableOpacity
                style={styles.loginButton}
                activeOpacity={0.7}
                onPress={() => {
                    // sendSmsVerification(formattedValue).then((sent) => {
                    //     console.log("Sent!");
                    //   });
                    handleSomeEvent();
                    navigation.navigate("Verify",{phoneInput:formattedValue})
                }}   //登入的按鈕在這
            >
                <Text style={styles.loginButtonText}>登入</Text>
            </TouchableOpacity>
            <TouchableOpacity
                // style={styles.registerButton}
                activeOpacity={0.7}
                onPress={() => {navigation.navigate("Register")}}
            >
                <Text style={styles.registerButtonText}>第一次使用－－註冊</Text>
            </TouchableOpacity>

            <VerifyButton title="登入"
                onPress={checkaccount}
             />
        </View>
    )
}

const styles = StyleSheet.create({
    loginText: {
        fontSize: 32,
        color: color.darkBlue,
        marginTop: 80,
        textAlign: 'center',
        fontWeight: '700'
    },
    inputBlock: {
        alignItems: 'center',
        margin: 50,
        fontSize: 24,
        color: color.darkBlue
    },
    inputLine: {   
        flexDirection: 'row',
        alignItems: 'center',
        margin: 10
    },
    inputText: {
        fontSize: 16,
        color: color.darkBlue,
    },
    input: {
        borderBottomColor: color.darkBlue,
        borderBottomWidth: 3,
        width: 250,
        padding: 3
    },
    loginButton: {
        backgroundColor: color.blue,
        padding: 8,
        borderRadius: 5,
        width: 70,
        alignSelf: 'flex-end',
        marginTop: 50,
        marginRight: 50
    },
    loginButtonText: {
        color: color.white,
        textAlign: 'center',
        fontSize: 16
    },
    registerButton: {
        backgroundColor: 'white',
        padding: 8,
        borderRadius: 10,
        width: 150,
        alignSelf: 'flex-end',
        margin: 20,
        marginRight: 50
    },
    registerButtonText: {
        color: color.textyellow,
        fontSize: 14,
        borderBottomColor: color.textyellow,
        borderBottomWidth: 2,
        margin: 20,
        marginRight: 40,
        alignSelf: 'flex-end',
        width: 130
    }
})