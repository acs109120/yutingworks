import time
import threading
import pymysql
import datetime
# try:
#     import Tkinter as tk
# except ImportError:
import tkinter as tk

while(1):
    db = pymysql.connect(host='localhost', port=3306, user='root', passwd='', db='playground', charset='utf8')
    cursor = db.cursor()
    GMT = datetime.timezone(datetime.timedelta(hours=8))
    time_now = datetime.datetime.now()

    win = tk.Tk()
    win.wm_title("預約時間")
    win.minsize(height=700, width=1200)

    #設定時間#################################################
    a = tk.StringVar()
    def showTime():
        now = datetime.datetime.now(tz=GMT).strftime('%H:%M:%S')  # 取得目前的時間，格式使用 H:M:S
        a.set(now)  # 設定變數內容
        win.after(1000,showTime)  # 視窗每隔 1000 毫秒再次執行一次 showTime()


    label9=tk.Label(win, text='目前時間', font=('微軟正黑體', 30))
    label9.place(x=500,y=500)  # 放入第一個 Label 標籤
    label10=tk.Label(win, textvariable=a, font=('微軟正黑體', 20))  # 放入第二個 Label 標籤，內容是 a 變數
    label10.place(x=510,y=550)
    showTime()  # 執行函式
    ############################################################
    searchid = "SELECT id  from rides WHERE name = %s"
    localmin = time_now.strftime("%M")
    localsec = time_now.strftime("%S")


    name = "IMO1" #設備名稱 name
    cursor.execute(searchid, (name))
    idTB = cursor.fetchall()
    num=len(idTB)

    #顯示有幾筆資料print(num)########################################

    label1 = tk.Label(win, text="第一梯次",fg="red",bg="yellow",font=("微軟正黑體",15))
    label1.place(x=100,y=30)

    label7 = tk.Label(win, text="第二梯次", fg="red", bg="yellow", font=("微軟正黑體", 15))
    label7.place(x=390, y=30)

    label8 = tk.Label(win, text="第三梯次", fg="red", bg="yellow", font=("微軟正黑體", 15))
    label8.place(x=670, y=30)

    label1 = tk.Label(win, text="第四梯次", fg="red", bg="yellow", font=("微軟正黑體", 15))
    label1.place(x=940, y=30)
    if num < 6:
        for x in range(0,num):
            label2 = tk.Label(win, text= idTB[x], fg="blue", bg="yellow", font=("Helvetica", 15))
            label2.place(x=130, y=100+x*65)
    elif num < 12:
        for x in range(0,6):
            label2 = tk.Label(win, text= idTB[x], fg="blue", bg="yellow", font=("Helvetica", 15))
            label2.place(x=130, y=100+x*65)
        for z in range(6,num):
            label3 = tk.Label(win, text= idTB[z], fg="blue", bg="yellow", font=("Helvetica", 15))
            label3.place(x=430, y=100+(z-6)*65)
    elif num < 18 :
        for x in range(0,6):
            label2 = tk.Label(win, text= idTB[x], fg="blue", bg="yellow", font=("Helvetica", 15))
            label2.place(x=130, y=100+x*65)
        for z in range(6,12):
            label3 = tk.Label(win, text= idTB[z], fg="blue", bg="yellow", font=("Helvetica", 15))
            label3.place(x=430, y=100+(z-6)*65)
        for k in range(12,num):
            label4 = tk.Label(win, text= idTB[k], fg="blue", bg="yellow", font=("Helvetica", 15))
            label4.place(x=710, y=100+(k-12)*65)

    elif num < 24:
        for x in range(0,6):
            label2 = tk.Label(win, text= idTB[x], fg="blue", bg="yellow", font=("Helvetica", 15))
            label2.place(x=130, y=100+x*65)
        for z in range(6,12):
            label3 = tk.Label(win, text= idTB[z], fg="blue", bg="yellow", font=("Helvetica", 15))
            label3.place(x=430, y=90+(z-6)*65)
        for k in range(12,18):
            label4 = tk.Label(win, text= idTB[k], fg="blue", bg="yellow", font=("Helvetica", 15))
            label4.place(x=710, y=100+(k-12)*65)
        for h in range(18, num):
            label5 = tk.Label(win, text=idTB[h], fg="blue", bg="yellow", font=("Helvetica", 15))
            label5.place(x=980, y=100 + (h - 18) * 65)
    else:
        for x in range(0, 6):
            label2 = tk.Label(win, text=idTB[x], fg="blue", bg="yellow", font=("Helvetica", 15))
            label2.place(x=130, y=100 + x * 65)
        for z in range(6, 12):
            label3 = tk.Label(win, text=idTB[z], fg="blue", bg="yellow", font=("Helvetica", 15))
            label3.place(x=430, y=100 + (z - 6) * 65)
        for k in range(12, 18):
            label4 = tk.Label(win, text=idTB[k], fg="blue", bg="yellow", font=("Helvetica", 15))
            label4.place(x=710, y=100 + (k - 12) * 65)
        for h in range(18,24):
            label5 = tk.Label(win, text= idTB[h], fg="blue", bg="yellow", font=("Helvetica", 15))
            label5.place(x=980, y=100+(h-18)*65)

    label6 = tk.Label(win, text="遊戲運行時間一梯次為15分鐘",fg="#ffffff",bg="#000000",font=("微軟正黑體",20))
    label6.place(x=380,y=600)
    label11 = tk.Label(win, text="請於時間內到達", fg="#ffffff", bg="#000000", font=("微軟正黑體", 15))
    label11.place(x=510, y=650)

    #########################################################
    selectdeleteid = " SELECT id FROM rides WHERE name = %s "
    cursor1 = db.cursor()
    cursor2 = db.cursor()
    cursor1.execute(selectdeleteid,name)
    cursor2.execute(selectdeleteid,name)
    total=cursor1.fetchall()
    num=len(total)
    #print(num)
    if len(total) < 6 :
        delete_id = cursor2.fetchmany(len(total))

    else:
        delete_id = cursor2.fetchmany(6)
        num=6
    #print(num)
    #print(len(total))
    #刪減的設備name要改 @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @
    deleteid = " DELETE FROM rides WHERE id = %s and name = 'hanbin' "
    print(delete_id)
    #if localsec == "30":
    if localmin == "00" or localmin == "10" or localmin == "20" or localmin == "30" or localmin == "40"or localmin == "50":
        print("delete_id:")
        print(delete_id)
        print("we delete")
        for y in range(0,num):
            #print(delete_id[y])
            #cursor2.execute(deleteid)
            cursor2.execute(deleteid, delete_id[y])
        db.commit()
    #######################################################
    #時間到 刪除6筆資料

    win.mainloop()
    time.sleep(5)

