import React, { useEffect } from 'react';
import { Button, View, StyleSheet, Text, TextInput, Image, TouchableOpacity, FlatList } from 'react-native';
import { StackNavigationProp } from '@react-navigation/stack';
import { navigationType } from '../navigationType'
import { color } from '../../style'
import searchImg from '../../assets/searchImg.png';
import filterImg from '../../assets/filterImg.png';
import eagleSky from '../../assets/eagleSky.png'
import ConcertCard from '../../component/ConcertCard'
import { http } from '../../../http'
import { loadConcerts, concerts } from './load'

class Concert {
    id: number = 0;
    title: string = "";
    date: string = "";
    time_start: string = "";
    time_end: string = "";
    img: string = "";
}

class FilterDates {
    chooseStartDate: string = "";
    chooseEndDate: string = "";
    discardStartDate: string = "";
    discardEndDate: string = "";
}

interface ConcertProps{
    posts: Concert[];
 }

export default function Home({ navigation, route }: navigationType) {
    const [searchText, changeSearchText] = React.useState('');
    let [showConcerts, updateConcerts] = React.useState([]  as Concert[]);
    let filterDates: FilterDates = {
        chooseStartDate: "",
        chooseEndDate: "",
        discardStartDate: "",
        discardEndDate:  ""
    }
    if(route.params)
        filterDates =  {
            chooseStartDate: route.params.chooseStartDate,
            chooseEndDate: route.params.chooseEndDate,
            discardStartDate: route.params.discardStartDate,
            discardEndDate:  route.params.discardEndDate
        }
    useEffect(() => {
        Promise.all([loadConcerts(filterDates, searchText)]).then(() =>
            updateConcerts(concerts)
        )
    }, [route])
    async function search(searchText: string){
        Promise.all([loadConcerts(filterDates, searchText)]).then(() =>
            updateConcerts(concerts)
        )
    }

    return (
        <View style={styles.body}>
            <View style={styles.flex}>
                <TextInput
                    style={styles.search}
                    onChangeText={text => changeSearchText(text)}
                    value={searchText}
                    placeholder="搜尋..."
                />
                <TouchableOpacity onPress={() => search(searchText)} 
                    style={styles.searchImgCss}
                    activeOpacity={0.7}>
                    <Image source={searchImg} style={styles.searchImgCss} />
                </TouchableOpacity>

                <TouchableOpacity onPress={() => navigation.navigate("FilterPage")} 
                    style={styles.filterImgCss}
                    activeOpacity={0.7}>
                    <Image source={filterImg} style={styles.filterImgCss} />
                </TouchableOpacity>
            </View>
            <FlatList
                data={showConcerts}
                // renderItem={(item) => {return <ConcertCard title={item[0].title} date={item[0].date img}={item[0].img}/>}}
                renderItem={({item}) => {return (
                    <TouchableOpacity
                        activeOpacity={0.8}
                        onPress={() => {navigation.navigate("Concert", {concertId : item.id})}}
                    >
                        {ConcertCard (item) }
                    </TouchableOpacity>
                )}}

                initialNumToRender={3}
                // onRefresh={}
                style = {styles.scroll}
                showsVerticalScrollIndicator = {false}
                ItemSeparatorComponent={() => {return <View style={{height: 50}} />}}
                ListFooterComponent={() => {   
                    return <View style={{height: 20}} />}}
                ListEmptyComponent={<Text style={styles.listEmpty}>暫無活動</Text>}
            />
      </View>
    );
}

const styles = StyleSheet.create({
    body: {
        // backgroundColor: color.backgroundColor,
        paddingTop: 10,
        paddingHorizontal: 25,
        height: "100%",
        alignItems: "center",
    },
    flex: {
        alignItems: 'flex-end',
        width: "100%",
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    search: {
        borderBottomColor: color.darkBlue,
        borderBottomWidth: 3.5,
        width: '60%',
        paddingBottom: 0,
        marginRight: 10
    },
    searchImgCss: {
        width: 21,
        height: 25,
    },
    filterImgCss: {
        width: 29,
        height: 21,
        marginLeft: 'auto'
    },
    scroll: {
        width: "100%",
        marginVertical: 30
    },
    listEmpty: {
        fontSize: 24,
        fontWeight: "600",
        color: color.blue,
        textAlign: "center",
        marginTop: 100
    }
})