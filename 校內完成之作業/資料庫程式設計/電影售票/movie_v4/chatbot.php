<!-- Created By CodingNepal -->
<?php
// connecting to database
$conn = mysqli_connect("localhost", "root", "", "filmdatabase") or die("Database Error");
// getting user message through ajax
$getMesg = mysqli_real_escape_string($conn, $_POST['text']);
$arr=explode(" ",$getMesg);

$work_type_flag = 0; // 1:SearchPopularMovie, 2:ShowMyTicket;
$top = 3; //預設列出排行前三

// 判斷前端輸入格式
for($i=0;$i<count($arr);$i++){
	if(strcasecmp($arr[$i],"top")==0){
		if(array_key_exists(1,$arr) && !ctype_digit($arr[1])){
			break;
		}
		else if(array_key_exists(1,$arr)){
			$top = $arr[1];
		}
        $work_type_flag=1;
        break;
    }
	else if(strcasecmp($arr[$i],"myticket")==0){
		$work_type_flag=2;
		break;
	}
	else if(strcasecmp($arr[$i],"back")==0){
		$work_type_flag=99;
	}
}

switch($work_type_flag){
	case 1:
        SearchPopularMovie($top);
        break;
	case 2:
	    ShowMyTicket();
		break;
	case 99:
		echo "back";
		exit;
		break;
    default:
        reply();
        break;
}

function reply(){
    echo "Sorry can't be able to understand you!<br>";
	echo "Please Enter [top [num] movie|myticket]";
}

function SearchPopularMovie($top){
	global $conn;
	$sql_ticket = "SELECT * FROM ticket";
    $rowcount;
    if ($result=mysqli_query($conn,$sql_ticket)) {
        $rowcount=mysqli_num_rows($result);
    }

    if($rowcount>0){
	    $Countarr = array();
	    $result = mysqli_query($conn, $sql_ticket);
        while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
	    	$flag = false;
	    	for($x=0;$x<count($Countarr);$x++){
		    	if($Countarr[$x][0]==$row["id"]){
		    	    $Countarr[$x][1]++;
		    		$flag = true;
		    		break;
		    	}
		    }
		    if(!$flag){
		    	array_push($Countarr,array($row["id"],1));
		   }
        }
	
	    usort($Countarr,function($a,$b){
		return $a[1] - $b[1];
	    });
	
	    for($i=1;$i<=$top;$i++){
			echo "No.".($i)."<br>" ;
			$movie_id = $Countarr[count($Countarr)-$i][0];
            echo "movie id: ".$movie_id."<br>";
			echo "片名:<br>";
			$data = mysqli_query($conn,"SELECT * FROM movie WHERE id = '$movie_id'");
			$movie_title = mysqli_fetch_array($data, MYSQLI_ASSOC);
			echo $movie_title["title"]."<br>";
			echo "售票量:".$Countarr[count($Countarr)-$i][1]."<br><br>";
			if($i==count($Countarr)){
				break;
			}
		}
	
    }
	
    else{
	    echo "No Information";
    }
}

function ShowMyTicket(){
	session_start();
	if($_SESSION['name']!=null){
        //$name=$_SESSION['name'];
	    //echo $name."<br>" ;
	
        global $idd;
        global $num;
        global $name;
        $name=$_SESSION['name'];
	    echo $name."<br>" ;
        global $j;
        global $data_seat;
        global $row;
        global $row_t;
        global $idt;
        $conn = mysqli_connect("localhost", "root", "", "filmdatabase") or die("Database Error");
        $conn_t = mysqli_connect("localhost", "root", "", "filmdatabase") or die("Database Error");
        $sql_t="SELECT * FROM `ticket` WHERE `name`='$name'";
        $result_t= mysqli_query($conn_t,$sql_t);
                        
        if(mysqli_num_rows($result_t) > 0){
				
	        foreach($result_t as $row_t){
                $idt=$row_t['id'];
                $sql="SELECT * FROM `movie` WHERE `id` LIKE '$idt'";
                $result= mysqli_query($conn,$sql); 
                if(mysqli_num_rows($result_t) > 0){
			    	foreach($result as $data){
                        echo "片名: ".$data['title']."<br>";
                        echo "種類: ".$data['kind']."<br>";
                        echo "片長: ".$data['duration']."<br>";
                        echo "時間: ".$data['time']."<br>";
                        echo "座位: ".$row_t['seat_row'],$row_t['seat_col']."<br>";
				    }
			    }
		    }
	    }
	}
	else {
		echo "Please login first.";
	}
}

?>