<!DOCTYPE html>

<head>
    <meta charset="UTF-5">
    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" href="booking.css">
</head>

<body>
    <header>
        <h2>影城系統</h2>
            <input type="button" onclick="location.href='menuLogin.php'" value="回首頁">
            <input type="button" onclick="location.href='single.php'" value="訂票資料">
            <input type="button" value="客服機器人" onclick="location.href='chatbot.html'">
    </header>
    <br>
    <br>
    <main>
        <?php
            $id=$_GET['id'];
            $conn = new mysqli("localhost", "root", "", "filmdatabase") or die("Database Error");
            $check_data = "SELECT id, spead, title, time, duration, kind FROM movie WHERE id=".$id;
            $run_query=mysqli_query($conn,$check_data) or die("Error");
            $fetch_data = mysqli_fetch_assoc($run_query);
        ?>
        <img src=<?php echo $id?>.png>
        <div class="information">
            <p class="title"><?php echo $fetch_data['title']?></p>
            <p>放映時間：<?php echo $fetch_data['time']?></p>
            <p>片長：<?php echo $fetch_data['duration']?></p>
            <p>種類：<?php echo $fetch_data['kind']?></p>
            <p>票價：<?php echo $fetch_data['spead']?></p>
            <lable>座位：</lable>
            <br>
            <form method=post class="booking" action=<?php echo "sellTicket.php?id=".$id ?>>
                <select name="row">
                    <option value="A">A</option>
                    <option value="B">B</option>
                    <option value="C">C</option>
                    <option value="D">D</option>
                    <option value="E">E</option>
                    <option value="F">F</option>
                    <option value="G">G</option>
                    <option value="H">H</option>
                    <option value="I">I</option>
                    <option value="J">J</option>
                </select>
                <select name="seat">
                    <option value="1"> 1</option>
                    <option value="2"> 2</option>
                    <option value="3"> 3</option>
                    <option value="4"> 4</option>
                    <option value="5"> 5</option>
                    <option value="6"> 6</option>
                    <option value="7"> 7</option>
                    <option value="8"> 8</option>
                    <option value="9"> 9</option>
                    <option value="10">10</option>
                    <option value="11">11</option>
                    <option value="12">12</option>
                    <option value="13">13</option>
                    <option value="14">14</option>
                    <option value="15">15</option>
                    <option value="16">16</option>
                    <option value="17">17</option>
                    <option value="18">18</option>
                </select>
                <input type="submit" value="確認訂票">
            </form>
            <br>
            <br>
            <br>
            <br>
            <?php
                include_once('seat.php');
            ?>
            <!-- <img src="seat.png"> -->
        </div>
        
    </main>
</body>