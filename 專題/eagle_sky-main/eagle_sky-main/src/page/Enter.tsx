  /**
   * Sample React Native App
   * https://github.com/facebook/react-native
   *
   * @format
   */
  import { color } from '../style'
  import { navigationType } from './navigationType'
  import React, { useState, useEffect } from 'react';
  import type {PropsWithChildren} from 'react';
  import QRCode from 'react-native-qrcode-svg';
  import eagleSky from '../assets/eagleSky.png';
  import {loadConcert, concert, getSeat, seat,area} from './enter/load'
  import {
    Image,
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    AppRegistry,
    Text,
    useColorScheme,
    View,
    Button,
    TouchableOpacity,
  } from 'react-native';
  import {
    Colors,
  } from 'react-native/Libraries/NewAppScreen';
  import { useRoute } from '@react-navigation/native';
  import { userId } from '../user';

    let activitytitle="Loading..."; //活動標題 
    let detailnotice2 = "Loading..."
    //2注意事項
    let ticket_seat="A12";
    let accoutphone=userId;
    let detailnotice3 = "Loading..."
    //3活動介紹
    //let detailnotice4 = "You may also install the standalone version of React Developer Tools to inspect the React component hierarchy, their props, and state. Status: Debugger session active."
    //4場地及票種規畫圖
    let refreshphoto='../assets/refresh.png';
    let photopath = '../assets/QRcode.png';
    let photo1='http://ip205048.ntcu.edu.tw:8030/uploads/thumbnail_img01copy1_5836a23dd0.jpg';
    let photo2='http://ip205048.ntcu.edu.tw:8030/uploads/thumbnail_img01copy1_5836a23dd0.jpg';
  // 在需要的地方调用loadConcert函数来加载concert数据

  type SectionProps = PropsWithChildren<{
    title: string;
  }>;
  var msDiff = new Date().getTime() - new Date("May 20, 20233").getTime();
  var workDayCount = Math.floor(msDiff / (1000 * 60 * 60 * 24));

  function Section({children, title}: SectionProps): JSX.Element {
    const isDarkMode = useColorScheme() === 'dark';

    return (
      <View style={styles.sectionContainer}>
        
        <Text
          style={[
            styles.sectionTitle,
            {
              color: isDarkMode ? Colors.white : Colors.black,
            },
          ]}>
          {title}
        </Text>
        
        <Text
          style={[
            styles.sectionDescription,
            {
              color: isDarkMode ? Colors.light : Colors.dark,
            },
          ]}>
          {children}
        </Text>
      </View>
    );
  }

  const PrimaryButton = ({ title, onPress = () => { } }) => {
    return (
        <TouchableOpacity
            activeOpacity={0.8}
            onPress={onPress}
        >
            <View style={styles.BtnContainer}>
                <Text style={styles.Title}>{title}</Text>
            </View>
        </TouchableOpacity>
    )
  }


  const RefreshButton = ({ onPress }) => {
    return (
      <TouchableOpacity activeOpacity={0.8} onPress={onPress}>
        <Image  
                    //resizeMode={'cover'} 
                    style={styles.qrcenter}
                    source={require(refreshphoto)} 
              /> 
      </TouchableOpacity>
    );
  };

  const currentDateTime = new Date();
    const currentDate =currentDateTime.toISOString().slice(0, 10); // 日期 格式為：YYYY-MM-DD  
    const currentHours = currentDateTime.getHours(); //hour
    const currentMinutes = currentDateTime.getMinutes(); //minute
    const currentSeconds = currentDateTime.getSeconds(); //second
    const formattedTime = `${currentDate} ${currentHours}:${currentMinutes}:${currentMinutes}`; //hour:minute:second
    const specialNumber = accoutphone;  // +電話綁account
    var qrCodeData = `${formattedTime}-${specialNumber}`;  //YYYY-MM-DD h:m:s-accountphone

  //var qrCodeData=generateQRCode
  export default function Home({ navigation }: navigationType): JSX.Element {
    const isDarkMode = useColorScheme() === 'dark';
    const [time, setTime] = React.useState(new Date());
    const [countdown, setCountdown] = useState(10); //設5分鐘
    const [showRefreshButton, setShowRefreshButton] = useState(false);
    const backgroundStyle = {
      backgroundColor: color.backgroundColor,
    };
    const route = useRoute();
      let concertId = route.params && route.params.concertId;

    const generateQRCode = () => {
      const currentDateTime2 = new Date();
      const currentDate2 =currentDateTime2.toISOString().slice(0, 10); // 日期 格式為：YYYY-MM-DD  
      const currentHours2 = currentDateTime2.getHours(); //hour
      const currentMinutes2 = currentDateTime2.getMinutes(); //minute
      const currentSeconds2 = currentDateTime2.getSeconds(); //second
      const formattedTime2 = `${currentDate2} ${currentHours2}:${currentMinutes2}:${currentSeconds2}`; //hour:minute:second
      const specialNumber2 = accoutphone;  // +電話綁account
      qrCodeData = `${formattedTime2}-${specialNumber2}`;  //YYYY-MM-DD h:m:s-accountphone
    
  };
    const handleRefresh = () => {
      setShowRefreshButton(false);
      setCountdown(10); //重啟設五分鐘
      generateQRCode();
    };
    const [dataFetched, setDataFetched] = useState(false);

    useEffect(() => {
      // 在组件初次渲染时获取数据
      loadConcert(concertId)
        .then((loadedConcerts) => {
          activitytitle = loadedConcerts.title;
          detailnotice2 = loadedConcerts.notice;
          detailnotice3 = loadedConcerts.introduction;
          photo1 = loadedConcerts.img;
          photo2= loadedConcerts.imgseat;
          setDataFetched(true);    // 標示已拿過資料了
        })
        .catch((error) => {
          console.error('Enter載入loadconcert時出現問題：', error);
        });
    }, [concertId]);

    Promise.all([  getSeat(concertId)]).then(()=>{
     // console.log("area :   ", area)
     // console.log("seat :   ",seat)
    })
  

  useEffect(() => {
    if (dataFetched && countdown === 0) {
     // console.log("倒數结束， setShowRefreshButton=true");
      setShowRefreshButton(true);
    }else if (countdown > 0) {
      const interval = setInterval(() => {
     //   console.log("倒數 秒數--");
        setCountdown((prevCountdown) => prevCountdown - 1);
      }, 1000);
  
      return () => clearInterval(interval);
    }
  }, [countdown, dataFetched]);
  
    
    const formatTime = (seconds) => {   //倒數計時器用 顯示倒數的秒數
      const minutes = Math.floor(seconds / 60);
      const remainingSeconds = seconds % 60;
      return `${minutes}:${remainingSeconds < 10 ? '0' : ''}${remainingSeconds}`;
    };

    return (
      <SafeAreaView style={backgroundStyle}>
        <StatusBar
          barStyle={isDarkMode ? 'light-content' : 'dark-content'}
          backgroundColor={backgroundStyle.backgroundColor}
        />
        <ScrollView
          contentInsetAdjustmentBehavior="automatic"
          style={backgroundStyle}>
          <View style={styles.title}>
            <Text style={styles.title}>{activitytitle}</Text>
          </View>
            
        {showRefreshButton ? (
          <View style={styles.qrdive}>
            <RefreshButton onPress={handleRefresh} />
            <Text style={styles.qrdetail}>QRcode 已過期</Text>
            <Text style={styles.timerText}>0:00</Text>
          </View>
        ) : (
          <View style={styles.qrdive}>
            <View>
                  <QRCode
                    value={qrCodeData}
                    size={182} // qrcode 大小
                  />
              </View>
              <Text style={styles.qrdetail}>入場QRcode</Text>  
              <Text style={styles.timerText}>{formatTime(countdown)}</Text>
            </View>
        )}

          <Text style={styles.seatTitle}>座位 : {area} {seat}</Text>  

          <View style={styles.detailsContainer}>
            <Section title="注意事項"> 
            <Text>{workDayCount}{detailnotice2}</Text>
            </Section>
          
            <Section title="活動介紹"> 
              <Text style={styles.highlight}></Text> 
            <Text>{detailnotice3}</Text>
            </Section>
            <Section title="場地及票種規畫圖"> 
              <Text style={styles.highlight}></Text> 
              
              <Text>{'\n'}</Text>    
            </Section>
            <Image  
                    //resizeMode={'cover'} 
                    style={{ width: 300, height: 200,alignSelf: 'center', }}
                    source={{ uri: photo2 }}
              /> 

              
          {/* <Text>{time.toLocaleString()}</Text> */}
          </View>
          <Text>{'\n'}</Text>
          {/* <PrimaryButton title="開啟室內定位功能"
                onPress={() => navigation.navigate("Concert")}
          /> */}
            <Text>{'\n'}</Text>
            <Text>{'\n'}</Text>
          
        </ScrollView>
      </SafeAreaView>
    );
  }

  const styles = StyleSheet.create({
    title:{ //活動標題
      marginTop:20,
      alignSelf: 'center',
      textAlign: 'center',
      fontWeight: '700',
      fontSize: 28,
      color:color.darkBlue,
    },
    refreshdetail:{
      width: 182,
      height: 182, 
      alignSelf: 'center',
      textAlign: 'center',
      alignItems:'center',
      justifyContent: 'center',
      backgroundColor:color.gray,
      borderRadius: 10,
    },
    refreshtext:{
      marginTop: 8,
      fontWeight: '600',
      fontSize: 24,
      alignSelf: 'center',
      textAlign: 'center',
      alignItems:'center',
      justifyContent: 'center',
      color:color.backgroundColor,
    },
    seatTitle:{
      fontSize: 20,
      fontWeight: '800',
      color:color.red,
      textAlign:'center',
      marginTop:30,
    },
    timerText: {
      fontSize: 20,
      fontWeight: '500',
      color: '#000000',
      marginTop:11,
    },
    sectionContainer: {
      marginTop: 32,
      paddingHorizontal: 24,
    },
    sectionTitle: {
      fontSize: 20,
      fontWeight: '600',
    },
    sectionDescription: {
      marginTop: 8,
      fontSize: 18,
      fontWeight: '400',
    },
    highlight: {
      fontWeight: '700',
    },
    Title: { 
      color:'white', 
      fontWeight: 'bold', 
      fontSize: 14
   }, //button本身設定
    BtnContainer: {
      backgroundColor: color.darkyellow,
      //height: 40,
      width:'50%',
      borderRadius: 10,
      justifyContent: "center",
      alignItems: "center",
      alignSelf: 'center',
      paddingTop:3,
      paddingBottom:7,
   },
    qrcenter: { //qrcode照片用
      maxWidth: 182,
      height: 182, 
      alignSelf: 'center',
   },
   qrdetail: {  //截圖字體設定
      marginTop: 8,
      fontWeight: '700',
      fontSize: 24,
      alignSelf: 'center',
      textAlign: 'center',
      color:color.darkBlue,
   },
    qrdive: { //qrcode邊框
    width:270,
      alignSelf:'center',
      borderRadius: 20,
      alignItems:'center',
      paddingTop:20,
      paddingBottom:15,
      marginTop:30,
      marginBottom:10,     
      backgroundColor: color.lightBlock,
      justifyContent: 'center',
    },
    detailsContainer: { //下面說明欄的眶框  
      marginHorizontal: 2,
      borderRadius: 15,
      marginBottom: 20,
      padding: 5,         
      marginLeft:10,
      marginRight:10,
      paddingHorizontal: 20,
    },
   center: {
      justifyContent: 'center',
      alignSelf: 'center',
      alignItems: 'center',
      width: 80,
      height: 80,
   },
   textCenter: {
      color: '#fff',
      backgroundColor: 'rgba(50,50,50,0.3)',
      fontSize: 40,
      alignItems: 'center',
  }
  });