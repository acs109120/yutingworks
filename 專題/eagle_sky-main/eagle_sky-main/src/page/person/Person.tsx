/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */
import React, { useEffect } from 'react';
import { StackNavigationProp } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import { navigationType } from '../navigationType'
import personImg2 from '../../assets/personImg2.png'
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { color } from '../../style'
import Person1 from './Person1'
import Person2 from './Person2'
import Person3 from './Person3'
import { userId } from '../../user';
import { getUserName, userName } from './loadAndUpload';
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';

let name="蕭湘竹"
let id=123456789

class Option {
    title: string = "";
}

function MyTabs() {
    const Tab = createMaterialTopTabNavigator();
    return (
        <Tab.Navigator initialRouteName='參與群組' screenOptions={screenOptions}>
            <Tab.Screen name="參與群組" component={Person1} />
            <Tab.Screen name="建立群組" component={Person2} />
            <Tab.Screen name="群組邀請" component={Person3} />
        </Tab.Navigator>
    );
}

function Triangle({ title }:Option) {
    let styles =  StyleSheet.create({
        triangle:{
            width:0,
            height:0,
            borderStyle:'solid',
            borderWidth:5,
            borderLeftWidth:10,
            borderLeftColor: color.blue,
            borderTopColor: 'transparent',
            borderBottomColor: 'transparent',
            borderRightColor: 'transparent'
        },
        option: {
            flexDirection: 'row',
            width: 'auto',
            fontSize: 14,
            alignItems: 'baseline',
            marginLeft: 15,
            marginBottom: 5
        }
    })
    return (
        <View style={styles.option}>
            <View style={styles.triangle} />
            <Text>{title}</Text>
        </View>
    )
}

function lagout(){
}

export default function Home({ navigation }: navigationType) {
    const [username, setUserName] = React.useState('');
    useEffect(() => {
        Promise.all([getUserName(userId)]).then(() =>
            setUserName(userName)
        )
    }, [])
    return (
    <View style={styles.body}>
        <View style={styles.main}>
            <Image source={personImg2} style={styles.personImg2} />
            <View>
                <Text style={styles.name}>{username}</Text>
                <Text style={styles.id}>{"ID：" + String(userId).padStart(8, '0')}</Text>
                <View style={styles.optionsBlock}>
                    <View>
                    <TouchableOpacity
                        activeOpacity={0.8}
                        onPress={() => {navigation.navigate("PersonOptions", {kind : 'changeName'})}}
                    >
                        <Triangle title="更改姓名" />
                    </TouchableOpacity>
                    
                    <TouchableOpacity
                        activeOpacity={0.8}
                        onPress={() => {navigation.navigate("PersonOptions", {kind : 'changePassword'})}}
                    >
                        <Triangle title="更改密碼" />
                    </TouchableOpacity>
                    </View>
                    <View>
                    <TouchableOpacity
                        activeOpacity={0.8}
                        onPress={() => {navigation.navigate("PersonOptions", {kind : 'changeMail'})}}
                    >
                        <Triangle title="更改信箱" />
                    </TouchableOpacity>
                    
                    <TouchableOpacity
                        activeOpacity={0.8}
                        // onPress={() => {navigation.navigate("PersonOptions", {kind : 'changeName'})}}
                    >
                        <Triangle title="登出帳號" />
                    </TouchableOpacity>
                    </View>
                </View>
            </View>
        </View>
        <MyTabs />
    </View>
  );
}

const styles = StyleSheet.create({
    body: {
        flex: 1
    },
    main: {
        marginHorizontal: 20,
        marginTop: 30,
        marginBottom: 50,
        flexDirection: 'row'
    },
    personImg2: {
        width: 110,
        height: 110,
        marginLeft: 10,
        marginRight: 30
    },
    name: {
        color: color.darkBlue,
        fontWeight: "600",
        fontSize: 20
    },
    id: {
        color: color.darkBlue,
        fontWeight: "600",
        fontSize: 14
    },
    optionsBlock: {
        flexDirection: 'row',
        margin: 5,
        marginTop: 20,
        marginLeft: 40
    }
})

const screenOptions = ({
    tabBarGap: 2,
    tabBarAllowFontScaling: true,
    tabBarLabelStyle: {
        fontSize: 14,
        fontWeight: "600"
    },
    tabBarStyle: {
        borderBottomColor: color.lightBlue,
        borderBottomWidth: 5,
        elevation: 0,
    },
    tabBarIndicatorStyle: {
        backgroundColor: color.darkBlue,
        height: 5,
        bottom: -5
    },
    tabBarAndroidRipple: null,
    tabBarPressColor: null
})