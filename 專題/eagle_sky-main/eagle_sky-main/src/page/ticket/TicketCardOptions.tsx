import { View, StyleSheet, Text, Alert, TouchableOpacity } from 'react-native';
import { color } from '../../style'
import { refundTicket, ResellTicket } from './load';

export { refund, sell, sellWaition, noReact }

function refund(areaId: number, ticketId: number){
    const selfStyle = StyleSheet.create({
        bottun: {
            backgroundColor: color.red,
            width: 70,
            height: 30,
            borderRadius: 4,
        }
    }) 
    return(
        <TouchableOpacity
            style={selfStyle.bottun}
            activeOpacity={0.7}
            onPress={() => {Alert.alert("是否確定退票？","",[
                {text: "是",
                    onPress: () => {refundTicket(areaId, ticketId)}
                },
                {text: "否"}
            ])}}
        >
            <Text style={styles.buttonText}>退票</Text>
        </TouchableOpacity>
    )
}

function sell(areaId: number, ticketId: number){
    const selfStyle = StyleSheet.create({
        bottun: {
            backgroundColor: color.red,
            width: 70,
            height: 30,
            borderRadius: 4,
        }
    }) 
    return(
        <TouchableOpacity
            style={selfStyle.bottun}
            activeOpacity={0.7}
            onPress={() => {Alert.alert("是否確定轉售票券？","",[
                {text: "是",
                    onPress: () => {ResellTicket(areaId, ticketId)}
                },
                {text: "否"}
            ])}}
        >
            <Text style={styles.buttonText}>售出票券</Text>
        </TouchableOpacity>
    )
}

function sellWaition(){
    const selfStyle = StyleSheet.create({
        bottun: {
            backgroundColor: color.darkyellow,
            width: 70,
            height: 30,
            borderRadius: 4,
        }
    }) 
    return(
        <TouchableOpacity
            style={selfStyle.bottun}
            activeOpacity={0.7}
        >
            <Text style={styles.buttonText}>待售出</Text>
        </TouchableOpacity>
    )
}

function noReact(){
    let sellOut = false
    const selfStyle = StyleSheet.create({
        bottun: {
            backgroundColor: color.gray,
            width: 70,
            height: 30,
            borderRadius: 4,
        },
        wait: {
            color: "#979DA1",
            fontSize: 12,
            marginBottom: 3
        }
    }) 
    return(
        <View>
            <Text style={selfStyle.wait}>{sellOut?"":"待確認"}</Text>
            <View style={selfStyle.bottun}>
                <Text style={styles.buttonText}>{sellOut?"已售出":"退票"}</Text>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    buttonText: {
        color: 'white',
        fontSize: 14,
        fontWeight: "600",
        textAlign: "center",
        lineHeight: 30
    }
})