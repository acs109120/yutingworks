import React, { useState, useEffect } from "react";
import { SafeAreaView, StyleSheet, Button, Text ,Modal,View,TouchableOpacity} from "react-native";
import { navigationType } from '../navigationType'
import { checkVerification } from "../../../api/verify";
import OTPInputView from "@twotalltotems/react-native-otp-input";
import { useRoute } from '@react-navigation/native';
import { color } from '../../style';
import auth from '@react-native-firebase/auth';
//const Otp = ({ route, navigation }) => {

export default function OTP({ navigation }: navigationType) {
  const route = useRoute();
  let phoneNumber = route.params && route.params.phoneNumber;
  let verificationNumbers=route.params && route.params.verificationNumbers;
 //const { phoneNumber } = route.params;
 const [confirm, setConfirm] = useState(null);
 const [invalidCode, setInvalidCode] = useState(false);
 const [notificationVisible, setNotificationVisible] = useState(false);
 //console.log('verificationNumbers:', verificationNumbers);
 console.log('phoneNumber:', phoneNumber);

 const Notification = ({ visible, message, onClose }) => {
  return (
      <Modal
          transparent={true}
          animationType="slide"
          visible={visible}
          onRequestClose={() => onClose()}
      >
          <View style={styles.notificationContainer}>
              <View style={styles.notification}>
                  <Text>{message}</Text>
                  <TouchableOpacity onPress={() => onClose()}>
                  </TouchableOpacity>
              </View>
          </View>
      </Modal>
  );
}

 const handleSomeEvent = (code) => {
  if (verificationNumbers === code ) {
    // 使用 phoneInput.current.getValue() 來獲取輸入的值
    //const phoneNumber = phoneInput.current.getValue();
    setNotificationVisible(true);
    navigation.replace("Home");
    console.log('code:', code);
  }else{
    setInvalidCode(true);
  }
};

async function signInWithPhoneNumber(phoneNumberss) {
  const confirmation = await auth().signInWithPhoneNumber(phoneNumberss);
  setConfirm(confirmation);
  console.log(phoneNumberss," Sending sms")
}

function onAuthStateChanged(user) {
  
}

  useEffect(() => {
    const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
    return subscriber; // unsubscribe on unmount
  }, [])

async function confirmCode(code) {
    let temp=true;
    try {
      await confirm.confirm(code);
    } catch (error) {
      console.log('Invalid code.');
      temp = false;
    }
    return temp;
}

 

 return (
   <SafeAreaView style={styles.wrapper}>
     { confirm &&  
        <Text style={styles.prompt}>輸入驗證碼</Text>  
     }
     <Text style={styles.message}>
        {`驗證碼傳送至 ( ${phoneNumber} ) `}
     </Text>

     <TouchableOpacity
         style={styles.editButton}
         onPress={() => {
          navigation.replace("PhoneNumberS")
          setConfirm(null);
        }}
      >
       <Text style={styles.editButtonText}>編輯登入號碼</Text>
     </TouchableOpacity>

     

      {(!confirm ) ? (
        
         <TouchableOpacity
            style={styles.loginButton}
            onPress={() => signInWithPhoneNumber('+886 972145362')}
          >
             <Text style={styles.loginButtonText}>傳送驗證碼</Text>
         </TouchableOpacity>
       ) : (
        <OTPInputView
          style={{ width: "80%", height: 200 }}
          pinCount={6}
          autoFocusOnLoad
          codeInputFieldStyle={styles.underlineStyleBase}
          codeInputHighlightStyle={styles.underlineStyleHighLighted}
          onCodeFilled={(code) => {
            // checkVerification(phoneNumber, code).then((success) => {
            //   if (!success) setInvalidCode(true);
            //   success && navigation.replace("Home");
            // });
              confirmCode(code).then((success)=>{
                if (!success){
                  setInvalidCode(true);

                }else{
                  setNotificationVisible(true);
                  navigation.replace("Home");
                }
                

              })
            }}
       />

       )
      }
     
     {invalidCode && <Text style={styles.error}>Incorrect code.</Text>}
     {/* <Button title="Confirm Code" onPress={() => confirmCode()} /> */}
     <Notification
        visible={notificationVisible}
        message="登入成功 進入主畫面"
        onClose={() => setNotificationVisible(false)}
      />
   </SafeAreaView>
 );
};

const styles = StyleSheet.create({
 wrapper: {
  backgroundColor: color.backgroundColor,
   flex: 1,
   justifyContent: "center",
   alignItems: "center",
 },

 borderStyleBase: {
   width: 30,
   height: 45,
 },

 borderStyleHighLighted: {
   borderColor: "#03DAC6",
 },

 underlineStyleBase: {
   width: 30,
   height: 45,
   borderWidth: 0,
   borderBottomWidth: 1,
   color: "black",
   fontSize: 20,
 },

 underlineStyleHighLighted: {
   borderColor: "#03DAC6",
 },

 prompt: {
   fontSize: 24,
   paddingHorizontal: 30,
   paddingBottom: 10,
 },

 message: {
   fontSize: 16,
   paddingHorizontal: 30,
   margin: 10
 },
 error: {
   color: "red",
 },
 notificationContainer: {
  flex: 1,
  justifyContent: 'center',
  alignItems: 'center',
  backgroundColor: color.backgroundColor,
},
notification: {
  backgroundColor: color.purplegray,
  padding: 20,
  borderRadius: 10,
  fontSize: 20,
},
editButton: {
  backgroundColor: color.backgroundColor,
  padding: 8,
  borderRadius: 5,
  //width: 100,
  alignSelf: 'center',
  //marginTop: 50,
  marginBottom:20,
  //marginRight: 50
},
editButtonText: {
  color: color.blue,
  textAlign: 'center',
  fontSize: 16
},
loginButton: {
  backgroundColor: color.blue,
  padding: 8,
  borderRadius: 5,
  //width: 100,
  alignSelf: 'center',
  //marginTop: 50,
  //marginRight: 50
},
loginButtonText: {
  color: color.white,
  textAlign: 'center',
  fontSize: 16
},
});

//export default Otp;
