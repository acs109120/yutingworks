<!DOCTYPE html>

<head>
    <meta charset="UTF-5">
    <link rel="stylesheet" href="bookingFailure.css">
    <meta name="viewport" content="width=device-width">
</head>

<body>
    <header>
        <h2>影城系統</h2>
            <input type="button" onclick="location.href='login.php'" value="登出">
            <input type="button" onclick="location.href='prf.php'" value="訂票資料">
            <input type="button" value="客服機器人" onclick="location.href='chatbot.html'">
    </header>
    <br>
    <br>
    <main>
        <h1>訂票失敗！</h1>
        <br>
        <p>此場次座位已被預訂</p>
        <br>
        <br>
        <br>
        <br>
        <div>
            <input type="button" onclick="location.href='menuLogin.php'" value="回首頁">
            <input type="button" onclick="location.href='booking.php?id=<?php echo $_GET['id']?>'" value="返回訂票">
        </div>
    </main>