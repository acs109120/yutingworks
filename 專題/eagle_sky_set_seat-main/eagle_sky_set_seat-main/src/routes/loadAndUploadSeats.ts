import qs from 'qs'
import axios from 'axios'

const http = axios
const sqlURL="http://ip205048.ntcu.edu.tw"
http.defaults.baseURL=sqlURL

function setGroupQuery(areaId: number = 2){
    let query = qs.stringify({
        filters: {
            area: {
                id:{
                    '$eq': areaId
                }
            }
        }
    })
    return query
}

function setPersonQuery(areaId: number = 1){
    let query = qs.stringify({
        filters: {
            area: {
                id: {
                    '$eq': areaId
                }
            },
            account: {
                groups: {
                    
                        // '$not': {
                            '$or': [
                                {id:{
                                    '$null': true
                                }},
                                {area: {
                                    id: {
                                    '$notContains': areaId,
                                    }
                                }}
                            ]
                        // }
                    
                    // area: {
                    //     id: {
                    //         '$notContains': areaId,
                    //     }
                    // },
                    // id:{
                    //     '$null': true
                    // }
                }
            }
        }
    })
    return query
}

let groups : number[][] = [] //index, user = ticket

async function loadGroups(areaId: number = 2){
    await http.get('/api/groups?populate=accounts.tickets.area&' + setGroupQuery(areaId)).then(res => {
        console.log(res)
        res.data.data.map((item: any, index: number)=>{
            groups[index] = []
            for(let accountIndex=0; accountIndex<item.attributes.accounts.data.length; accountIndex++){
                for(let ticketIndex=0; ticketIndex<item.attributes.accounts.data[accountIndex].attributes.tickets.data.length; ticketIndex++){
                    if(item.attributes.accounts.data[accountIndex].attributes.tickets.data[ticketIndex].attributes.area.data.id == areaId)
                        groups[index][accountIndex]=item.attributes.accounts.data[accountIndex].attributes.tickets.data[ticketIndex].id
                }
            }
        })
        groups = [...groups]
        console.log(groups)
    })
    return groups;
}

let person: number[] = []
async function loadPerson(areaId: number = 1){
    let index: number = 0
    console.log(1)
    await http.get('/api/tickets?populate=area,account.groups.area&' + setPersonQuery(areaId)).then(res=>{
        res.data.data.map((item: any) => {
            if(item.attributes.account.data.attributes.groups.data.length == 0){
                //console.log(item.attributes.account.data.attributes.groups)
                person[index] = item.id
            }
            else{
                let temp = item.attributes.account.data.attributes.groups.data
                let flag = false;
                for(let i = 0;i<temp.length;i++){
                    if(temp[i].attributes.area.id==areaId)
                        flag = true
                }
                if(flag)
                    person[index] = item.id
                else
                    index-- 
            }
            index++
        })
        person = [...person]
        //console.log("-",person)
    })
    return person
}

async function setSeat(ticketId: number = 1, seat: string){
    await http.put('api/tickets/' + ticketId.toString(), {
        data:{
            seat: seat
        }
    })
}

export {loadGroups, groups, loadPerson, person, setSeat}