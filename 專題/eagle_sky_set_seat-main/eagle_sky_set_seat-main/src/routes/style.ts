import {css} from '@stitches/core'

export const inputBlock = css({
    display: 'flex',
    justifyContent: 'center'
}).toString()

export const seatBlock = css({
    margin: '30px auto'
}).toString()

export const seatLine = css({
    display: 'flex',
    justifyContent: 'center'
}).toString()

export const seatSquare = ()=>{
    return css({
        width: '25px',
        height: '25px',
        borderRadius: '5px',
        border: '1px #555 solid',
        backgroundColor: '#123',
    }).toString()
}

export const inputCss = css({
    width: '60px',
    height: '40px',
    backgroundColor: '	#FFED97',
    border: 'none',
    userSelect: 'none'
}).toString()