import { http, sqlURL } from '../../../http'
import eagleSky from '../../assets/eagleSky.png'
import qs from 'qs'
import { userId } from '../../user';

class Concert {
    id: number = 2;
    title: string = "";
    date: string = "";
    date_sell: string = "";
    time_start: string = "";
    time_end: string = "";
    img: string = "";
    imgseat:string="";
    introduction: string= "";
    notice:string= "";
}

let concert : Concert

async function loadConcert(id :number){
    await http.get('/api/concert-alls/'+ id.toString() +'?populate=seat_map&populate=photo').then(res => {
        concert = {
            id: res.data.data.id,
            title: res.data.data.attributes.title,
            date: res.data.data.attributes.date,
            date_sell: res.data.data.attributes.date_sell,
            time_start: res.data.data.attributes.time_start,
            time_end: res.data.data.attributes.time_end,
            introduction: res.data.data.attributes.introduction,
            notice: res.data.data.attributes.notice,
            img: (!res.data.data.attributes.photo.data)?
                eagleSky:
                sqlURL + res.data.data.attributes.photo.data.attributes.formats.thumbnail.url,
            imgseat:(!res.data.data.attributes.seat_map.data)?
                eagleSky:
                sqlURL + res.data.data.attributes.seat_map.data.attributes.formats.thumbnail.url

        }
        concert = concert
    })
    return concert;
}

let ticketConcert: number[] = []
let query = qs.stringify({
    filters: {
        account: {
            id:{
                '$eq': userId
            }
        }
    }
})

async function getTicketConcert(){
    await http.get('/api/tickets?populate=area.concert_all&' + query).then(res => {
        ticketConcert = res.data.data.map((item: any) =>
            item.attributes.area.data.attributes.concert_all.data.id
        )
    })
    return ticketConcert;
}

export {loadConcert, concert, getTicketConcert, ticketConcert}