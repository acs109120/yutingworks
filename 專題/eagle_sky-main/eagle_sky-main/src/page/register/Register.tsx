import React, { useEffect } from 'react';
import { Alert,Button, View, StyleSheet, Text, TextInput, Image, TouchableOpacity, FlatList } from 'react-native';
import { navigationType } from '../navigationType'
import { color } from '../../style'


export default function Register({ navigation, route }: navigationType){
    const [phoneNumber, setPhoneNumber] = React.useState('');
    const [phoneNumberverify, setPhoneNumberverify] = React.useState('');
    const [name, setName] = React.useState('');
    const [email, setEmail] = React.useState('')
    const [emailverify, setEmailverify] = React.useState('')
    const [password, setPassword] = React.useState('')
    const [passwordtwice, setPasswordtwice] = React.useState('')
    const handlecreate = (name:string , password:string , passwordtwice:string) => {
        if(name.trim()==='' || password.trim()==='' || passwordtwice.trim()===''){
            Alert.alert("資料勿空白")
        }else{
            if(password != passwordtwice){
                Alert.alert("密碼不符")
            }else{
                navigation.navigate("RegisterOTP", { name: name, password:password})
            }   
        }
    }
    const handlephone = () => {


    }
    const handleemail = () => {

    }
    return (
        <View>
            <Text style={styles.loginText}>註冊</Text>
            <View style={styles.inputBlock}>
                <View style={styles.inputLine}>
                    <Text style={styles.inputText}>姓名：</Text>
                    <TextInput
                        style={styles.input}
                        placeholder=" "
                        value={name}
                        onChangeText={text => setName(text)}
                    />
                </View>
                {/* <View style={styles.inputLine}>
                    <Text style={styles.inputText}>電話：</Text>
                    <TextInput
                        style={styles.input}
                        placeholder=" "
                        value={phoneNumber}
                        onChangeText={text => setPhoneNumber(text)}
                    />

                </View>

               
                <View style={styles.inputLine}>
                    <Text style={styles.inputText}>信箱：</Text>
                    <TextInput
                        style={styles.input}
                        placeholder=" "
                        value={email}
                        onChangeText={text => setEmail(text)}
                    />
                </View> */}

                
                <View style={styles.inputLine}>
                    <Text style={styles.inputText}>密碼：</Text>
                    <TextInput
                        style={styles.input}
                        placeholder=" "
                        value={password}
                        onChangeText={text => setPassword(text)}
                    />
                </View>
                <View style={styles.inputLine}>
                    <Text style={styles.inputText}>確認：</Text>
                    <TextInput
                        style={styles.input}
                        placeholder=" "
                        value={passwordtwice}
                        onChangeText={text => setPasswordtwice(text)}
                    />
                </View>
            </View>
            <TouchableOpacity
                style={styles.registerButton}
                activeOpacity={0.7}
                onPress={() => {
                    handlecreate(name,password,passwordtwice)
                    
                }}   
            >
                <Text style={styles.registerButtonText}>下一步</Text>
            </TouchableOpacity>
            <TouchableOpacity
                // style={styles.loginButton}
                activeOpacity={0.7}
                onPress={() => {navigation.navigate("PhoneNumberS")}}
            >
                <Text style={styles.loginButtonText}>已持有帳號－－登入</Text>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    loginText: {
        fontSize: 32,
        color: color.darkBlue,
        marginTop: 80,
        textAlign: 'center',
        fontWeight: '700'
    },
    inputBlock: {
        alignItems: 'center',
        margin: 50,
        fontSize: 24,
        color: color.darkBlue
    },
    inputLine: {   
        flexDirection: 'row',
        alignItems: 'center',
        margin: 10
    },
    inputText: {
        fontSize: 16,
        color: color.darkBlue,
    },
    input: {
        borderBottomColor: color.darkBlue,
        borderBottomWidth: 3,
        width: 250,
        padding: 3
    },
    registerButton: {
        backgroundColor: color.blue,
        padding: 8,
        borderRadius: 5,
        width: 70,
        alignSelf: 'flex-end',
        marginTop: 40,
        marginRight: 50
    },
    registerButtonText: {
        color: color.white,
        textAlign: 'center',
        fontSize: 16
    },
    loginButton: {
        backgroundColor: 'white',
        padding: 8,
        borderRadius: 10,
        width: 150,
        alignSelf: 'flex-end',
        margin: 20,
        marginRight: 50
    },
    loginButtonText: {
        color: color.textyellow,
        fontSize: 14,
        borderBottomColor: color.textyellow,
        borderBottomWidth: 2,
        margin: 20,
        marginRight: 40,
        alignSelf: 'flex-end',
        width: 130
    }
})