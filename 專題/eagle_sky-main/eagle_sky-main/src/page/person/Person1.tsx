import  React, { useState, useEffect } from 'react';
import { StackNavigationProp } from '@react-navigation/stack';
import { navigationType } from '../navigationType'
import { color } from '../../style'
import GroupDropdown from '../../component/GroupDropdown'
import { getUserName, userName, changeGroupName, addGroupMember, leaveGroup, getGroups, groups } from '../../page/person/loadAndUpload';
import { userId } from '../../user';
import { EditGroup, AddMember, RemoveMember } from '../../page/person/Person1function';
import {
  Image,
  SafeAreaView,
  FlatList,
  StatusBar,
  StyleSheet,
  AppRegistry,
  Text,
  useColorScheme,
  View,
  Button,
  TouchableOpacity,
} from 'react-native';

export default function GroupScreen({navigation}:navigationType): JSX.Element{

  class Group {
    id: number = 0;
    name: string = "";
    concert: string = "";
    areaId: number = 0;
    accounts: {
        id: number;
        name: string;
    }[] = [];
  }

  //拿account內所有資料
  let [showGroups, updateGroups] = React.useState([]  as Group[]);
      useEffect(() => {
          Promise.all([getGroups()]).then(() =>
              updateGroups(groups)
          )
      }, [])
  console.log("getGroups:",showGroups[0]);




  let [showUserName, updateUserName] = React.useState("");
        useEffect(() => {
                  Promise.all([getUserName()]).then(() =>
                      updateUserName(userName)
                  )
        }, [userId])




    return (
      <SafeAreaView style={styles.background}>
        <FlatList
          contentInsetAdjustmentBehavior="automatic"
          style={styles.background}
          contentContainerStyle={{ justifyContent: 'space-around' }}
          data={[1]} // 添加一個佔位bit
          renderItem={({ item }) => (
            <View style={styles.container}>
              <Text style={{ height: 20 }} />

              <FlatList
                data={showGroups}
                renderItem={({ item }) => (
                  <GroupDropdown
                    navigation={navigation}
                    group={item}
                    user={showUserName}
                    groupId={item.id}
                    groupName={item.name}
                  />
                )}
                initialNumToRender={3}
                showsVerticalScrollIndicator={false}
                ItemSeparatorComponent={() => <View style={styles.line} />}
                ListFooterComponent={() => <View style={styles.line} />}
              />
            </View>
          )}
        />
      </SafeAreaView>
    );
}

const styles = StyleSheet.create({
  background: {
    //paddingTop: 0,
    paddingHorizontal: 10,
    flex: 1,
    height: '100%',
    backgroundColor: color.backgroundColor,
  },
  container: {
    flex:1,
  },
  line: {
    height: 1,
    width: '85%',
    backgroundColor: color.blue,
    flex: 1,
    padding: 2,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
  },
});

