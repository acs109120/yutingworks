import React, { Component, useState, useEffect } from 'react';
import { StackNavigationProp } from '@react-navigation/stack';
import { navigationType } from './navigationType'
import { color } from '../style'
import { BookingCard } from '../component/BookingCard';
// import GroupDropdown from '../component/GroupDropdown';
import { loadAreas, areas, loadResellAreas, resellAreas } from '../page/booking/loadAndUpload';
import { loadConcert, concert } from '../page/concert/load';
import { loadTickets, tickets } from '../page/ticket/load';
import { useRoute } from '@react-navigation/native';
import { userId } from '../user';
import {
    Button,
    View,
    StyleSheet,
    Text,
    TouchableOpacity,
    SafeAreaView,
    FlatList,
    TextInput,
} from 'react-native';

export default function Booking({navigation}:navigationType): JSX.Element{

    class Concert {
        title: string = "";
        date: string = "";
        time_start: string = "";
        time_end: string = "";
    }

    class Area {
        id: number=0;
        area: string="Z";
        price: number=0;
        seat_total: number=0;
    }

    class ResellArea {
        id: number=0;
        area: string="Z";
        price: number=0;
        seat_total: number=0;
    }


const route = useRoute();
let concertId = route.params && route.params.concertId;
let sellWhat = route.params && route.params.sellWhat;


let [showAreas, updateAreas] = React.useState([]  as Area[]);
      useEffect(() => {
        loadAreas(concertId).then((area) => {
          const getAreas = area.map((areas) => ({
                 id: areas.id,
                area: areas.area,
                seat_total: areas.seat_total,
                price: areas.price
          }));
          updateAreas(getAreas);
        });
      }, []);


let [showResellAreas, updateResellAreas] = React.useState([]  as ResellArea[]);
      useEffect(() => {
        loadResellAreas(concertId).then((resellArea) => {
          const getResellAreas = resellArea.map((resellAreas) => ({
                id: resellAreas.id,
                area: resellAreas.area,
                seat_total: resellAreas.seat_total,
                price: resellAreas.price
          }));
          updateResellAreas(getResellAreas);
        });
      }, []);

        let [showConcert, updateConcert] = React.useState({
          title: "",
          date: "",
          time_start: "",
          time_end: "",
        });

        useEffect(() => {
          loadConcert(concertId).then((loadedConcert) => {
            if (loadedConcert) {
              updateConcert({
                title: loadedConcert.title,
                date: loadedConcert.date,
                time_start: loadedConcert.time_start,
                time_end: loadedConcert.time_end,
              });
            }
          });
        }, [concertId]);



    return(
        <SafeAreaView style={styles.background}>
            <FlatList
                 contentInsetAdjustmentBehavior="automatic"
                 style={styles.background}
                 contentContainerStyle={{justifyContent: 'space-around'}}
                 data={[1]} // 添加一個佔位bit
                 renderItem={({ item }) => (
            <View style={styles.container}>
                <Text style={styles.header}>{showConcert.title}</Text>
                <Text style={styles.header}>訂票</Text>
                <Text style={{ height: 20 }} />
                    <FlatList
                        data={sellWhat ? showAreas : showResellAreas}
                        renderItem={({item}) => {return <BookingCard seat={item.area} price={item.price}
                            remaining={item.seat_total} navigation={navigation} sellWhat={sellWhat} concertId={concertId} areaId={item.id} />}}
                        initialNumToRender={3}
                        style = {styles.scroll}
                        showsVerticalScrollIndicator = {false}
                        ItemSeparatorComponent={() => {return <View style={{height: 35}} />}}
                        ListFooterComponent={() => {
                        return <View style={{height: 20}} />}}
                        //keyExtractor={(item) => item.seat}
                        //nestedScrollEnabled // 添加此属性
                    />

            </View>
            )}/>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    background: {
        paddingTop: 10,
        paddingHorizontal: 25,
        flex: 1,
        height: "100%",
        backgroundColor: color.backgroundColor,
    },
    container:{
        marginTop: 15,
        paddingHorizontal: 0,
    },
    header: {
        marginTop: 5,
        fontSize: 26,
        fontWeight: '900',
        color: color.darkBlue,
        textAlign: 'center', // 文字水平居中
        textAlignVertical: 'center', // 文字垂直居中（僅Android支持）
    },
    scroll: {
        // width: "100%",
        marginLeft: -30,
        marginTop: 20,
    },
    list: {
        position: "absolute",
        zIndex: 2,
    },
});
