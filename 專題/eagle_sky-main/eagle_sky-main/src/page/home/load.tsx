import { http, sqlURL } from '../../../http'
import eagleSky from '../../assets/eagleSky.png'
import qs from 'qs'

class Concert {
    id: number = 0;
    title: string = "";
    date: string = "";
    time_start: string = "";
    time_end: string = "";
    img: string = "";
}

class FilterDates {
    chooseStartDate: string = "";
    chooseEndDate: string = "";
    discardStartDate: string = "";
    discardEndDate: string = "";
}

let emptyfilter : FilterDates = {
    chooseStartDate: "",
    chooseEndDate: "",
    discardStartDate: "",
    discardEndDate: ""
}

let concerts : Concert[] = []

async function loadConcerts(filterDates: FilterDates = emptyfilter, searchText: string = ""){
    let querySearch = qs.stringify({
        filters: {
            title: {
                '$containsi': searchText
            }
        }
    })
    let queryChoosefilter = qs.stringify({
        filters: {
            '$and': [
                filterDates.chooseStartDate? 
                {date: {'$gte': filterDates.chooseStartDate}}:
                {},
                filterDates.chooseEndDate?
                {date: {'$lte': filterDates.chooseEndDate}}:
                {}
            ],
            '$or': [
                filterDates.discardStartDate? 
                {date: {'$lte': filterDates.discardStartDate}}:
                {},
                filterDates.discardEndDate? 
                {date: {'$gte': filterDates.discardEndDate}}:
                {}
            ]
        }
    })

    await http.get('/api/concert-alls?populate=photo&sort[0]=date&' +
                    querySearch + '&' + queryChoosefilter ).then(res => {
        concerts = res.data.data.map((item: any) => {
            return ({
                id: item.id,
                title: item.attributes.title,
                date: item.attributes.date,
                time_start: item.attributes.time_start,
                time_end: item.attributes.time_end,
                img: (!item.attributes.photo.data)?
                    eagleSky:
                    sqlURL + item.attributes.photo.data.attributes.formats.thumbnail.url
            })
        })
        concerts = [...concerts]
    })
    return concerts;
}

export {loadConcerts, concerts}