import cv2
from pyzbar.pyzbar import decode
import pymysql
from datetime import datetime
import datetime
import time

def writeDB(id):
    time_now = datetime.datetime.now()
    localtime = time_now.strftime("20%y-%m-%d %H:%M:%S")
    #print(localtime)
    #print(nexthour)
    playride="IMO1"
    #資料庫連線設定
    db = pymysql.connect(host='localhost', port=3306, user='root', passwd='', db='playground', charset='utf8')
    #建立操作游標
    cursor = db.cursor()
    cursor1 = db.cursor()
    #SQL語法  "SELECT rides1,rides2,rides3 From playground Where id = '" + id +"'"
    
    selectsql = "SELECT estimate FROM `playground` WHERE id='" + id + "'"
    #checkride = "SELECT rides1,rides2,rides3 FROM `playground` WHERE id = %s "
    checkrides1 = "SELECT rides1 FROM `playground` WHERE id = %s "
    checkrides2 = "SELECT rides2 FROM `playground` WHERE id = %s "
    checkrides3 = "SELECT rides3 FROM `playground` WHERE id = %s "
    #insertid = "INSERT INTO playground(id, time, estimate, count, rides1,rides2,rides3) VALUES ('"+id+"','"+ localtime +"','"+nexthour+"','1','slides','','')"
    updaterides1 = "UPDATE playground SET count = %s , rides1 = %s WHERE id = %s"
    updaterides2=  "UPDATE playground SET count = %s , rides2 = %s WHERE id = %s"
    updaterides3=  "UPDATE playground SET count = %s , rides3 = %s WHERE id = %s"
    
    insertride="INSERT INTO rides(name,reserve_time,id) VALUES ('"+playride+"','"+localtime+"','"+id+"')"
    #執行語法
    
    ####################################
    try:
        cursor.execute(selectsql)
        estimate_time=list(cursor.fetchone())
        if estimate_time == None:
            print("Unavaliable IMO")
            return False
        #print(time_now)
        print(localtime)
        #print(estimate_time[0])
        
        if str(localtime) <= str(estimate_time[0]) :
            #print("successful appoinment")
            flag=1
        else:
            print("Unavaliable IMO")
            flag=0
        ################################################
        cursor.execute(checkrides1,id)
        check1=cursor.fetchall()
        cursor.execute(checkrides2,id)
        check2=cursor.fetchall()
        cursor.execute(checkrides3,id)
        check3=cursor.fetchall()
        
        if check1[0][0] == playride or check2[0][0] == playride or check3[0][0] == playride:
            flag=0
            print("已經有你的預約資料了ㄛ")
        
        if(flag):
            print("預約成功，待會見 IMO~")
            sum=0
            if check1[0][0] != "" :
                sum=sum+1
            if check2[0][0] != "" :
                sum=sum+1
            if check3[0][0] != "" :
                sum=sum+1
        
            if sum == 0  :
                cursor1.execute(updaterides1,("1",playride,id))
                cursor.execute(insertride)
                #print("0筆資料")
            elif sum == 1  :
                if check1[0]== "" : #rides1是空的，資料加入rides1
                    cursor1.execute(updaterides1, ("2", playride,id))
                    cursor.execute(insertride)
                    #print("已有1筆資料，資料加入rides1")
                else:                  #rides1不是空的，代表有一筆資料在rides1，資料加入rides2
                    cursor1.execute(updaterides2, ("2", playride, id))
                    cursor.execute(insertride)
                    #print("已有1筆資料在rides1，資料加入rides2")
            elif sum == 2:
                if check1[0] == "":  #rides1是空的，資料加入rides1
                    cursor1.execute(updaterides1, ("3", playride, id))
                    cursor.execute(insertride)
                    #print("已有2筆資料，資料加入rides1")
                elif check2[0] =="": #rides2是空的，資料加入rides2
                    cursor1.execute(updaterides2, ("3", playride, id))
                    cursor.execute(insertride)
                    #print("已有2筆資料，資料加入rides2")
                else:                   #rides3是空的，資料加入rides3
                    cursor1.execute(updaterides3, ("3", playride, id))
                    cursor.execute(insertride)
                    #print("已有2筆資料，資料加入rides3")
            else :
                print("當前已預約3個設施了，不能再繼續預約了喔 IMO~")
        
            db.commit()
        
        #關閉連線
        db.close()
    except:
        db.rollback()
        print('Error Try Again')
        db.close()

# 捕获摄像头输入
cap = cv2.VideoCapture(0)
repeat_qrcodes = ""

while True:
    # 读取摄像头帧
    ret, frame = cap.read()

    # 将帧转换为灰度图像
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    # 检测 QR 码
    qr_codes = decode(gray)
    
    #print(qr_codes)
    
    
        # 处理解码结果
    for qr_code in qr_codes:
        if repeat_qrcodes != qr_code.data.decode('utf-8'):
            #print(qr_code.data.decode('utf-8'))
            repeat_qrcodes = qr_code.data.decode('utf-8')
            writeDB(str(int(repeat_qrcodes)))
        
    # 显示摄像头输入
    #cv2.imshow('frame', frame)

    # 按 'q' 键退出循环
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
    

# 释放摄像头
cap.release()

# 关闭窗口
cv2.destroyAllWindows()