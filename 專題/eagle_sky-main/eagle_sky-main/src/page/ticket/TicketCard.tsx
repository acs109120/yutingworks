import React from 'react';
import { View, StyleSheet, Text, Image, TouchableOpacity } from 'react-native';
import ticketImg from '../../assets/ticketImg2.png'
import eagleSky from '../../assets/eagleSky.png'
import { color } from '../../style'
import { refund, sell, sellWaition, noReact } from './TicketCardOptions'


class Ticket {
    id: number = 0;
    seat: any = "";
    pay_finished : boolean = false;
    sell_waiting : boolean = false;
    area: string = "Z";
    areaId: number = 0;
    concertId : number = 0;
    concert: string = "演唱會名稱"
    date: string = "";
    time_start: string = "";
    time_end: string = "";
    img: string = "";
}

function TicketBottun(date: string, sell_waiting: boolean, pay_finished: boolean, areaId: number, ticketId: number){
    let kind = noReact()
    if(pay_finished){
        let dateRange = (new Date(date).getTime()-new Date().getTime())/(1000 * 3600 * 24)
        if(dateRange > 10)
            kind = refund(areaId, ticketId)
        else if(dateRange > 2){
            if(sell_waiting)  
                kind = sellWaition()
            else
                kind = sell(areaId, ticketId)
        }
        else
            return(<View />)
    }
    return(
        kind
    )
}


export default function TicketCard(ticket: Ticket){
    return (
        <View style={styles.body}>
            <Image source={ticketImg} style={styles.ticketImgCss} />
            <Image source={ticket.img ?
                {uri: ticket.img} :
                eagleSky}
                style={styles.imgCss}
                resizeMode={ticket.img ? "cover" : "contain"}
            />
            <View style={styles.left}>
                <View>
                    <Text style={styles.title} ellipsizeMode='tail' numberOfLines={2}> {ticket.concert} </Text>
                    <Text style={styles.date}> {ticket.date} </Text>
                </View>
                <View style={styles.ticketBottunBlock}>
                    {TicketBottun(ticket.date, ticket.sell_waiting, ticket.pay_finished, ticket.areaId, ticket.id)}
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    body: {
        flexDirection: 'row',
        width: "75%",
        flex: 1,
    },
    ticketImgCss: {
        width: 200,
        height: 120,
        marginRight: 10
    },
    imgCss: {
        position: 'absolute',
        width: 130,
        height: 90,
        backgroundColor: '#FFF',
        marginVertical: 15,
        marginLeft: 10
    },
    left: {
        justifyContent: 'space-between',
        width: "53%"
    },
    title: {
        color: color.darkBlue,
        fontSize: 14,
        fontWeight: "600",
        marginTop: 5,
    },
    date: {
        color: color.darkBlue,
        fontSize: 12
    },
    ticketBottunBlock: {
        flex: 1,
        alignItems: 'flex-end',
        justifyContent: 'flex-end',
        paddingBottom: 10
    }
})