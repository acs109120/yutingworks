import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { color } from '../style'

const CountdownTimer = () => {
  const initialTime = 5 * 60; // 5分鐘
  const [timeLeft, setTimeLeft] = useState(initialTime);

  useEffect(() => {
    const timer = setInterval(() => {
      if (timeLeft > 0) {
        setTimeLeft(timeLeft - 1);
      }
    }, 1000);

    return () => clearInterval(timer);
  }, [timeLeft]);

  const formatTime = (seconds:number) => {
    const minutes = Math.floor(seconds / 60);
    const remainingSeconds = seconds % 60;
    return `${minutes}:${remainingSeconds < 10 ? '0' : ''}${remainingSeconds}`;
  };

  return (
    <View style={styles.container}>
      <Text style={styles.timerText}>請在時限內完成訂單：{formatTime(timeLeft)}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  timerText: {
    fontSize: 12,
    fontWeight: 'bold',
    color: color.darkBlue,
    textAlign:'right',
  },
});

export default CountdownTimer;
