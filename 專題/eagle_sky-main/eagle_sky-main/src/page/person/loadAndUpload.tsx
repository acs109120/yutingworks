import { http } from '../../../http'
import qs from 'qs'
import { userId } from '../../user';

async function changeName(id :number = 2, content: string){
    await http.put('/api/accounts/'+ id.toString(), {
        data:{
            name: content
        }
    })
}

async function changeEmail(id :number = 2, content: string){
    await http.put('/api/accounts/'+ id.toString(), {
        data:{
            name: content
        }
    })
}

async function changePassword(id :number = 2, content: string){
    await http.put('/api/accounts/'+ id.toString(), {
        data:{
            name: content
        }
    })
}

async function changeGroupName(groupId :number = 1, content: string){
    await http.put('/api/groups/'+ groupId.toString(), {
        data:{
            name: content
        }
    })
}

let userName: string = "";
async function getUserName(userId: number = 2){
    await http.get('/api/accounts/' + userId.toString()).then(res => {
        userName = res.data.data.attributes.name
    })
}

async function addGroupMember(groupId :number = 1, recieverId: number=2, senderId: number=1){
    await http.post('/api/informs?populate=account&populate=group', {
        data:{
            kind: true,
            account: recieverId,
            group: groupId,
            sender: senderId,
        }
    })
}

class Group {
    id: number = 0;
    name: string = "";
    concert: string = "";
    areaId: number = 0;
    accounts: {
        id: number;
        name: string;
    }[] = [];
}

let groups: Group[] = []

function getGroupsQuery(userId: number = 2){
    let query = qs.stringify({
        filters: {
            accounts: {
                id: {
                    '$contains': userId
                }
            }
        }
    })
    return query
}

async function getGroups(){
    await http.get('/api/groups?populate=area.concert_all&populate=accounts&' + getGroupsQuery(userId)).then(res => {
        groups = res.data.data.map((item: any) => {
            return {
                id: item.id,
                name: item.attributes.name,
                concert: item.attributes.area.data.attributes.concert_all.data.attributes.title,
                areaId: item.attributes.area.data.id,
                accounts: item.attributes.accounts.data.map((account: any) => {
                    return {
                        id: account.id,
                        name: account.attributes.name
                    }
                })
            }
        })
    })
}

async function leaveGroup(groupId: number = 1, userId: number = userId){
    let members: number[] = []
    await http.get('/api/groups/' + groupId.toString() + '?populate=accounts').then(res => {
        res.data.data.attributes.accounts.data.map((item:any) => {
            if(item.id != userId)
                members.push(item.id)
        })
    })
    await http.put('/api/groups/'+ groupId.toString(), {
        data:{
            accounts: members
        }
    })
    members.map((item) => {
        http.post('./api/informs',{
            data: {
                kind: false,
                account: item,
                groupId: groupId,
                sender: userId
            }
        })
    })
}

async function createGroup(groupname :string = "", accounts: number = 2, areaId: number = 2){
    const response = await http.post('/api/groups?populate=account&populate=group', {
        data:{
            name: groupname,
            accounts: accounts,
            area: areaId
        }
    })
    const groupidd = response.data.data.id; 
    console.log("groupidd:", groupidd);

    return groupidd;
}

let accountName: string = "";
async function getAccountName(accountId: number = 2){
    await http.get('/api/accounts/' + accountId.toString()).then(res => {
        accountName =  res.data.data.attributes.name
    })
    return accountName;
}

function setQuery(userId: number = 2){
    let query = qs.stringify({
        filters: {
            account: {
                id:{
                    '$eq': userId
                }
            }
        }
    })
    return query
}

let ticketConcertAreasOne: number[] = []
async function getTicketConcertAreasOne(userId: number = 2){
    await http.get('/api/tickets?populate=area&' + setQuery(userId)).then(res => {
        ticketConcertAreasOne = res.data.data.map((item: any) =>
            item.attributes.area.data.id
        )
    })
    return ticketConcertAreasOne;
}

class TicketConcertAreas {
    id: number = 0;
    concertName: string = "";
    areaName: string = "";
}
let ticketConcertAreas: TicketConcertAreas[] = []
async function getTicketConcertAreas(userId: number = 2){
    await http.get('/api/tickets?populate=area.concert_all&' + setQuery(userId)).then(res => {
        ticketConcertAreas = res.data.data.map((item: any) => {
            return {
                id: item.attributes.area.data.id,
                concertName: item.attributes.area.data.attributes.concert_all.data.attributes.title,
                areaName: item.attributes.area.data.attributes.area
            }
        })
    })
    return ticketConcertAreas;
}

class Inform {
    id: number = 0;
    kind: boolean = false;
    groupId: number = 2;
    groupName: string = "";
    groupAreaId: number = 0;
    sender: string = "";
    concertName: string = "";
}

let informs: Inform[] = []

async function getInforms(){
    await http.get('/api/informs?populate=group.area&populate=account&populate=sender&' + setQuery(userId)).then(res => {
        informs = res.data.data.map((item: any) =>{
            return ({
                id: item.id,
                kind: item.attributes.kind,
                groupId: item.attributes.group.data? item.attributes.group.data.id: null,
                groupName: item.attributes.group.data? item.attributes.group.data.attributes.name: null,
                groupAreaId: item.attributes.group.data
                            ?(item.attributes.group.data.attributes.area.data?
                                item.attributes.group.data.attributes.area.data.id: null)
                            :null,
                sender: item.attributes.sender.data? item.attributes.sender.data.attributes.name: null,
                concertName: item.attributes.concertName
            })
        })
    })
    return informs;
}

async function checkAddgroup(groupId :number = 3){
    let members: number[] = []
    await http.get('/api/groups/' + groupId.toString() + '?populate=accounts').then(res => {
        members = res.data.data.attributes.accounts.data.map((item:any) => {
            return (item.id)
        })
    })
    members.push(userId)
    await http.put('/api/groups/'+ groupId.toString(), {
        data:{
            accounts: members
        }
    })
}

async function checkRejectGroup(informId :number = 3){
    await http.delete('/api/informs/'+ informId.toString(), )
}

let groupConcertAreas: number[] = []
async function getGroupConcertAreas(){
    await http.get('/api/groups/?populate=accounts&populate=area&' + getGroupsQuery(userId)).then(res => {
        groupConcertAreas = res.data.data.map((item:any) => {
            return (item.attributes.area.data.id)
        })
    })
}
class Concert {
    id: number = 0;
    title: string = "";   
}

let concerts : Concert[] = []

async function loadConcerts(){
    await http.get('/api/concert-alls?populate=photo').then(res => {
        concerts = res.data.data.map((item: any) => {
            return ({
                id: item.id,
                title: item.attributes.title,        
            })
        })
        concerts = [...concerts]
    })
    return concerts;
}

class Concertarea {
    title: string = "";
    areaId: number = 0;
}
///
let concertarea : Concertarea
let concertareaid :number[]=[];
async function loadConcertArea(id :number){
    await http.get('/api/concert-alls/'+ id.toString() ).then(res => {
        concertareaid = res.data.data.map((item: any) =>{
            return ({
                title:  item.attributes.title,
                areaId: item.attributes.areas.id,        
            })
        })
        concertareaid = [...concertareaid]
    })
    return concertareaid;
}

export { changeName, changeEmail, changePassword, getUserName, userName,                              //Person
        changeGroupName, addGroupMember, leaveGroup, getGroups, groups,                               //Person1
        loadConcertArea,concertarea,createGroup, getAccountName, accountName,getTicketConcertAreasOne, ticketConcertAreasOne,loadConcerts, concerts, getTicketConcertAreas, ticketConcertAreas,//Person2
        getInforms, informs, checkAddgroup, checkRejectGroup, getGroupConcertAreas, groupConcertAreas }    //Person3