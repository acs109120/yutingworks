import pymysql

db = pymysql.connect(host='localhost', port=3306, user='root', passwd='', db='playground', charset='utf8')
sqlSearch = "DELETE FROM rides"
cursor = db.cursor()
try:
  #搜尋資料庫
  cursor.execute(sqlSearch)
  db.commit()
  print('Clear rides')
except:
  #發生錯誤時停止執行SQL
  db.rollback()
  print('Error Try Again')
  db.close()
  
sqlSearch = "DELETE FROM playground"
cursor = db.cursor()
try:
  #搜尋資料庫
  cursor.execute(sqlSearch)
  db.commit()
  print('Clear playground')
except:
  #發生錯誤時停止執行SQL
  db.rollback()
  print('Error Try Again')
  db.close()