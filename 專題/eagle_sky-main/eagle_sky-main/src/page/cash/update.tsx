import { http } from '../../../http';
import { userId } from '../../user';
import qs from 'qs';

async function cashTicket(areaId: number = 1){
    await http.post('/api/tickets/', {
        data:{
            account: userId,
            area: areaId
        }
    })
}

let query = qs.stringify({
    filters: {
        tickets: {
            sell_waiting: {
                '$eq': true
            }
        }
    }
})

async function cashResellTicket(areaId: number = 1, concertName: string = ""){
    let tickets :number[] = [];
    await http.get('/api/areas/' + areaId.toString() + '?populate=tickets').then((res) => {
        res.data.data.attributes.tickets.data.map((item: any) => {
            if(item.attributes.sell_waiting)
                tickets.push(item.id)
        })
    })
    let ticketId = Math.floor(Math.random()*tickets.length)
    ticketId = tickets[ticketId]
    await http.put('/api/tickets/' + ticketId.toString(), {
        data:{
            account: userId,
            sell_waiting: false
        }
    })

    let accountId: number = 1;
    await http.get('/api/tickets/' + ticketId.toString() + '?populate=account').then((res) => {
        accountId = res.data.data.attributes.account.data.id
    })
    await http.post('/api/informs',{
        data: {
            kind: false,
            account: accountId,
            concertName: concertName
        }
    })
}

export { cashTicket, cashResellTicket }