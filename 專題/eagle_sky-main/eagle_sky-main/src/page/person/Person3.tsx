//Person3
// -要拿account在此演唱會的位置區域  (與inform的areaid作比對，確定他有這張演唱會的票) 用getTicketConcertAreas
// -拿account所有的群組的areaid (防止他在同一個演唱會裡有兩個群組 )用getGroupConcerts
import { color } from '../../style'
import { navigationType } from '../navigationType'
import React, {useState, useEffect ,Component } from 'react';
import type {PropsWithChildren} from 'react';
import { 
  Image,
  SafeAreaView,
  TextInput,
  ScrollView,
  StatusBar,
  StyleSheet,
  AppRegistry,
  Text,
  useColorScheme,
  View,
  Alert ,
  Button,
  TouchableOpacity,
} from 'react-native';
import {
  Colors,
} from 'react-native/Libraries/NewAppScreen';
import { getInforms,informs,checkAddgroup,checkRejectGroup,
  getTicketConcertAreasOne,ticketConcertAreasOne,getGroupConcertAreas,
  groupConcertAreas,getGroups,groups } from './loadAndUpload';
import { useNavigation } from '@react-navigation/native';
const backgroundStyle = {
  backgroundColor: color.backgroundColor,
};

let gpIdd: number[] = [];
let gpAreaID: number[] = [];
let informDeleteID: number[] = []; //接收拒絕 刪除通知
let receiveDeletID:number[] = []; //收到
export default function Home({ navigation }: navigationType) {
  
  class Inform {
    id: number = 0;
    kind: boolean = false;
    groupId: number = 2;
    groupName: string = "";
    groupAreaId: number = 0;
    sender: string = "";
    concertName: string = "";
}
class Group {
  id: number = 0;
  name: string = "";
  concert: string = "";
  areaId: number = 0;
  accounts: {
      id: number;
      name: string;
  }[] = [];
}
//拿account所有informs
let [showInforms, updateInforms] = React.useState([]  as Inform[]);
useEffect(() => {
    Promise.all([getInforms()]).then(() =>
        updateInforms(informs)
    )
}, [])
//console.log("inform:",showInforms);
//console.log("123的長度:",showInforms.length);

  //拿這account所有群組歸屬的演唱會areaid [areaId的陣列]
  let [showGroupAreas, updateGroupAreas] = React.useState([]  as number[]);
  useEffect(() => {
      Promise.all([getGroupConcertAreas()]).then(() =>
      updateGroupAreas(groupConcertAreas)
      )
  }, [])
 // console.log("getGroupConcertAreas:",showGroupAreas);

  //使用者所有的票的區域
  let [showTicketAreas, updateTicketAreas] = React.useState([]  as number[]);
  useEffect(() => {
      Promise.all([getTicketConcertAreasOne()]).then(() =>
      updateTicketAreas(ticketConcertAreasOne)
      )
  }, [])
  //console.log("showTicketAreas:",showTicketAreas);

//拿account內所有資料
  // let [showAccoutGroups, updateAccoutGroups] = React.useState([]  as Group[]);
  // useEffect(() => {
  //     Promise.all([getGroups()]).then(() =>
  //         updateAccoutGroups(groups)
  //     )
  // }, [])
  //console.log("getGroups:",showAccoutGroups);


function determinArea (index){  //判斷account所有演唱會的areaid是否有 跟inform之groupid一樣的(判斷同區域)
 // 要拿account在此演唱會的位置區域  (與inform的areaid作比對，確定他有這張演唱會的票) 用getTicketConcertAreas
    let isdarea = false;       
    for (let i = 0; i < showTicketAreas.length+1; i++) {
      if (showTicketAreas[i] === gpAreaID[index]) {
        isdarea = true;
        console.log("isdarea:" ,isdarea);
        break;   // 找到了，代表有同區域的票 
      }
      
      console.log("gpAreaID[index]",gpAreaID[index])
      console.log("isdarea:" ,isdarea);
    }   

    //判段account在此演唱會是否加過群組了
    let dgroup = false;             
    for (let i = 0; i < showGroupAreas.length+1; i++) {
      if (showGroupAreas[i] === gpAreaID[index]) {
          dgroup = true;        
          console.log("determingroup:" ,dgroup);
          console.log("邀請之areaid: ",gpAreaID[index])
          break;   // 找到了，代表已有加入此演唱會的群組了 
      }
      console.log("determingroup:" ,dgroup);
    }        
    if(!dgroup && isdarea){
      return true;
    }else{
      return false;
    }
};

// const determinGroup = (index) => {  //查group內account中id是否有跟現在userid一樣的 有:代表有群組 
// // 拿account所有的群組的areaid (防止他在同一個演唱會裡有兩個群組 )用getGroupConcerts
//     let dgroup = false;             
//     for (let i = 0; i < showGroupAreas.length+1; i++) {
//       if (showGroupAreas[i] === gpAreaID[index]) {
//           dgroup = true;        
//           console.log("determingroup:" ,dgroup);
//           console.log("邀請之areaid: ",gpAreaID[index])
//           break;   // 找到了，代表已有加入此演唱會群組了 
//       }
//       console.log("determingroup:" ,dgroup);
//     }        
// };

  const showTipAccept = (index) => {
        Alert.alert('接受邀請');
        checkAddgroup(gpIdd[index]);
        checkRejectGroup(informDeleteID[index]);
         // 更新showInforms状态
        const updatedInforms = [...showInforms];
        updatedInforms.splice(index, 1); // 从数组中删除相应项
        updateInforms(updatedInforms);
        reloadPage();
  };

  const showTipDelete = () => {
    Alert.alert('拒絕邀請');
    reloadPage();
  };

  const showAlertAccept = (index) => {
    Alert.alert(
      '接受邀請',
      'Are you sure ?',
      [
        {
          text: '確認',
          onPress: () => {
            //hideInvitation(index);
            if(!determinArea(index)){
              Alert.alert('[無此演場會套票]或[已加入過群組]');
            }else{
              showTipAccept(index);
              
            }
            
            
          },
        },
        { text: '取消', style: 'cancel' },
      ],
      { cancelable: false }
    );
  };

  const showAlertDelete = (index) => {
    Alert.alert(
      '拒絕邀請',
      'Are you sure ?',
      [
        {
          text: '確認',
          onPress: () => {
            //hideInvitation(index);
            showTipDelete();
            checkRejectGroup(informDeleteID[index]);
             // 更新showInforms
             const updatedInforms = [...showInforms];
             updatedInforms.splice(index, 1); // 刪除對應項
             updateInforms(updatedInforms);
          },
        },
        { text: '取消', style: 'cancel' },
      ],
      { cancelable: false }
    );
  };

  const deleteReceive = (index) => {
   //更新資料
    const updatedInforms = [...showInforms];
    updatedInforms.splice(index, 1); // 刪除對應資料
    updateInforms(updatedInforms);
    reloadPage();
  };

  const navigations = useNavigation();

  const reloadPage = () => {
    navigations.navigate('Person'); 
    console.log("reload");
  };
    return (
      
      
      <SafeAreaView style={styles.background}>
    
        <ScrollView
          
          contentInsetAdjustmentBehavior="automatic"
          style={styles.background}
        >
        <View style={styles.divin}> 

        {showInforms.map((item, index) => (
          item.kind&&(
            
            <View key={index}>  
                <View style={styles.divmem1}> 
                    <Text style={styles.word}> {item.groupName}  邀請您加入  </Text>
                    <Text style={{height: 12}}/>
                    <View style={styles.inputContainer}>
                        <TouchableOpacity
                          activeOpacity={0.8}
                          style={styles.BtnContainer2}
                          onPress={() => {
                            showAlertAccept(index);
                             gpIdd[index] = item.groupId;
                             informDeleteID[index]=item.id;
                             gpAreaID[index]=item.groupAreaId;
                              
                             gpAreaID[index];
                             //determinArea(index);

                          }}
                        >
                          <View>
                            <Text style={styles.Title1}>接受</Text>
                          </View>
                        </TouchableOpacity>
                    
                        <TouchableOpacity
                          activeOpacity={0.8}
                          style={styles.BtnContainer}
                          onPress={() => {
                             showAlertDelete(index);
                             gpIdd[index] = item.groupId;
                            }}
                        >
                          <View>
                            <Text style={styles.Title2}>拒絕</Text>
                          </View>
                        </TouchableOpacity>
                    </View>
              </View>

              
            </View>
            
          )
        ))}
        {showInforms.map((item, index) => (
          !item.kind&&(
              (item.groupName !== null) ? (
                <View key={index}>
                      <View style={styles.divmem1}> 
                    <Text style={styles.word}> {item.groupName} : {item.sender} 退出群組</Text>
                    <Text style={{height: 12}}/>
                    <TouchableOpacity
                      activeOpacity={0.8}
                      style={styles.BtnContainer3}
                      onPress={() => {
                        
                        receiveDeletID[index]=item.id;
                        console.log(" receiveDeletID[index]=item.groupId ", item.groupId);
                        checkRejectGroup(receiveDeletID[index]);
                        deleteReceive(index);
                      }}
                      
                    >
                      <View>
                        <Text style={styles.Title2}>收到</Text>
                      </View>
                    </TouchableOpacity>
                  </View>
                  </View>
              ) : (
                  <View key={index}>
                  <View style={styles.divmem1}> 
                  <Text style={styles.word}>  您的 {item.concertName} 轉售票已售出 </Text>
                  <Text style={{height: 12}}/>
                  <TouchableOpacity
                    activeOpacity={0.8}
                    style={styles.BtnContainer3}
                    onPress={() => {
                        
                        receiveDeletID[index]=item.id;
                        console.log(" receiveDeletID[index]=item.groupId ", item.groupId);
                        checkRejectGroup(receiveDeletID[index]);
                        deleteReceive(index);
                    }}
                    
                  >
                    <View>
                      <Text style={styles.Title2}>收到</Text>
                    </View>
                  </TouchableOpacity>
                  </View>
                  </View>
              )
            )
          ))}
        </View>
        </ScrollView>
      </SafeAreaView>
      
    );
}


const styles = StyleSheet.create({
  background:{
    backgroundColor: color.backgroundColor,
    //minHeight:900,
    flex: 1
  },
  bckg:{
    backgroundColor: color.backgroundColor,
    minHeight:900,
  },
  divin:{  //最外層框
    borderRadius: 15,
    padding: 10,
    marginTop:20,
    marginLeft:35,
    marginRight:35,
    marginButtom:0,
       
  },
  divmem1:{  //下方新增成員之框
    borderRadius: 10, 
    backgroundColor:color.lightBlock,
    padding: 17,
    paddingTop:18,
    paddingBottom:8,
    marginBottom:25,
},

inputContainer: {
    flexDirection: 'row', // 水平排列
    marginTop:5,
    marginHorizontal:45,
    marginBottom: 0,
    justifyContent: 'space-between',
},
word: {
    fontSize: 16,
    fontWeight: '500',
    color: color.darkBlue,
    flexDirection: 'row',

},
BtnContainer2: {  //'接受' 黃按鈕
    color:color.brown, 
    fontWeight: 'bold',
    backgroundColor: color.yellow,
    width:60,
    borderRadius: 4,
    justifyContent: "center",
    alignItems: "center",
    paddingTop:3,
    paddingBottom:7,
},
BtnContainer: {  //'建立' 按鈕
  backgroundColor: color.blue,
  color:'#FFFFFF', 
  fontWeight: 'bold',
  width:60,
  borderRadius: 4,
  justifyContent: "center",
  alignItems: "center",
  paddingTop:3,
  paddingBottom:7,
},
BtnContainer3: {  //'收到' 按鈕
  backgroundColor: color.blue,
  width:60,
  borderRadius: 4,
  justifyContent: "center",
  alignItems: "center",
  alignSelf: 'center',
  paddingTop:3,
  paddingBottom:7,
},
//接受黃按鈕 字
Title1: { 
  color:color.brown, 
  fontWeight: 'bold', 
  alignSelf: 'center',
  justifyContent: "center",
},
//藍案紐
Title2: { 
  color:'#FFFFFF', 
  fontWeight: 'bold', 
  alignSelf: 'center',
  justifyContent: "center",
}
});


