import { http, sqlURL } from '../../../http'
import eagleSky from '../../assets/eagleSky.png'
import { userId } from '../../user';
import { leaveGroup } from '../person/loadAndUpload';
import qs from 'qs'

class Ticket {
    id: number = 0;
    userId: number = 0;
    seat: any = "";
    pay_finished : boolean = false;
    sell_waiting : boolean = false;
    area: string = "Z";
    areaId: number = 0;
    concertId : number = 0;
    concert: string = "演唱會名稱"
    date: string = "";
    time_start: string = "";
    time_end: string = "";
    img: string = "";
}

let query = qs.stringify({
    filters: {
        account: {
            id:{
                '$eq': userId
            }
        }
    }
})

let tickets : Ticket[] = []

async function loadTickets(){
    await http.get('/api/tickets?populate=account&populate=area&populate=area.concert_all&populate=area.concert_all.photo&'+query).then(res => {
        tickets = res.data.data.map((item: any) => {
            return ({
                id : item.id,
                userId : item.attributes.account.data.id,
                seat : item.attributes.seat?
                        item.attributes.seat:
                        "",
                pay_finished : item.attributes.pay_finished,
                sell_waiting : item.attributes.sell_waiting,
                area : item.attributes.area.data.attributes.area,
                areaId : item.attributes.area.data.id,
                concert : item.attributes.area.data.attributes.concert_all.data.attributes.title,
                concertId : item.attributes.area.data.attributes.concert_all.data.id,
                date : item.attributes.area.data.attributes.concert_all.data.attributes.date,
                time_start : item.attributes.area.data.attributes.concert_all.data.attributes.time_start,
                time_end : item.attributes.area.data.attributes.concert_all.data.attributes.time_end,
                img: (!item.attributes.area.data.attributes.concert_all.data.attributes.photo.data)?
                    eagleSky:
                    sqlURL + item.attributes.area.data.attributes.concert_all.data.attributes.photo.data.attributes.formats.thumbnail.url
            })
        })
        tickets = [...tickets]
    })
    return tickets;
}

async function refundTicket(areaId: number, ticketId: number) {
    let seat = 0;
    await http.get('/api/areas/' + areaId.toString()).then((res) => {
        seat = res.data.data.attributes.seat_total
    })
    await http.put('api/areas/' + areaId.toString(), {
        data:{
            seat_total: (seat+1)
        }
    })
    await getGroups(areaId)
    leaveGroup(groupId)
    await http.delete('/api/tickets/'+ ticketId.toString())
}

async function ResellTicket(areaId: number, ticketId: number) {
    let seat = 0;
    await http.get('/api/areas/' + areaId.toString()).then((res) => {
        seat = res.data.data.attributes.seat_resell_total
    })
    await getGroups(areaId)
    leaveGroup(groupId)
    await http.put('api/areas/' + areaId.toString(), {
        data:{
            seat_resell_total: (seat+1)
        }
    })
    await http.put('api/tickets/' + ticketId.toString(), {
        data:{
            sell_waiting: true
        }
    })
}

let groupId: number = 0
function getGroupsQuery(areaId: number = 0){
    let query = qs.stringify({
        filters: {
            accounts: {
                id: {
                    '$contains': userId
                }
            },
            area: {
                id: {
                    '$eq': areaId
                }
            }
        }
    })
    return query
}
async function getGroups(areaId: number){
    await http.get('/api/groups?populate=area&' + getGroupsQuery(areaId)).then(res => {
        res.data.data.map((item: any) => {
            groupId = item.id;
        })
    })
}

export {loadTickets, tickets, refundTicket, ResellTicket}