import { BASE_URL } from "@env";

// const sendSmsVerification = async (phoneNumber,verificationNumbers) => {
//  try {
//    const verifactionmessage="[Eaglesky] Your code is "+verificationNumbers;
//    const data = JSON.stringify({
//      To: phoneNumber,
//      channel: "sms",
//      body: verifactionmessage,
//    });

//    const response = await fetch(`${BASE_URL}/sendmessage`, {
//      method: "POST",
//      headers: {
//        "Content-Type": "application/json",
//      },
//      body: data,
//    });

//    const json = await response.json();
//    return json.success;
//  } catch (error) {
//    console.error(error);
//    console.log("in vscode1")
//    return false;
//  }
// };

const sendSmsVerification = async (phoneNumber) => {
  console.log("in sendfuntion")
  try {
    const data = JSON.stringify({
      to: phoneNumber,
      channel: "sms",
    });
    console.log("data",data)
    // const response = await fetch(`${BASE_URL}/sms-send`, {
    //   method: "POST",
    //   headers: {
    //     "Content-Type": "application/json",
    //   },
    //   body: data,
    // });
 
    const json = await response.json();
    return json.success;
  } catch (error) {
    console.error(error);
    console.log("in vscode1")
    
    return false;
  }
 };

const checkVerification = async (phoneNumber, code) => {
 try {
   const data = JSON.stringify({
     to: phoneNumber,
     code,
   });

   const response = await fetch(`${BASE_URL}/check-otp`, {
     method: "POST",
     headers: {
       "Content-Type": "application/json",
     },
     body: data,
   });

   const json = await response.json();
   return json.success;
 } catch (error) {
   console.error(error);
   console.log("in vscode2")
   return false;
 }
};

export { sendSmsVerification, checkVerification };
// module.exports = {
//  sendSmsVerification,
//  checkVerification,
// };