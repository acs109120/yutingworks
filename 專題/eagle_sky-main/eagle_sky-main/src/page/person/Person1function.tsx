import React, { useState } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, TextInput, Alert } from 'react-native';
import { color } from '../../style';
import { navigationType } from '../navigationType';
import { changeGroupName, addGroupMember, leaveGroup } from '../../page/person/loadAndUpload';
import { useRoute } from '@react-navigation/native';

let userId = 2;
let userPassword = "123";

export default function PersonScreen({route, navigation }: navigationType) {

    const kind = route.params ? route.params.kind : {kind:'ooo'}
    let groupId = route.params && route.params.groupId;
    let accountId = route.params && route.params.accountId;
    return (
        <View style={styles.body}>
            <View style={styles.block}>
                { kind=="EditGroup" && <EditGroup navigation = {navigation} groupId = {groupId}/>}
                { kind=="AddMember" && <AddMember navigation = {navigation} groupId = {groupId}/>}
                { kind=="RemoveMember" && <RemoveMember navigation = {navigation} groupId = {groupId} accountId = {accountId}/>}
            </View>
        </View>
    )
}

export function EditGroup({ navigation, groupId }: navigationType){
    const [name, groupName] = React.useState("");
    return(
        <View>
            <View style={styles.line}>
                <Text style={styles.text}>更改群組姓名：</Text>
                <TextInput
                    style={styles.textInput}
                    onChangeText={text => groupName(text)}
                    value={name}
                />
            </View>
{/*             <View style={styles.line}> */}
{/*                 <Text style={styles.text}>　　密碼：</Text> */}
{/*                 <TextInput */}
{/*                     style={styles.textInput} */}
{/*                     onChangeText={text => typePassword(text)} */}
{/*                     value={password} */}
{/*                 /> */}
{/*             </View> */}

            <View style={styles.buttonBlock}>
                <TouchableOpacity
                    style={styles.cancelButton}
                    activeOpacity={0.7}
                    onPress={() => {navigation.goBack()}}
                >
                    <Text style={styles.buttonText}>取消</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style={styles.sureButton}
                    activeOpacity={0.7}
                    onPress={()=>{
//                         if(userPassword==password)
                            changeGroupName(groupId, name)
                            console.log(groupId, name)
                        if(!name)
                            Alert.alert("群組姓名不可為空")
//                         else
//                             Alert.alert("密碼錯誤")
                    }}
                >
                    <Text style={styles.buttonText}>確定更改</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

export function AddMember({ navigation, groupId }: navigationType){
    const [password, typePassword] = React.useState('');
    const [senderID, setSenderID] = useState('');
    const [senderName, setSenderName] = useState('');
      const handleSenderID = (text:string) => {
        setSenderID(text);
      };

      const handleSenderName = (text:string) => {
        setSenderName(text);
      };
    return(
        <View>
            <View style={styles.line}>
                <Text style={styles.text}>ID：</Text>
                <TextInput
                    style={styles.textInput}
                    placeholder=""
                    onChangeText={handleSenderID}
                    value={senderID}
                />
            </View>
             <View style={styles.line}>
                 <Text style={styles.text}>姓名：</Text>
                 <TextInput
                     style={styles.textInput}
                     placeholder=""
                     onChangeText={handleSenderName}
                     value={senderName}
                 />
             </View>
{/*             <View style={styles.line}> */}
{/*                 <Text style={styles.text}>　　密碼：</Text> */}
{/*                 <TextInput */}
{/*                     style={styles.textInput} */}
{/*                     onChangeText={text => typePassword(text)} */}
{/*                     value={password} */}
{/*                 /> */}
{/*             </View> */}

            <View style={styles.buttonBlock}>
                <TouchableOpacity
                    style={styles.cancelButton}
                    activeOpacity={0.7}
                    onPress={() => {navigation.goBack()}}
                >
                    <Text style={styles.buttonText}>取消</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style={styles.sureButton}
                    activeOpacity={0.7}
                    onPress={()=>{
//                         if(userPassword==password)
                            addGroupMember(userId,groupId,senderID)
                       if(!senderName)
                            Alert.alert("姓名不可為空")
                        else if(!senderID)
                            Alert.alert("ID不可為空")
//                         else
//                             Alert.alert("密碼錯誤")
                    }}
                >
                    <Text style={styles.buttonText}>確定更改</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

export function RemoveMember({ navigation,groupId,accountId }: navigationType){
    const [password, typePassword] = React.useState('');
    return(
        <View>
            <View style={styles.line}>
                <Text style={styles.text}>是否確定移除該成員?</Text>
            </View>
{/*             <View style={styles.line}> */}
{/*                 <Text style={styles.text}>　　密碼：</Text> */}
{/*                 <TextInput */}
{/*                     style={styles.textInput} */}
{/*                     onChangeText={text => typePassword(text)} */}
{/*                     value={password} */}
{/*                 /> */}
{/*             </View> */}
            <View style={styles.buttonBlock}>
                <TouchableOpacity
                    style={styles.cancelButton}
                    activeOpacity={0.7}
                    onPress={() => {navigation.goBack()}}
                >
                    <Text style={styles.buttonText}>取消</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style={styles.sureButton}
                    activeOpacity={0.7}
                    onPress={()=>{
//                         if(userPassword==password)
                            leaveGroup(groupId,accountId)
                             console.log(groupId,accountId);
//                         else
//                             Alert.alert("密碼錯誤")
                    }}
                >
                    <Text style={styles.buttonText}>確定移除</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    body: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    block: {
        backgroundColor: color.backgroundColor,
        width: '80%',
        borderRadius: 5,
        paddingHorizontal: 25,
        paddingVertical: 10
    },
    line: {
        flexDirection: 'row',
        marginVertical: 5,
        alignItems: 'center'
    },
    text: {
        fontSize: 16,
        color: color.darkBlue,
    },
    textInput: {
        borderBottomWidth: 3,
        borderColor: color.darkBlue,
        padding: 2,
        flex: 1
    },
    buttonBlock: {
        flexDirection: 'row',
        gap: 10,
        justifyContent: 'flex-end'
    },
    cancelButton: {
        backgroundColor: color.blue,
        width: 50,
        height: 30,
        borderRadius: 5,
        alignSelf: 'flex-end',
        marginTop: 50,
        marginBottom: 20
    },
    sureButton: {
        backgroundColor: color.red,
        width: 80,
        height: 30,
        borderRadius: 8,
        alignSelf: 'flex-end',
        marginTop: 50,
        marginBottom: 20
    },
    buttonText:{
        color: "white",
        textAlign: 'center',
        alignSelf: 'center',
        lineHeight: 30
    }
})