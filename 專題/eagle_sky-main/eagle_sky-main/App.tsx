/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */
import React from 'react';
import { View, StyleSheet, Text, TextInput, Image, TouchableOpacity, FlatList } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { useNavigation } from '@react-navigation/native';
import { color } from './src/style'
import { navigationType } from './src/page/navigationType'
import Sample1 from './src/page/Sample1'
import Sample2 from './src/page/Sample2'
import Home from './src/page/home/Home'
import Cash from './src/page/cash/Cash'
import Person from './src/page/person/Person'
import Ticket from './src/page/ticket/Ticket'
import Verify from './src/page/verify/Verify'
import eagleSky from './src/assets/eagleSky2.png'
import ticketImg from './src/assets/ticketImg.png'
import personImg from './src/assets/personImg.png'
import Enter from './src/page/Enter'
import Concert from './src/page/Concert'
import Booking from './src/page/Booking'
import PersonOptions from './src/page/person/PersonOptions'
import FilterPage from './src/page/home/FilterPage'
import Person1function from './src/page/person/Person1function'
import PhoneNumberS from './src/page/screens/PhoneNumberS'
import OTP from "./src/page/screens/OTP"; 
import Gated from "./src/page/screens/Gated"; 
import Login from './src/page/login/Login';
import Register from './src/page/register/Register';
import RegisterOTP from './src/page/register/RegisterOTP';
import { userId } from './src/user';

export type RootStackParamList = {
  Home: undefined;
  ColorPalette: undefined;
};

const Stack = createStackNavigator();

function MyStack() {
  return (
    <Stack.Navigator initialRouteName={userId? "PhoneNumberS": "Home"}  id="main" screenOptions={screenOptions} >
      <Stack.Screen name="Sample2" component={Sample2} options={options} />
      <Stack.Screen name="RegisterOTP" component={RegisterOTP} options={options} />
      <Stack.Screen name="PhoneNumberS" component={PhoneNumberS} options={options} />
      <Stack.Screen name="OTP" component={OTP} options={options}/>
      <Stack.Screen name="Gated" component={Gated} options={options}/>
      <Stack.Screen name="Sample1" component={Sample1} options={options} />
      <Stack.Screen name="Verify" component={Verify} options={options} />
      <Stack.Screen name="Home" component={Home} options={options} />
      <Stack.Screen name="Person" component={Person} options={options} />
      <Stack.Screen name="Ticket" component={Ticket} options={options} />
      <Stack.Screen name="Concert" component={Concert} options={options} />
      <Stack.Screen name="Enter" component={Enter} options={options} />
      <Stack.Screen name="Cash" component={Cash} options={options} />
      <Stack.Screen name="Booking" component={Booking} options={options}/>
      <Stack.Screen name="PersonOptions" component={PersonOptions} options={miniWindowOptions}/>
      <Stack.Screen name="FilterPage" component={FilterPage} options={miniWindowOptions}/>
      <Stack.Screen name="Person1function" component={Person1function} options={miniWindowOptions}/>
      <Stack.Screen name="Login" component={Login} options={options}/>
      <Stack.Screen name="Register" component={Register} options={options}/>

    </Stack.Navigator>
  );
}

const screenOptions = ({
  keyboardHandlingEnabled: false,
  cardOverlayEnabled: false,
  headerMode: 'float'
  // animationTypeForReplace: 'pop'
  // animationEnabled: false
});

const options = ({
  title: '鳶空實名制售票',
  headerTintColor: "white",
  headerTitleAlign: 'center',
  headerStyle: {
    backgroundColor: color.darkBlue,
    height: 51
  },
  headerTitleStyle: {
    fontFamily: 'NotoSerifSC-Regular',
  },
  cardStyle: {
    flex: 1,
    backgroundColor: color.backgroundColor
  },
  headerLeft: () => (backHome()),
  headerRight: () => (
    <View style={{flexDirection: 'row', alignItems: 'center', marginRight: 10}}>
      <GoTicket />
      <GoToPerson />
    </View>
  )
});

const miniWindowOptions = ({
  title: '鳶空實名制售票',
  headerTintColor: "white",
  headerTitleAlign: 'center',
  headerStyle: {
    backgroundColor: color.darkBlue,
    height: 51
  },
  headerTitleStyle: {
    fontFamily: 'NotoSerifSC-Regular',
  },
  cardStyle: {
    flex: 1,
    backgroundColor: 'rgba(0, 0, 0, 0.5)'
  },
  presentation: 'transparentModal',
  headerLeft: () => (backHome()),
  headerRight: () => (
    <View style={{flexDirection: 'row', alignItems: 'center', marginRight: 10}}>
      <GoTicket />
      <GoToPerson />
    </View>
  )
});

function GoTicket() {
  const navigation = useNavigation();
  return (
    <TouchableOpacity
      onPress={() => navigation.navigate('Ticket')}
      activeOpacity={0.9}
    >
      <Image source={ticketImg} style={{width:35, height: 22, marginLeft:10}} />
    </TouchableOpacity>
  );
}

function GoToPerson() {
  const navigation = useNavigation();
  return (
    <TouchableOpacity 
      onPress={() => navigation.navigate('Person')}
      activeOpacity={0.9}
    >
      <Image source={personImg} style={{width:35, height: 35, marginLeft:10}} />
    </TouchableOpacity>
  );
}

function backHome() {
  const navigation = useNavigation();
  return (
    <TouchableOpacity 
      onPress={() => {navigation.reset({
        index: 0,
        routes: [{name: 'Home' as never}],
      });}}
      activeOpacity={0.8}
    >
      <Image source={eagleSky} style={{width:55, height: 55, marginLeft:10}} />
    </TouchableOpacity>
  );
}

export function App() {
  return (
    <NavigationContainer>
      <MyStack />
    </NavigationContainer>
  );
}

export default App