import React, { Component, useState, useEffect } from 'react';
import { StackNavigationProp } from '@react-navigation/stack';
import { navigationType } from '../navigationType'
import type {PropsWithChildren} from 'react';
import { styles } from './Cash_style'
import CountdownTimer from '../../component/CountdownTimer';
import PaymentDropdown from './PaymentDropdown';
import { cashTicket, cashResellTicket } from '../../page/cash/update';
import { loadConcert, concert } from '../../page/concert/load';
// import { loadAreas, areas, loadResellAreas, resellAreas } from '../../page/booking/loadAndUpload';
import { userId,userName,userPhone } from '../../user';
// import { getUserName, userName, userPhone, getUserPhone } from '../../page/person/loadAndUpload';
import { useRoute } from '@react-navigation/native';
import {
    Button,
    View,
    StyleSheet,
    Text,
    TouchableOpacity,
    SafeAreaView,
    FlatList,
    TextInput,
    Modal,
} from 'react-native';




export default function Cash({navigation}:navigationType): JSX.Element{

    class Concert {
        title: string = "";
        date: string = "";
        time_start: string = "";
        time_end: string = "";
    }
        let [showConcert, updateConcert] = React.useState({
          title: "",
          date: "",
          time_start: "",
          time_end: "",
        });

        useEffect(() => {
          loadConcert(concertId).then((loadedConcert) => {
            if (loadedConcert) {
              updateConcert({
                title: loadedConcert.title,
                date: loadedConcert.date,
                time_start: loadedConcert.time_start,
                time_end: loadedConcert.time_end,
              });
            }
          });
        }, [concertId]);

    const route = useRoute();
    let concertId = route.params && route.params.concertId;
    let sellWhat = route.params && route.params.sellWhat;
    let areaId = route.params && route.params.areaId;
    let areaArea = route.params && route.params.areaArea;
    let areaPrice = route.params && route.params.areaPrice;


    let [showNotification, setShowNotification] = React.useState(false);
    let [notificationMessage, setNotificationMessage] = React.useState("");

        const PayingButton = ({ title, sellWhat, areaId, navigation }) => {
            const handlePress = () => {
                if (sellWhat) {
                    // 如果是正常票，cashTicket
                    cashTicket(areaId);

                } else {
                    // 如果是轉售票，cashResellTicket
                    cashResellTicket(areaId, showConcert.title);
                }
            // 設置通知消息
            setNotificationMessage("付款成功");

            // 顯示通知
            setShowNotification(true);
                navigation.navigate('Home');
            };
            return(
                    <TouchableOpacity activeOpacity={0.8} onPress={handlePress}>
                        <View style={styles.button}>
                            <Text style={styles.button_text}>
                                {title}
                            </Text>
                        </View>
                    </TouchableOpacity>
                )
        }

        const Notification = ({ visible, message, onClose }) => {
                return (
                    <Modal
                        transparent={true}
                        animationType="slide"
                        visible={visible}
                        onRequestClose={() => onClose()}
                    >
                        <View style={styles.notificationContainer}>
                            <View style={styles.notification}>
                                <Text>{message}</Text>
                                <TouchableOpacity onPress={() => onClose()}>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </Modal>
                );
        }

    return(
        <SafeAreaView style={styles.background} >
            <FlatList
                            contentInsetAdjustmentBehavior="automatic"
                            style={styles.background}
                            contentContainerStyle={{justifyContent: 'space-around'}}
                            data={[1]} // 添加一個佔位bit
                            renderItem={({ item }) => (
                <View style={styles.container}>
                    <Text style={styles.header}>訂單付款</Text>
                    <Text style={{height: 10}}/>
                    <CountdownTimer />
                    <Text style={{ height: 20 }} />
                    <Text style={styles.title}>{showConcert.title}</Text>
                    <Text style={styles.word}>座位區：{areaArea}</Text>
                    <Text style={styles.word}>日期：{showConcert.date} {showConcert.time_start}~{showConcert.time_end}</Text>
                    <Text style={{ height: 20 }} />
                    <View style={styles.line} />
                    <Text style={{ height: 10 }} />
                    <Text style={styles.word}>訂票者：{userName}</Text>
                    <Text style={styles.word}>電話：{userPhone}</Text>
                    <Text style={{ height: 20 }} />
                        {/* 下拉式表單 */}
                        <PaymentDropdown />
                        <Text style={{height: 30}}/>
                        <Text style={styles.text} >金額：{areaPrice}</Text>
                        <Text style={{height: 10}}/>
                        <PayingButton
                            title="結帳"
                            sellWhat={sellWhat}
                            areaId={areaId}
                            navigation={navigation}
                        />
                         <Notification
                             visible={showNotification}
                             message={notificationMessage}
                             onClose={() => setShowNotification(false)}
                         />
                </View>
            )}/>
        </SafeAreaView>
    )
}