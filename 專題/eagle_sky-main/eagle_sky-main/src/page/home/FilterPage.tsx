import React, { useState } from 'react';
import { Button, View, StyleSheet, Text, TextInput, Image, TouchableOpacity, FlatList } from 'react-native';
import { StackNavigationProp } from '@react-navigation/stack';
import { navigationType } from '../navigationType'
import DateTimePicker from '@react-native-community/datetimepicker';
import { color } from '../../style'

function turnToDate(stamp: Date){
    let date = new Date(stamp)
    let Y = date.getFullYear() + '-';
    let M = (date.getMonth()+1 < 10 ? '0'+(date.getMonth()+1) : date.getMonth()+1) + '-';
    let D = date.getDate()< 10? '0'+date.getDate() : date.getDate();
    return Y+M+D
}


export default function FilterPage({ navigation }: navigationType) {
    function showDateTimePicker(onChangeFunc: Function, date: string, setShowDatePicker: Function){
        return (
            <DateTimePicker
                testID="dateTimePicker"
                value={date!=""? new Date(date): new Date()}
                negativeButton={{label: '取消'}}
                onChange={(item: any) => {
                    if(item.type=="dismissed")
                        onChangeFunc(setShowDatePicker(false))
                    else
                        onChangeFunc(turnToDate(item.nativeEvent.timestamp),setShowDatePicker(false))
                }}
            />
        )
    }
    const [showDatePicker1, setShowDatePicker1] = useState(false)
    const [showDatePicker2, setShowDatePicker2] = useState(false)
    const [showDatePicker3, setShowDatePicker3] = useState(false)
    const [showDatePicker4, setShowDatePicker4] = useState(false)
    const [chooseStartDate, setChooseStartDate] = useState("");
    const [chooseEndDate, setChooseEndDate] = useState("");
    const [discardStartDate, setDiscardStartDate] = useState("");
    const [discardEndDate, setDiscardEndDate] = useState("");
    return (
        <View style={styles.body}>
            <View style={styles.block}>
                <Text style={styles.title}>選擇時段：</Text>
                <View style={styles.line}>
                    <TouchableOpacity
                        style={chooseStartDate? styles.buttonChosen: styles.button}
                        activeOpacity={0.7}
                        onPress={() => {setShowDatePicker1(true)}}
                    >
                        <Text style={styles.buttonText}>{chooseStartDate? chooseStartDate: "起始日期"}</Text>
                    </TouchableOpacity>
                    {showDatePicker1 && (showDateTimePicker(setChooseStartDate, chooseStartDate, setShowDatePicker1))}
                    <Text style={styles.text}>至</Text>
                    <TouchableOpacity
                        style={chooseEndDate? styles.buttonChosen: styles.button}
                        activeOpacity={0.7}
                        onPress={() => {setShowDatePicker2(true)}}
                    >
                        <Text style={styles.buttonText}>{chooseEndDate? chooseEndDate: "終止日期"}</Text>
                    </TouchableOpacity>
                    {showDatePicker2 && (showDateTimePicker(setChooseEndDate, chooseEndDate, setShowDatePicker2))}
                </View>
                    
                <Text style={styles.title}>排除時段：</Text>
                <View style={styles.line}>
                    <TouchableOpacity
                        style={discardStartDate? styles.buttonChosen: styles.button}
                        activeOpacity={0.7}
                        onPress={() => {setShowDatePicker3(true)}}
                    >
                        <Text style={styles.buttonText}>{discardStartDate? discardStartDate: "起始日期"}</Text>
                    </TouchableOpacity>
                    {showDatePicker3 && (showDateTimePicker(setDiscardStartDate, discardStartDate, setShowDatePicker3))}
                    <Text style={styles.text}>至</Text>
                    <TouchableOpacity
                        style={discardEndDate? styles.buttonChosen: styles.button}
                        activeOpacity={0.7}
                        onPress={() => {setShowDatePicker4(true)}}
                    >
                        <Text style={styles.buttonText}>{discardEndDate? discardEndDate: "終止日期"}</Text>
                    </TouchableOpacity>
                    {showDatePicker4 && (showDateTimePicker(setDiscardEndDate, discardEndDate, setShowDatePicker4))}
                </View>

                <View style={styles.endLine}>
                    <TouchableOpacity
                        activeOpacity={0.7}
                        onPress={() => {navigation.goBack()}}
                    >
                        <Text style={styles.optionButton}>取消</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        activeOpacity={0.7}
                        onPress={() => {navigation.navigate("Home", {
                            chooseStartDate: chooseStartDate,
                            chooseEndDate: chooseEndDate,
                            discardStartDate: discardStartDate,
                            discardEndDate: discardEndDate
                        })}}
                    >
                        <Text style={styles.optionButton}>確認</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    body: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    block: {
        backgroundColor: color.backgroundColor,
        width: '80%',
        borderRadius: 5,
        paddingHorizontal: 25,
        paddingVertical: 10
    },
    title:{
        fontSize: 16,
        fontWeight: "600",
        color: color.blue,
        marginVertical: 10
    },
    line:{
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: 15,
        marginBottom: 15
    },
    button: {
        backgroundColor: color.gray,
        width: 100,
        borderRadius: 5,
    },
    buttonChosen:{
        backgroundColor: color.blue,
        width: 100,
        borderRadius: 5,
    },
    buttonText:{
        color: "white",
        textAlign: 'center',
        alignSelf: 'center',
        lineHeight: 28,
        flex: 0
    },
    text:{
        fontSize: 16,
        fontWeight: "600",
        color: color.darkBlue,
        marginHorizontal: 5
    },
    endLine:{
        flexDirection: 'row',
        justifyContent: 'flex-end',
        paddingHorizontal: 15,
        marginVertical: 15,
        gap: 20
    },
    optionButton:{
        color: "white",
        paddingHorizontal: 15,
        borderRadius: 5,
        lineHeight: 30,
        fontSize: 16,
        fontWeight: "600",
        backgroundColor: color.darkyellow,
    }
})