-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- 主機： 127.0.0.1
-- 產生時間： 2022-05-16 17:37:27
-- 伺服器版本： 10.4.22-MariaDB
-- PHP 版本： 8.1.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 資料庫: `robot`
--

-- --------------------------------------------------------

--
-- 資料表結構 `memo`
--

CREATE TABLE `memo` (
  `date` date NOT NULL COMMENT '日期',
  `time` time NOT NULL COMMENT '時間',
  `type` varchar(300) NOT NULL COMMENT '類別',
  `detail` mediumtext NOT NULL COMMENT '事件'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='備忘錄';

-- --------------------------------------------------------

--
-- 資料表結構 `message`
--

CREATE TABLE `message` (
  `id` int(11) DEFAULT NULL,
  `queries` varchar(300) NOT NULL COMMENT '問',
  `replies` varchar(300) NOT NULL COMMENT '答'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 傾印資料表的資料 `message`
--

INSERT INTO `message` (`id`, `queries`, `replies`) VALUES
(0, 'hi|hello|hey', 'Hello! Search time please eneter time; Search incident type please enter incident;Insert memo please enter memo'),
(1, 'Search for time|time', 'Please enter the time below.'),
(2, 'Search for incident|incident', 'Please enter the incident below.'),
(3, 'Insert the new memo|memo', 'OK!');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
